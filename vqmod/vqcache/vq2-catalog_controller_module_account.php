<?php  
class ControllerModuleAccount extends Controller {
	protected function index() {
		$this->language->load('module/account');
		
    	$this->data['heading_title'] = $this->language->get('heading_title');
    	
		$this->data['text_register'] = $this->language->get('text_register');
    	$this->data['text_login'] = $this->language->get('text_login');
		$this->data['text_logout'] = $this->language->get('text_logout');
		$this->data['text_forgotten'] = $this->language->get('text_forgotten');
		$this->data['text_account'] = $this->language->get('text_account');
		$this->data['text_edit'] = $this->language->get('text_edit');
		$this->data['text_password'] = $this->language->get('text_password');
		$this->data['text_address'] = $this->language->get('text_address');
		$this->data['text_wishlist'] = $this->language->get('text_wishlist');
		$this->data['text_order'] = $this->language->get('text_order');
		$this->data['text_download'] = $this->language->get('text_download');
		$this->data['text_return'] = $this->language->get('text_return');
		$this->data['text_transaction'] = $this->language->get('text_transaction');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
		
		$this->data['logged'] = $this->customer->isLogged();
		$this->data['register'] = $this->url->link('account/register', '', 'SSL');
    	$this->data['login'] = $this->url->link('account/login', '', 'SSL');
		$this->data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$this->data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['edit'] = $this->url->link('account/edit', '', 'SSL');
		$this->data['password'] = $this->url->link('account/password', '', 'SSL');
		$this->data['address'] = $this->url->link('account/address', '', 'SSL');
		$this->data['wishlist'] = $this->url->link('account/wishlist');
		$this->data['order'] = $this->url->link('account/order', '', 'SSL');
		$this->data['download'] = $this->url->link('account/download', '', 'SSL');
		$this->data['return'] = $this->url->link('account/return', '', 'SSL');
		$this->data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');


		//+mod by yp start
		$this->data['accc_show_website'] = false;
		if($this->config->get('account_combine_status')) {			
			$this->language->load('affiliate/account_combine');
			if($this->affiliate->isLogged()) {
				$this->data['text_link_to_affiliate'] = $this->language->get('text_link_to_affiliate_in_customer');
				$this->data['link_to_affiliate'] = $this->url->link('affiliate/account', '', 'SSL');			
			} else {
				$this->data['link_to_affiliate'] = $this->url->link('affiliate/account_combine', '', 'SSL');
				$customer_id = $this->customer->isLogged();
				if($customer_id) {
					$this->load->model('affiliate/account_combine');
					$_aff_status = $this->model_affiliate_account_combine->getAccountStatus($customer_id);
					if($_aff_status === true) {
						$this->data['text_link_to_affiliate'] = $this->language->get('text_link_to_affiliate_in_customer');
					} else {
						$_map = array(
							'no_account' => 'text_link_to_create_affiliate_in_customer',
							'not_approved' => 'text_link_to_affiliate_not_approved_in_customer',
							'disabled' => 'text_link_to_affiliate_disabled_in_customer'
						);
						$this->data['text_link_to_affiliate'] = isset($_map[$_aff_status]) ? $this->language->get($_map[$_aff_status]) : '';
						if($_aff_status === 'no_account' && $this->config->get('account_combine_website_required')) {
							$this->data['accc_show_website'] = true;
							$this->language->load('affiliate/edit');
							$this->data['accc_entry_website'] = $this->language->get('entry_website');
							$this->data['accc_website_required'] = $this->config->get('account_combine_website_required');
							$this->data['accc_error_website'] = str_replace(array("\r\n", "\r", "\n"), array('\\n', '\\n', '\\n'), addslashes(html_entity_decode($this->language->get('error_website'), ENT_QUOTES, 'UTF-8')));
							$this->data['accc_website_textarea'] = $this->config->get('account_combine_website_textarea');
						} else {
							$this->data['accc_show_website'] = false;
						}
					}
				} else {
					$this->data['text_link_to_affiliate'] = '';
				}
			}
		} else {
			$this->data['text_link_to_affiliate'] = '';
			$this->data['link_to_affiliate'] = '';
		}
		//+mod by yp end

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/account.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/account.tpl';
		} else {
			$this->template = 'default/template/module/account.tpl';
		}
		

				$this->data['ms_seller_created'] = $this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId());			
				$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
			

				$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller_physical'));
				$this->data['ms_link_shipping_settings'] = $this->url->link('seller/account-shipping-settings', '', 'SSL');
			
		$this->render();
	}
}
?>