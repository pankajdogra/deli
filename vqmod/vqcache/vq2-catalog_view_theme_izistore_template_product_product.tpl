<?php echo $header; ?><?php echo $column_right; ?>

				<style>
.auction .content_prices
{
    border-top: none;
    margin-top: auto;
    padding-top: 0;
	padding-left: 14px;
}

.auction .content_prices a.ajax_add_to_cart_button
{
	height: auto !important;
}

.auction #timer_0
{
	color: #000000 !important;
	text-align:center;
	font-size: 17px;
    font-weight: bold;
    line-height: 90%;
	text-shadow: 0 1px 0 #FFFFFF;
}

.auction .auction_closing_time, .auction .auction_min_step, #auction_loader span
{
    color: #666666;
    font-size: 10px;
    line-height: 13px;
}

.auction .auction_closing_time
{
	text-align:center;
}

.auction .auction_min_step
{
	float:right;
}

.auction_button
{
	width: 100px;
}

#winning_bidder
{
	font-weight:bold;
}

#winning_bidder.auction_leader
{
	color: green;
}

#auction_nickname a
{
	text-decoration: underline;
}

#auction_nickname a:hover
{
	text-decoration: none;
}

#nickname_
{
	vertical-align:top;
	width:14px;
	margin-left:-3px;
}

#auction_loader
{
	float:left;
}

#auction_loader img
{
	vertical-align:middle;
	width:13px;
	height:13px;
}

.auction .liner
{
	margin-top: 5px;
}

.auctiondivider 
{
    border-bottom : 1px dotted #E9E9E9;
    height: 2px;
    margin: 8px 0;
    overflow: hidden;
    width: 100%;
}

.buy_now
{
	/* margin: 0 auto; */
}

.auction .currency_symbol
{
	margin-right: 10px;
}

input.bid_input
{
	height: 21px;
	margin-left: 0;
	margin-right: 10px;
    padding: 0 5px;
    width: 30px;
}

.fleft
{
	float:left;
}

.fright
{
	float:right;
}

.price_tag
{
	float: left;
	min-width: 85px;
}

.buy_now_price
{
	padding-top: 3px;
}

a#auction_nickname
{
	text-decoration: underline;	
}

a#auction_nickname:hover
{
	text-decoration: none;
}

/* auction block error messages start */

.msg_holder
{
	position:relative;
	
}


.liner {
    margin-top: 5px;
}

.field_error:before, .field_error:after
{
    border-style: solid;
    display: block;
    content: " ";
    height: 0;
    position: absolute;
    width: 0;
}

.field_error:before 
{
    border-color: transparent yellow;
    border-width: 6px 0 6px 6px;
    right: -6px;
    top: 8px;
    z-index: 2;
}

.field_error:after
{
    border-color: transparent red;
    border-width: 6px 0 6px 6px;
    right: -7px;
    top: 8px;
    z-index: 1;
}

.field_error 
{
    background: none repeat scroll 0 0 yellow;
    border: 1px solid red;
    border-radius: 5px 5px 5px 5px;
    box-shadow: 2px 2px 2px gray;
    display: none;
    left: -169px;
    margin: 10px;
    padding: 5px;
    position: absolute;
    top: -10px;
    width: 160px;
    z-index: 1;
	opacity: 0.8;
}

.price_msg
{
	left: -105px !important;
	top: -16px !important;
}

.nickname_msg
{
	left: -190px !important;
	top: -18px !important;
}

/* auction block error messages end */

.auction_taxes_label
{
	margin-left: 3px;
	font-size: 9px;
}

.auction .iframe
{
	font-weight: normal !important;
}

#auction_subscriptions
{
	padding-left: 20px !important;
	background: url("../img/mail.png") no-repeat scroll 0 0 transparent;
}

/* bids history start */

#module-advauction-bids table
{
	border-left: 1px solid #e9e9e9;
}

#module-advauction-bids table .table-header
{
	border-left: 1px solid #999999;
}

#module-advauction-bids table tr.my-bid
{
	background:#FAF0E6;
}

/* bids history end */

/* my account begin */

#module-advauction-account #left_column,
#module-advauction-currentactivity #left_column,
#module-advauction-paymenthistory #left_column,
#module-advauction-subscriptions #left_column,
#module-advauction-options #left_column {
    display: none;
}

#module-advauction-account #center_column,
#module-advauction-currentactivity #center_column,
#module-advauction-paymenthistory #center_column,
#module-advauction-subscriptions #center_column,
#module-advauction-options #center_column {
    width: 757px;
}

#module-advauction-account #center_column p.title_block,
#module-advauction-currentactivity #center_column p.title_block,
#module-advauction-paymenthistory #center_column p.title_block,
#module-advauction-subscriptions #center_column p.title_block,
#module-advauction-options #center_column p.title_block,
#module-advauction-options p.title_block.content_only {
	color: #666666;
	font-size: 12px;
	font-weight: normal;
}

/* current activity begin */

#module-advauction-currentactivity #center_column table.std td,
#module-advauction-paymenthistory #center_column table.std td
{
	vertical-align:middle;
}

#module-advauction-currentactivity #center_column table.std td.leader
{
	color: green;
}

#module-advauction-currentactivity #center_column table.std td.loser
{
	color: red;
}

#module-advauction-currentactivity #center_column table.std th.center,
#module-advauction-currentactivity #center_column table.std td.center
{
	text-align: center;
}

/* current activity end */

/* payment history begin */
#module-advauction-paymenthistory #center_column #product_list li
{
	margin-left: 9px;
    border: none;
    margin-bottom: 0;
    padding: 0;
}

#module-advauction-paymenthistory #center_column .price_with_taxes
{
	font-weight:bold;
	margin-top:5px;
}

#module-advauction-paymenthistory #center_column .unpaid
{
	color:red;
}

/* payment history end */

#module-advauction-account .myaccount_lnk_list {
    list-style-type: none;
}

#module-advauction-account .myaccount_lnk_list li {
    font-size: 13px;
    line-height: 36px;
}

#module-advauction-account .myaccount_lnk_list img.icon,
#module-advauction-account p img.icon {
    left: 0;
    position: relative;
    top: 6px;
}

#module-advauction-subscriptions .auction-subscriptions
{
    border: 1px solid #EEEEEE;
    border-radius: 3px 3px 3px 3px;
    margin-bottom: 14px;
    padding: 12px 8px;
    position: relative;
}

#module-advauction-subscriptions .auction-subscriptions a.product_img_link {
    border: 1px solid #CCCCCC;
    display: block;
    float: left;
    margin-right: 14px;
    overflow: hidden;
    position: relative;
}

#module-advauction-subscriptions .auction-subscriptions h3 {
    color: #000000;
    font-size: 13px;
    padding: 0 0 10px;
}

#module-advauction-subscriptions .auction-subscriptions .remove {
    position: absolute;
    right: 10px;
    top: 10px;
}

#module-advauction-subscriptions .auction-subscriptions .remove .icon {
    cursor: pointer;
}

#module-advauction-options fieldset
{
	background: none repeat scroll 0px 0px rgb(248, 248, 248);
	border: 1px solid rgb(204, 204, 204);
	margin: 0px 0px 12px;
	padding-top: 20px;
}

#module-advauction-options fieldset input[type="text"]
{
	border: 1px solid #CCCCCC;
	color: #666666;
	font-size: 12px;
	height: 22px;
	padding: 0 5px;
	width: 360px;
}

#module-advauction-options label
{
	display: inline-block;
	font-size: 14px;
	padding: 6px 15px;
	text-align: right;
	width: 230px;
}

#module-advauction-options label.popup_label
{
	width: 140px;
}

#module-advauction-options label.options-checkbox
{
	width: 589px;
}

/* my account end */

/* homefeatured fix */

#featured-products_block_center li
{
	height: 295px;
}
</style>
			
<section class="container">
<aside class="row margin15">
<aside class="span3">


<?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  
  <?php echo $column_left; ?>
</aside>
<aside class="span8">
<h1><?php echo $heading_title; ?></h1>
  <div class="product-info">
    <?php if ($thumb || $images) { ?>
    <div class="left">
      <?php if ($thumb) { ?>
      <div class="image"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" /></a></div>
      <?php } ?>
      <?php if ($images) { ?>
      <div class="image-additional">
        <?php foreach ($images as $image) { ?>
        <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
    <div class="right">
      <div class="description"> 
        <?php if ($manufacturer) { ?>
        <span><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a><br />
        <?php } ?>
        <span><?php echo $text_model; ?></span> <?php echo $model; ?><br />
        <?php if ($reward) { ?>
        <span><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
        <?php } ?>
        <span><?php echo $text_stock; ?></span> <span id="stock_holder_span"></span></div>
      
			
 <?php if(!$logged){ ?>	  
	   <?php if($checkstatusbid == 1){ ?>
	   
	    <?php if($checkBidstate == 0){ ?>
         	 <div id="timercontents">     
		<?php include('catalog/view/theme/default/template/product/counter.tpl'); ?>
		</div>
        <?php } else {?>
 

       <b><?php echo $text_bid_only_reg;?></b>
	   
       <?php } ?> 
		
		<?php if($endbidtime){ 
		$date_end = date("Y-m-d", strtotime($endbidtime));
		$date = explode("-",$date_end);
		$TargetDate = $date[1].'/'.$date[2].'/'.$date[0];
		?>
		<div class="auction_closing_time"><span><?php echo $text_close_time;?></span>  <?php echo $TargetDate;?></div>
		<?php } ?>	
		
		<br/>
         
        <div class="auctiondivider"></div>		 
		
		<div class="msg_holder">
		<div class="field_error nickname_msg"></div>
			<?php echo $text_part; ?><span id="auction_nickname">Guest (<a href="<?php echo $login;?>">log in</a>)</span>	
		</div>
		
		<div class="liner">
		 <?php echo $text_winner; ?>
		 
		<?php if($countCustomerBids>0){ ?>
		<?php if($checkwinner) {?>
		<span id="winning_bidder"><?php echo $checkwinner[2]; ?></span>
		<?php } else {?>
		<span id="winning_bidder"><?php echo $text_bid_bids;?></span>
		<?php } ?>	
       <?php }else{ ?>
	   	<span id="winning_bidder"><?php echo $text_bid_bids;?></span>
		
      <?php } ?>	   
	  </div>
	  
	  <div class="auctiondivider"></div>	
		
	<div class="msg_holder" id="current_price">
		    <div class="price_tag"><?php echo $text_current_price; ?></div>
			
			     <div id="current_price_value"><?php echo $maxcustomerbidamt;?></div>
				 
				 <div class="price_tag"><?php echo $text_bid_curr_is;?></div><div id="current_price_value"><?php echo $startbidprice;?></div>
				 
				 
				 <div style="text-align:right;font-weight:bold;color:#38B0E3;" ><a id="btn">Bids(<?php echo $bids1;?>)</a></div>
			  
				<div class="clearfix"></div>
	</div>
	
	<div style="display:none;">
    <div id="lightboxRegister"> 
	<table class="list" style="margin-top:20px;">
	
	
	 <thead>
	 
      <tr>
        <td class="left">Date</td>
        <td class="left">Bidder</td>
		<td class="left">Price</td>
		</tr>
    </thead>
	
	<?php if ($rbids1) { ?>
      <?php foreach ($rbids1 as $ractivity) { ?>
	
	 <tr>
	     
		 <td class="left"><?php echo $ractivity['date_added']; ?></td>
	     <td class="left"><?php echo $ractivity['firstname']; ?> <?php echo $ractivity['lastname']; ?></td>
        <td class="left"><?php echo $this->currency->format($ractivity['price_bid']); ?></td>
		
		
		</tr>
		
	<?php } } else{?>
	
	<tr><td colspan="3"  style="text-align:center">There are no bid yet</td></tr>
	
	
	<?php } ?>
	
	
	
	</table>
	
	</div>
</div>
	
	<script>
	  $('#btn').click(function(){
    $.colorbox({inline:true, width:"50%",height:"60%", href: '#lightboxRegister'});
});
	  </script>
	
 <div class="auctiondivider"></div>	
	
	
	<?php if($checkBidstate == 0){ ?>
	<div id="openAuction">
	
	           <?php if($config_bid == 1){ ?>
            	<div class="liner msg_holder" id="pbid">				
				
				<input type="hidden" name="customer_id" value="<?php echo $customerid;?>" id="customer_id">
				<input type="hidden" name="mincustomerbid" value="<?php echo $mincustomerbid;?>" id="mincustomerbid">
				<input type="hidden" name="productname" value="<?php echo $productname;?>" id="productname">
				<input type="text" id="bid_value" class="bid_input" name="customer_bid">
				<a id="button-bid" class="button">Place my bid</a>			
				
				
		        </div>
				
				<?php } ?>
				
				<br/>
				
				<?php if($config_bid == 1){ ?>
				
				<div class="liner msg_holder" id="abid">
				
				<input type="hidden" name="customer_id" value="<?php echo $customerid;?>" id="customer_id">
				<input type="hidden" name="mincustomerbid" value="<?php echo $mincustomerbid;?>" id="mincustomerbid">
				<input type="hidden" name="productname" value="<?php echo $productname;?>" id="productname">
				<input type="text" id="autobid_value" class="bid_input" name="autobid_value">
				<a id="button-auto" class="button">Auto bid to</a>		
			
				
				</div>
				
				<?php } ?>
				<div class="clearfix"></div>
		        <div id="auction_loader">
				<img src="catalog/view/theme/default/image/ajax-loader-static.png">
				<span>Updating in </span><span id="auction_reloading">10</span><span>s.</span>
			</div>
	
			<div class="liner" style="text-align:right;"><?php echo $text_min_step;?><?php echo $minoffersteps;?></div>
			<div class="clearfix"></div>
	</div>
	<?php }  ?>
	
	
	<div class="auctiondivider"></div>
	
     <?php }?>	
		<?php include('catalog/view/theme/default/template/product/buy.tpl'); ?>
      <?php }else{ ?>
      	
		<?php if($checkstatusbid == 1){ ?>
         	   
		<?php if($checkBidstate == 0){ ?>
         	 <div id="timercontents">  
		<?php include('catalog/view/theme/default/template/product/counter.tpl'); ?>
		</div>
        <?php } else {?>
 

       <b><?php echo $text_bid_only_reg;?></b>
	   
       <?php } ?> 		
		
		<?php if($endbidtime){ ?>
		
	<?php	$date_end = date("Y-m-d", strtotime($endbidtime));
        $date = explode("-",$date_end);
		$TargetDate = $date[1].'/'.$date[2].'/'.$date[0];
		?>
		<div class="auction_closing_time"><span><?php echo $text_close_time;?></span>  <?php echo $TargetDate;?></div>
		
		<?php } ?>		
		<br/>
         
        <div class="auctiondivider"></div>		 
		
		<div class="msg_holder">
		<div class="field_error nickname_msg"></div>
			<?php echo $text_part; ?><span id="auction_nickname">
			
			<?php if($namesetting){
			
			echo $namesetting;
			}else {
			echo $firstname;
			}
			?>
			
			</span>	
		</div>
		
		<div class="liner">
		 <?php echo $text_winner; ?>
		<?php if($countCustomerBids>0){ ?>
		<?php if($checkwinner) {?>
		<span id="winning_bidder"><?php echo $checkwinner[2]; ?></span>
		<?php } else {?>
		<span id="winning_bidder"><?php echo $text_bid_bids;?></span>
		<?php } ?>	
       <?php }else{ ?>
	   	<span id="winning_bidder"><?php echo $text_bid_bids;?></span>
		
      <?php } ?>		
	  </div>
	  
	
	  
	  <div class="auctiondivider"></div>	
		
	<div class="msg_holder" id="current_price">
		    <div class="price_tag"><?php echo $text_current_price; ?></div>
			
		      <div id="current_price_value"><?php echo $maxcustomerbidamt;?></div>
			  
			  <div class="price_tag"><?php echo $text_bid_curr_is;?></div>
			  
			  <div id="current_price_value"><?php echo $startbidprice;?></div>
			  
			  <div style="text-align:right;font-weight:bold;color:#38B0E3;" ><a id="btn">Bids(<?php echo $bids1;?>)</a></div>
			  
			
				<div class="clearfix"></div>
				
	</div>
	
	<div style="display:none;">
    <div id="lightboxRegister"> 
	<table class="list" style="margin-top:20px;">
	
	
	 <thead>
	 
      <tr>
        <td class="left">Date</td>
        <td class="left">Bidder</td>
		<td class="left">Price</td>
		</tr>
    </thead>
	
	<?php if ($rbids1) { ?>
      <?php foreach ($rbids1 as $ractivity) { ?>
	
	 <tr>
	     
		 <td class="left"><?php echo $ractivity['date_added']; ?></td>
	     <td class="left"><?php echo $ractivity['firstname']; ?> <?php echo $ractivity['lastname']; ?></td>
        <td class="left"><?php echo $this->currency->format($ractivity['price_bid']); ?></td>
		
		
		</tr>
		
	<?php } } else{?>
	
	<tr><td colspan="3"  style="text-align:center">There are no bid yet</td></tr>
	
	
	<?php } ?>
	
	
	
	</table>
	
	</div>
</div>
	
	<script>
	  $('#btn').click(function(){
    $.colorbox({inline:true, width:"50%",height:"60%", href: '#lightboxRegister'});
});
	  </script>
	
	<div class="auctiondivider"></div>	
	
	
	
	

	
	
	<?php if($checkBidstate == 0){ ?>
	
	
	<div id="openAuction">
	
	            <?php if($config_bid == 1){ ?>
				
            	<div class="liner msg_holder" id="pbid">				
				
				<input type="hidden" name="customer_id" value="<?php echo $customerid;?>" id="customer_id">
					<input type="hidden" name="mincustomerbid" value="<?php echo $mincustomerbid;?>" id="mincustomerbid">
					<input type="hidden" name="productname" value="<?php echo $productname;?>" id="productname">
				<input type="text" id="bid_value" class="bid_input" name="customer_bid">
				<a id="button-bid" class="button">Place my bid</a>			
				
				
		        </div>
				
				<br/>
				<?php } ?>
				
				<?php if($config_autobid == 1){ ?>
				
				<div class="liner msg_holder" id="abid">
				
				<input type="hidden" name="customer_id" value="<?php echo $customerid;?>" id="customer_id">
				<input type="hidden" name="mincustomerbid" value="<?php echo $mincustomerbid;?>" id="mincustomerbid">
				<input type="hidden" name="productname" value="<?php echo $productname;?>" id="productname">
				<input type="text" id="autobid_value" class="bid_input" name="autobid_value">
				<a id="button-auto" class="button">Auto bid to</a>		
			
				
				</div>
				
				<?php } ?>
				<div class="clearfix"></div>
	
	
			<div id="auction_loader">
				<img src="catalog/view/theme/default/image/ajax-loader-static.png">
				<span>Updating in </span><span id="auction_reloading">10</span><span>s.</span>
			</div>
	
			<div class="liner" style="text-align:right;"><?php echo $text_min_step;?><?php echo $minoffersteps;?></div>
			
			<div class="clearfix"></div>
			
			<br/>
			
			<?php if($config_allow_subscriptions_on == 1){ ?>
			
			<div class="liner msg_holder" id="sub">				
				
				<input type="hidden" name="customer_id" value="<?php echo $customerid;?>" id="customer_id">
				<input type="hidden" name="mincustomerbid" value="<?php echo $mincustomerbid;?>" id="mincustomerbid">
				<input type="hidden" name="productname" value="<?php echo $productname;?>" id="productname">
				<input type="hidden" id="email" name="email" value="<?php echo $email;?>">
				<input type="hidden" id="currentprice" name="currentprice" value="<?php echo $maxcustomerbidamt;?>">
				<?php if($checksub>0){	?>
				<a id="button-sub" style="display:none">Subscribe this auction</a>	

                <a id="button-unsub" style="display:block">UnSubscribe this auction</a>
				
				<?php } else{ ?>
				
				<a id="button-sub" style="display:block">Subscribe this auction</a>	

                <a id="button-unsub" style="display:none">UnSubscribe this auction</a>
				
				<?php } ?>
				
				
				
		        </div>
           <?php } ?>				
	</div>
	<?php } ?>
	
	
	
	<div class="auctiondivider"></div>
	
     <?php }	 ?>	
		<?php include('catalog/view/theme/default/template/product/buy.tpl'); ?>
      <?php } ?>
    
      
      
    </div>
  </div>
  
  
  <script type="text/javascript"><!--

$('#button-bid').bind('click', function() {
$.ajax({
	url: 'index.php?route=product/product/bid&product_id=<?php echo $product_id; ?>',
	type: 'post',
	data: $('#pbid input[type=\'text\'], #pbid input[type=\'hidden\']'),
	dataType: 'json',
      success: function(data) {
	  
	      $('.success, .warning, .attention, information, .error').remove();
		  
			if (data['error']) {
				$('#button-bid').after('<span class="error">' + data['error'] + '</span>');
			}
			
			if (data['success']) {
				$('#button-bid').after('<div style="color:green">' + data['success'] + '</div>');
				window.location.reload(true);
				
			}
		}
	});
});


$('#button-auto').bind('click', function() {
$.ajax({
	url: 'index.php?route=product/product/auto&product_id=<?php echo $product_id; ?>',
	type: 'post',
	data: $('#abid input[type=\'text\'], #abid input[type=\'hidden\']'),
	dataType: 'json',
      success: function(data) {
	  
	       $('.success, .warning, .attention, information, .error').remove();
		   
			if (data['error']) {
				$('#button-auto').after('<span class="error">' + data['error'] + '</span>');
			}
			
			if (data['success']) {
				$('#button-auto').after('<div style="color:green">' + data['success'] + '</div>');
				window.location.reload(true);
				
			}
		}
	});
});


$('#button-sub').bind('click', function() {
$.ajax({
	url: 'index.php?route=product/product/subscription&product_id=<?php echo $product_id; ?>',
	type: 'post',
	data: $('#sub input[type=\'hidden\']'),
	dataType: 'json',
      success: function(data) {
	  
	       $('.success, .warning, .attention, .abc, .error').remove();
		   
			if (data['error']) {
				$('#button-sub').after('<span class="error">' + data['error'] + '</span>');
			}
			
			if (data['success']) {
			
					
				$('#sub').after('<div style="color:green" class="abc">' + data['success'] + '</div>');
				
				var ele = document.getElementById("button-sub");
				var text = document.getElementById("button-unsub");
				
				if(ele.style.display == "block") {
					ele.style.display = "none";
					text.style.display = "block";
					}
					else if(text.style.display == "block") {
					ele.style.display = "block";
					text.style.display = "none";
				}
											
			}
		}
	});
});



$('#button-unsub').bind('click', function() {
$.ajax({
	url: 'index.php?route=product/product/unsubscription&product_id=<?php echo $product_id; ?>',
	type: 'post',
	data: $('#sub input[type=\'hidden\']'),
	dataType: 'json',
      success: function(data) {
	  
	       $('.success, .warning, .attention, .abc, .error').remove();
		   
			if (data['error']) {
				$('#button-sub').after('<span class="error">' + data['error'] + '</span>');
			}
			
			if (data['success']) {
			
					
				$('#sub').after('<div style="color:green" class="abc">' + data['success'] + '</div>');
				
				
				
				var ele = document.getElementById("button-sub");
				var text = document.getElementById("button-unsub");
				
				if(ele.style.display == "block") {
					ele.style.display = "none";
					text.style.display = "block";
					}
					else if(text.style.display == "block") {
					ele.style.display = "block";
					text.style.display = "none";
				}
											
			}
		}
	});
});

function toggle() {
	var ele = document.getElementById("button-sub");
	var text = document.getElementById("button-unsub");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "show";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}
}

</script>




			



































































































































































































  <div id="tabs" class="htabs"><a href="#tab-description"><?php echo $tab_description; ?></a>
    <?php if ($attribute_groups) { ?>
    <a href="#tab-attribute"><?php echo $tab_attribute; ?></a>
    <?php } ?>
    <?php if ($review_status) { ?>
    <a href="#tab-review"><?php echo $tab_review; ?></a>
    <?php } ?>
    <?php if ($products) { ?>
    <a href="#tab-related"><?php echo $tab_related; ?> (<?php echo count($products); ?>)</a>
    <?php } ?>

			<?php if (!empty($data['FacebookComments']['Enabled']) && $data['FacebookComments']['Enabled'] == 'yes' && $data['FacebookComments']['showInTab'] == 'yes') { ?>
			<a href="#tab-fbComments"><?php echo $tab_fbComments; ?></a>
			<?php } ?>
			

				<?php if ($this->config->get('msconf_product_shipping_cost_estimation') && $shippable) { ?>
					<a href="#tab-shipping-estimation"><?php echo $ms_tab_shipping_estimation ?></a>
				<?php } ?>
			
  </div>
  <div id="tab-description" class="tab-content"><?php echo $description; ?></div>
  <?php if ($attribute_groups) { ?>
  <div id="tab-attribute" class="tab-content">
    <table class="attribute">
      <?php foreach ($attribute_groups as $attribute_group) { ?>
      <thead>
        <tr>
          <td colspan="2"><?php echo $attribute_group['name']; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
        <tr>
          <td><?php echo $attribute['name']; ?></td>
          <td><?php echo $attribute['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
      <?php } ?>
    </table>
  </div>
  <?php } ?>
  <?php if ($review_status) { ?>
  <div id="tab-review" class="tab-content">
    <div id="review"></div>
    <h2 id="review-title"><?php echo $text_write; ?></h2>
    <b><?php echo $entry_name; ?></b><br />
    <input type="text" name="name" value="" />
    <br />
    <br />
    <b><?php echo $entry_review; ?></b>
    <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
    <br />
    <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
    <input type="radio" name="rating" value="1" />
    &nbsp;
    <input type="radio" name="rating" value="2" />
    &nbsp;
    <input type="radio" name="rating" value="3" />
    &nbsp;
    <input type="radio" name="rating" value="4" />
    &nbsp;
    <input type="radio" name="rating" value="5" />
    &nbsp;<span><?php echo $entry_good; ?></span>
    
    <b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="" />
    <br />
    <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />
    <br />
    <div class="buttons">
      <div class="right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
    </div>
  </div>
  <?php } ?>
  <?php if ($products) { ?>
  <div id="tab-related" class="tab-content">
    <div class="box-product">
      <?php foreach ($products as $product) { ?>
      <div>
        <?php if ($product['thumb']) { ?>
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><?php echo $button_cart; ?></a></div>
      <?php } ?>
    </div>
  </div>
  <?php } ?>

			<?php if (!empty($data['FacebookComments']['Enabled']) && $data['FacebookComments']['Enabled'] == 'yes' && $data['FacebookComments']['showInTab'] == 'yes') { ?>
			<div id="tab-fbComments" class="tab-content">
				<div class="fb-comments" data-href="<?php echo $fbCommentsPageURL; ?>" data-num-posts="<?php echo $data['FacebookComments']['NumberOfPosts']; ?>" data-width="<?php echo $data['FacebookComments']['Width']; ?>" data-colorscheme="<?php echo $data['FacebookComments']['ColorScheme']; ?>"></div>
			</div>
			<?php } ?>
			

				<?php if ($this->config->get('msconf_product_shipping_cost_estimation') && $shippable) { ?>
  				<div id="tab-shipping-estimation" class="tab-content">
					<label for="shipping_geo_zone"><?php echo $ms_select_geo_zone; ?></label>
					<select id="shipping_geo_zone" name="shipping_geo_zone">
						<?php foreach ($geo_zones as $geo_zone) { ?>
							<option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
						<?php } ?>
					</select>
					<a id="button_get_rates" class="button"><?php echo $ms_button_get_rates; ?></a>
					<div class="shipping_methods"></div>
  				</div>
				<?php } ?>
			
  <?php if ($tags) { ?>
  <div class="tags"><b><?php echo $text_tags; ?></b>
    <?php for ($i = 0; $i < count($tags); $i++) { ?>
    <?php if ($i < (count($tags) - 1)) { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
    <?php } else { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
    <?php } ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?>
 </aside> </aside></section>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});
//--></script> 

<script type="text/javascript"><!--
$(function() {
	$.get(
		'index.php',
		{
			'route' : 'product/product/stock',
			'product_id' : '<?php echo $product_id; ?>'
		},
		function(data) {
			$('#stock_holder_span').replaceWith(data);
		}
	)
});
--></script>

				<script type="text/javascript"><!--
					var shipping_estimate_product_id = <?php echo $product_id; ?>;
				//--></script>
			
<?php echo $footer; ?>