<?php  
class ControllerProductProduct extends Controller {
	private $error = array(); 
	

	public function bid() {
		//echo "<pre/>";print_r($this->request->post);die;
		$this->language->load('product/product');
		
		$this->load->model('catalog/product');
				
		$json = array();
		
		
		
		if (empty($this->request->post['customer_id'])) {
		   
				$json['error'] = "In order to participate, you must be logged in";
		}else{
			
			$num = $this->model_catalog_product->checkbanipaddress($this->customer->getId());
			
			if($num){
			
					$json['error'] = "You have been denied the right to participate in auctions";
			
			}else{			
			        if ($this->request->post['customer_bid'] < $this->request->post['mincustomerbid']) {
				     $json['error'] = "Your price is too low.Offer atleast ".$this->request->post['mincustomerbid']."";
			        }

					$autobids = $this->model_catalog_product->forautobids($this->request->get['product_id'],$this->customer->getId());
					if ($this->request->post['customer_bid'] > $autobids && $autobids>0) {
						$json['error'] = "Your price is high .your max bid limit is ".$autobids."";
			        }
            }			
			
		}
			
			
			if (!$json) {
		
			
			$countproductbids = $this->model_catalog_product->forMaxProductBidstable($this->request->get['product_id']);			
			$maxpbids = $this->model_catalog_product->forMaxProductBids1($this->request->get['product_id']);				
			$minvalue =$maxpbids;
			
	    if($minvalue > 0){				
						
		if($countproductbids > 0){
		            if($this->request->post['customer_bid'] >= $minvalue){
						$this->model_catalog_product->updateProductBidstable($this->request->get['product_id'],$this->customer->getId(),$this->request->post['customer_bid'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
						$json['success'] = "Auction reached maximum limit.Auction Ended";
				    }			
		    }
				
		}else{
		
			if($countproductbids > 0){
		            if($this->request->post['customer_bid'] >= $maxpbids){
					
						$this->model_catalog_product->updateProductBidstable($this->request->get['product_id'],$this->customer->getId(),$this->request->post['customer_bid'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
						$json['success'] = "Auction reached maximum limit.Auction Ended";
				    }			
		    }
		
		
		
		}
		
		$this->data['checkclosedate'] = $this->model_catalog_product->checkclosedatetime($this->request->get['product_id']);
		$currentdate= date("Y-m-d");
			
			
			if($this->data['checkclosedate'] == $currentdate){
							
				$this->model_catalog_product->updateProductBidstable($this->request->get['product_id'],$this->customer->getId(),$this->request->post['customer_bid'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
				
				$json['success'] = "Auction reached end date.Auction Ended";
				
				
				
			}
			
			$total = $this->model_catalog_product->countforCustomerBids($this->request->get['product_id'],$this->customer->getId());
					
			if($total){
					$this->model_catalog_product->updateCustomerBid($this->request->get['product_id'],$this->customer->getId(),$this->request->post['customer_bid'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
					
					
					
			}else{
					
					$this->model_catalog_product->addCustomerBid($this->request->get['product_id'],$this->customer->getId(),$this->request->post['customer_bid'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
					
					
					
			}
			
			   $this->model_catalog_product->addBidhistory($this->request->get['product_id'],$this->customer->getId(),$this->request->post['customer_bid'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
					
					
			
								
					$json['success'] = "Your bid is successfully placed";
		   }	
		
		
		
		
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function auto() {
		$this->language->load('product/product');
		
		$this->load->model('catalog/product');
				
		$json = array();
		
		  if (empty($this->request->post['customer_id'])) {
		   
				$json['error'] = "In order to participate, you must be logged in";
			}else{
			
			$num = $this->model_catalog_product->checkbanipaddress($this->customer->getId());
			
			if($num){
			
					$json['error'] = "You have been denied the right to participate in auctions";
			
			}else{
			
			if ($this->request->post['autobid_value'] < $this->request->post['mincustomerbid']) {
				$json['error'] = "Please set exact or higher autobid amount";
			}

            }			
			
			}
			
			
			if (!$json) {
			
			$total = $this->model_catalog_product->countforautoBids($this->request->get['product_id'],$this->customer->getId());
			
			if($total){
			
			
			
			$this->model_catalog_product->updateautoBid($this->request->get['product_id'],$this->customer->getId(),$this->request->post['autobid_value'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
			
			
			
			}else{
			
			$this->model_catalog_product->addautoBid($this->request->get['product_id'],$this->customer->getId(),$this->request->post['autobid_value'],$this->customer->getFirstName(),$this->customer->getLastName(),$this->request->post['productname']);
			
			
			
			}
			
			
						
			$json['success'] = "Your Max price is successfully set to ".$this->request->post['autobid_value']."";
			
			}
		   	
		
		
		
		
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function subscriptions() {
	
		$this->language->load('mail/subscription');


		$this->load->model('catalog/product');
		
		$chk =  $this->model_catalog_product->chkcode($this->request->get['id'],$this->request->get['code']);
		
		if($chk){
			
		  $result=$this->model_catalog_product->updatesubscriptions($this->request->get['id'],$this->request->get['status']);


			 $this->data['error_warning'] = "Subscription successfully confirmed.";

		
		}

		$this->redirect($this->url->link('product/success'));	
	
	
	
	
	}
	
	public function subscription() {
		$this->language->load('product/product');
		
		$this->load->model('catalog/product');
				
		$json = array();
		
		
	if (!$json) {	


           $total = $this->model_catalog_product->countsubscribe($this->request->get['product_id'],$this->customer->getId());
			
			if($total){	
			
			
			$this->model_catalog_product->updatesubscription($this->request->get['product_id'],$this->customer->getId(),$this->customer->getFirstName(),$this->customer->getLastName(),$this->customer->getEmail(),$this->request->post['productname'],$this->request->post['currentprice']);
			
			
			}else {
			
			
			$this->model_catalog_product->addsubscription($this->request->get['product_id'],$this->customer->getId(),$this->customer->getFirstName(),$this->customer->getLastName(),$this->customer->getEmail(),$this->request->post['productname'],$this->request->post['currentprice']);
			
			}
						
			$config_subscriptions_confirmation_on = $this->config->get('config_subscriptions_confirmation_on');
		
		if($config_subscriptions_confirmation_on=='1'){
						
			$json['success'] = "An email with confirmation details has been sent to address  ".$this->customer->getEmail()."";
			
			}else {
			$json['success'] = "Subscribed Successfully";
			
			}
			
	}
		   	
		
		
		
		
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function unsubscription() {
		$this->language->load('product/product');
		
		$this->load->model('catalog/product');
				
		$json = array();
		
		
	if (!$json) {	


           
			
			$this->model_catalog_product->deletesubscription($this->request->get['product_id'],$this->customer->getId());
			
			
			
			$json['success'] = "Sucessfully unsubscribed";
			
	}
		   	
		
		
		
		
		$this->response->setOutput(json_encode($json));
	}

			

			private function curPageURL() {
			 $pageURL = (empty($_SERVER['HTTPS'])) ? 'http://' : 'https://';
			 if ($_SERVER["SERVER_PORT"] != "80") {
			  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			 } else {
			  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			 }
			 return $pageURL;
			}
			
	public function index() { 

			$this->data['fbCommentsPageURL'] = $this->curPageURL();
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$this->data['data']['FacebookComments'] = str_replace('http', 'https', $this->config->get('FacebookComments'));
			} else {
				$this->data['data']['FacebookComments'] = $this->config->get('FacebookComments');
			}
			$this->language->load('module/facebookcomments');
			$this->data['tab_fbComments'] = $this->language->get('tab_fbComments');
			
		$this->language->load('product/product');
	
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);
		
		$this->load->model('catalog/category');	
		
		if (isset($this->request->get['path'])) {
			$path = '';
			
			$parts = explode('_', (string)$this->request->get['path']);
			
			$category_id = (int)array_pop($parts);
				
			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}
				
				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
					$this->data['breadcrumbs'][] = array(
						'text'      => $category_info['name'],
						'href'      => $this->url->link('product/category', 'path=' . $path),
						'separator' => $this->language->get('text_separator')
					);
				}
			}
			
			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);
				
			if ($category_info) {			
				$url = '';
				
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}	
	
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}	
				
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
				
				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}
										
				$this->data['breadcrumbs'][] = array(
					'text'      => $category_info['name'],
					'href'      => $this->url->link('product/category', 'path=' . $this->request->get['path']),
					'separator' => $this->language->get('text_separator')
				);
			}
		}
		
		$this->load->model('catalog/manufacturer');	
		
		if (isset($this->request->get['manufacturer_id'])) {
			$this->data['breadcrumbs'][] = array( 
				'text'      => $this->language->get('text_brand'),
				'href'      => $this->url->link('product/manufacturer'),
				'separator' => $this->language->get('text_separator')
			);	
	
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
							
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {	
				$this->data['breadcrumbs'][] = array(
					'text'	    => $manufacturer_info['name'],
					'href'	    => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url),					
					'separator' => $this->language->get('text_separator')
				);
			}
		}
		
		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';
			
			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}
						
			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}
						
			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}
			
			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}	

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
												
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_search'),
				'href'      => $this->url->link('product/search', $url),
				'separator' => $this->language->get('text_separator')
			); 	
		}
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {

			$this->document->addScript('catalog/view/javascript/dialog-sellercontact.js');
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/multiseller.css');
			$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));

				$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller_physical'));
			
			$this->load->model('localisation/country');
			$this->load->model('tool/image');
			
			$seller_id = $this->MsLoader->MsProduct->getSellerId($this->request->get['product_id']);
			$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
			
			if (!$seller) {
				$this->data['seller'] = NULL;
			} else {
				$this->data['seller'] = array();
				if (!empty($seller['ms.avatar'])) {
					$this->data['seller']['thumb'] = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
				} else {
					$this->data['seller']['thumb'] = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
				}
					
				$country = $this->model_localisation_country->getCountry($seller['ms.country_id']);
				
				if (!empty($country)) {			
					$this->data['seller']['country'] = $country['name'];
				} else {
					$this->data['seller']['country'] = NULL;
				}
				
				if (!empty($seller['ms.company'])) {
					$this->data['seller']['company'] = $seller['ms.company'];
				} else {
					$this->data['seller']['company'] = NULL;
				}
				
				if (!empty($seller['ms.website'])) {
					$this->data['seller']['website'] = $seller['ms.website'];
				} else {
					$this->data['seller']['website'] = NULL;
				}
				
				$this->data['seller']['nickname'] = $seller['ms.nickname'];

				$this->data['seller']['shipping_type'] = $seller['ms.shipping_type'];
			
				
				$this->data['seller']['href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
				
				$this->data['seller']['total_sales'] = $this->MsLoader->MsSeller->getSalesForSeller($seller['seller_id']);
				$this->data['seller']['total_products'] = $this->MsLoader->MsProduct->getTotalProducts(array(
					'seller_id' => $seller['seller_id'],
					'product_status' => array(MsProduct::STATUS_ACTIVE)
				));
				
				$badges = array_unique(array_merge(
					$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_id' => $seller['seller_id'], 'language_id' => $this->config->get('config_language_id'))),
					$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_group_id' => $seller['ms.seller_group'], 'language_id' => $this->config->get('config_language_id'))),
					$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_group_id' => $this->config->get('msconf_default_seller_group_id'), 'language_id' => $this->config->get('config_language_id')))
				), SORT_REGULAR);
				
				foreach ($badges as &$badge) {
					$badge['image'] = $this->model_tool_image->resize($badge['image'], $this->config->get('msconf_badge_width'), $this->config->get('msconf_badge_height'));
				}
				$this->data['seller']['badges'] = $badges;
			}

			$this->data['ms_product_attributes'] = $this->MsLoader->MsAttribute->getProductAttributes($this->request->get['product_id'], array('multilang' => 0, 'attribute_type'=> array(MsAttribute::TYPE_TEXT, MsAttribute::TYPE_TEXTAREA, MsAttribute::TYPE_DATE, MsAttribute::TYPE_DATETIME, MsAttribute::TYPE_TIME), 'mavd.language_id' => 0));
			$this->data['ms_product_attributes'] = array_merge($this->data['ms_product_attributes'], $this->MsLoader->MsAttribute->getProductAttributes($this->request->get['product_id'], (array())));
			

$this->data['logged'] = $this->customer->isLogged();
$this->data['text_close_time'] = $this->language->get('text_close_time');
			$this->data['login'] = $this->url->link('account/login');
			$this->data['text_bid_only_reg'] = $this->language->get('text_bid_only_reg');
			
			$this->data['text_winner'] = $this->language->get('text_winner');
			$this->data['text_part'] = $this->language->get('text_part');
			$this->data['text_current_price'] = $this->language->get('text_current_price');
			
			$this->data['text_min_step'] = $this->language->get('text_min_step');
			
			$this->data['productname'] = $product_info['name'];
			
			$this->data['text_bid_bids'] = $this->language->get('text_bid_bids');
			$this->data['text_bid_place'] = $this->language->get('text_bid_place');
			$this->data['text_bid_submit'] = $this->language->get('text_bid_submit');
			$this->data['text_bid_delete'] = $this->language->get('text_bid_delete');
			$this->data['text_bid_curr_is'] = $this->language->get('text_bid_curr_is');
			$this->data['text_bid_accept'] = $this->language->get('text_bid_accept');
			$this->data['text_bid_refuse'] = $this->language->get('text_bid_refuse');
			$this->data['text_bid_big_off'] = $this->language->get('text_bid_big_off');
			$this->data['text_bid_explain'] = $this->language->get('text_bid_explain');	
			$this->data['text_bid_reserved'] = $this->language->get('text_bid_reserved');
			
		

			
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
						
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
						
			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}			

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}
						
			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}
			
			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}	
						
			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}
			
			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}	
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
																		
			$this->data['breadcrumbs'][] = array(
				'text'      => $product_info['name'],
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']),
				'separator' => $this->language->get('text_separator')
			);			
			

			
			$this->data['logged'] = $this->customer->isLogged();
			
			$this->data['text_close_time'] = $this->language->get('text_close_time');
			$this->data['login'] = $this->url->link('account/login');
			
			$this->data['text_winner'] = $this->language->get('text_winner');
			$this->data['text_part'] = $this->language->get('text_part');
			$this->data['text_current_price'] = $this->language->get('text_current_price');
			
			$this->data['text_min_step'] = $this->language->get('text_min_step');
			
			$this->data['productname'] = $product_info['name'];
			
			
			if ((float)$product_info['startbidid']) {
				$this->data['startbidid'] = $product_info['startbidid'];
			} else {
				$this->data['startbidid'] = false;
			}
		
			
			if ((float)$product_info['startbidprice']) {
				$this->data['startbidprice'] = $this->currency->format($this->tax->calculate($product_info['startbidprice'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			$this->data['clearstartbidprice'] = $product_info['startbidprice'];
			} else {
				$this->data['startbidprice'] = false;
				$this->data['clearstartbidprice'] = 0;
			}
					
			if ((float)$product_info['endbidtime']) {
				$this->data['endbidtime'] = $product_info['endbidtime'];
			} else {
				$this->data['endbidtime'] = "";
			}
			
			
			if($this->customer->isLogged()){
			$this->data['customerid'] = $this->customer->getId();
			$this->data['firstname'] = $this->customer->getFirstName();
			$this->data['email'] = $this->customer->getEmail();
			}else{
			$this->data['customerid'] ='';
			}
			
			
			
			
			$this->data['checkstatusbid'] = $this->model_catalog_product->checkBidstatus($this->request->get['product_id']);
			
			$this->data['checkBidstate'] = $this->model_catalog_product->checkBidstate($this->request->get['product_id']);
			
			
			$this->data['checkwinner'] = $this->model_catalog_product->checkcustomerwinner($this->request->get['product_id']);
			
			
			
			$this->data['checkwinner1'] = $this->model_catalog_product->checkwinner($this->request->get['product_id']);

		
			
			
			$this->data['minofferstep'] = $this->model_catalog_product->minofferstep($this->request->get['product_id']);
			
			$this->data['minoffersteps'] = $this->currency->format($this->data['minofferstep']);
			
			
			$this->data['selectmaxcustomerbid'] = $this->model_catalog_product->checkforMaxProductBids($this->request->get['product_id']);
		
			$this->data['checkforcustomerbid'] = $this->model_catalog_product->checkforCustomerBid($this->request->get['product_id'],$this->customer->getId());
			
			$countsubscribe = $this->model_catalog_product->countsubscribe($this->request->get['product_id'],$this->customer->getId());
			
			$this->data['checksub'] = $countsubscribe;
			
		if($countsubscribe>0){	
		$this->data['getsubscribe'] = $this->model_catalog_product->getsubscribe($this->request->get['product_id'],$this->customer->getId());
		}
		
		if($this->customer->isLogged()){
		$this->data['namesetting'] = $this->model_catalog_product->namesettings();
		
		
		
		}else{
		$this->data['namesetting'] ="";
		}
			
			
			
			
		 $this->data['maxcustomerbid'] = $this->model_catalog_product->forMaxProductBids($this->request->get['product_id']);
			
		$this->data['maxpbids'] = $this->model_catalog_product->forMaxProductBids1($this->request->get['product_id']);
			
			
			
			
			
		$this->data['countCustomerBids'] = $this->model_catalog_product->countCustomerBids($this->request->get['product_id']);
		
		$this->data['countproductbids'] = $this->model_catalog_product->forMaxProductBidstable($this->request->get['product_id']);
			
		if($this->data['countCustomerBids'] > 0){
						
				$this->data['mincustomerbid'] = $this->data['maxcustomerbid']+ $this->data['minofferstep'];
				
				$this->data['maxcustomerbidamt'] = $this->currency->format($this->data['maxcustomerbid']);
				
				}else {
				
				$this->data['mincustomerbid'] = $this->data['clearstartbidprice']+$this->data['minofferstep'];
				
				$this->data['maxcustomerbidamt'] = $this->currency->format($this->data['clearstartbidprice']);
			
			
			}
			
		
			
		
		$this->data['config_bid'] = $this->config->get('config_bid');
		$this->data['config_autobid'] = $this->config->get('config_autobid');

		$this->data['config_buy_now'] = $this->config->get('config_buy_now');



		$this->data['config_allow_subscriptions_on'] = $this->config->get('config_allow_subscriptions_on');
		$this->data['config_subscriptions_type'] = $this->config->get('config_subscriptions_type');

		$this->data['config_subscriptions_confirmation_on'] = $this->config->get('config_subscriptions_confirmation_on');



		$this->load->model('account/activity');

		$this->data['bids'] = $this->model_account_activity->getTotalbids1($this->request->get['product_id']);

		$this->data['rbids'] = $this->model_account_activity->getbids1($this->request->get['product_id']);
		
		
		$this->data['bids1'] = $this->model_account_activity->getTotalbids1($this->request->get['product_id']);

		$this->data['rbids1'] = $this->model_account_activity->getbids1($this->request->get['product_id']);

			
			
					
			
			if(isset($this->request->post['accept_customer_buy']) && $this->data['checkforcustomerbid'] != 0){
				$this->data['acceptbuy'] = $this->model_catalog_product->customerAcceptBuying($this->request->get['product_id'],$this->customer->getId());			
			}else{
				$this->data['acceptbuy'] = 0;			
			}
			
			
			
			
			
			
		

			
			$this->document->setTitle($product_info['name']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
			

				$this->data['shippable'] = $product_info['shipping'];
			
			$this->data['heading_title'] = $product_info['name'];
			
			$this->data['text_select'] = $this->language->get('text_select');
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_reward'] = $this->language->get('text_reward');
			$this->data['text_points'] = $this->language->get('text_points');	
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_stock'] = $this->language->get('text_stock');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_option'] = $this->language->get('text_option');
			$this->data['text_qty'] = $this->language->get('text_qty');
			$this->data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$this->data['text_or'] = $this->language->get('text_or');
			$this->data['text_write'] = $this->language->get('text_write');
			$this->data['text_note'] = $this->language->get('text_note');
			$this->data['text_share'] = $this->language->get('text_share');
			$this->data['text_wait'] = $this->language->get('text_wait');
			$this->data['text_tags'] = $this->language->get('text_tags');
			
			$this->data['entry_name'] = $this->language->get('entry_name');
			$this->data['entry_review'] = $this->language->get('entry_review');
			$this->data['entry_rating'] = $this->language->get('entry_rating');
			$this->data['entry_good'] = $this->language->get('entry_good');
			$this->data['entry_bad'] = $this->language->get('entry_bad');
			$this->data['entry_captcha'] = $this->language->get('entry_captcha');
			
			$this->data['button_cart'] = $this->language->get('button_cart');
			$this->data['button_wishlist'] = $this->language->get('button_wishlist');
			$this->data['button_compare'] = $this->language->get('button_compare');			
			$this->data['button_upload'] = $this->language->get('button_upload');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->load->model('catalog/review');

			$this->data['tab_description'] = $this->language->get('tab_description');
			$this->data['tab_attribute'] = $this->language->get('tab_attribute');
			$this->data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

				if ($this->config->get('msconf_comments_enable')) {
					$this->language->load('multiseller/multiseller');
					$this->data['tab_comments'] = sprintf($this->language->get('ms_comments_tab_comments'), $this->MsLoader->MsComments->getTotalComments(array(
						'product_id' => $this->request->get['product_id'],
						'displayed' => 1
					)));
					
					$this->document->addScript('catalog/view/javascript/ms-comments.js');
					if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/ms-comments.css')) {
						$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/ms-comments.css');
					} else {
						$this->document->addStyle('catalog/view/theme/default/stylesheet/ms-comments.css');
					}
				}
			

				if ($this->config->get('msconf_product_shipping_cost_estimation')) {
					$this->data['geo_zones'] = $this->MsLoader->MsShipping->getGeoZones();
					$this->document->addScript('catalog/view/javascript/shipping-estimation.js');
				}
			
			$this->data['tab_related'] = $this->language->get('tab_related');
			
			$this->data['product_id'] = $this->request->get['product_id'];
			$this->data['manufacturer'] = $product_info['manufacturer'];
			$this->data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$this->data['model'] = $product_info['model'];
			$this->data['reward'] = $product_info['reward'];
			$this->data['points'] = $product_info['points'];
			
			if ($product_info['quantity'] <= 0) {
				$this->data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$this->data['stock'] = $product_info['quantity'];
			} else {
				$this->data['stock'] = $this->language->get('text_instock');
			}
			
			$this->load->model('tool/image');

			if ($product_info['image']) {
				$this->data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$this->data['popup'] = '';
			}
			
			if ($product_info['image']) {
				$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
			} else {
				$this->data['thumb'] = '';
			}
			
			$this->data['images'] = array();
			
			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
			
			foreach ($results as $result) {
				$this->data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
				);
			}	
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$this->data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['price'] = false;
			}
						
			if ((float)$product_info['special']) {
				$this->data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['special'] = false;
			}
			
			if ($this->config->get('config_tax')) {
				$this->data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$this->data['tax'] = false;
			}
			
			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
			
			$this->data['discounts'] = array(); 
			
			foreach ($discounts as $discount) {
				$this->data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}
			
			$this->data['options'] = array();
			
			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) { 
				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') { 
					$option_value_data = array();
					
					foreach ($option['option_value'] as $option_value) {
						if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
							if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
								$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
							} else {
								$price = false;
							}
							
							$option_value_data[] = array(
								'product_option_value_id' => $option_value['product_option_value_id'],
								'option_value_id'         => $option_value['option_value_id'],
								'name'                    => $option_value['name'],
								'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
								'price'                   => $price,
								'price_prefix'            => $option_value['price_prefix']
							);
						}
					}
					
					$this->data['options'][] = array(
						'product_option_id' => $option['product_option_id'],
						'option_id'         => $option['option_id'],
						'name'              => $option['name'],
						'type'              => $option['type'],
						'option_value'      => $option_value_data,
						'required'          => $option['required']
					);					
				} elseif ($option['type'] == 'text' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
					$this->data['options'][] = array(
						'product_option_id' => $option['product_option_id'],
						'option_id'         => $option['option_id'],
						'name'              => $option['name'],
						'type'              => $option['type'],
						'option_value'      => $option['option_value'],
						'required'          => $option['required']
					);						
				}
			}
							
			if ($product_info['minimum']) {
				$this->data['minimum'] = $product_info['minimum'];
			} else {
				$this->data['minimum'] = 1;
			}
			
			$this->data['review_status'] = $this->config->get('config_review_status');
			$this->data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$this->data['rating'] = (int)$product_info['rating'];
			$this->data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
			
			$this->data['products'] = array();
			
			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				} else {
					$image = false;
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
							
				$this->data['products'][] = array(
					'product_id' => $result['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $result['name'],
					'price'   	 => $price,
					'special' 	 => $special,
					'rating'     => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}	
			
			$this->data['tags'] = array();
			
			if ($product_info['tag']) {		
				$tags = explode(',', $product_info['tag']);
				
				foreach ($tags as $tag) {
					$this->data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}
			
			$this->model_catalog_product->updateViewed($this->request->get['product_id']);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/product.tpl';
			} else {
				$this->template = 'default/template/product/product.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
						
			$this->response->setOutput($this->render());
		} else {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
						
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}	
						
			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}			

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}	
					
			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}
							
			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}
					
			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}
			
			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}	
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
														
      		$this->data['breadcrumbs'][] = array(
        		'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $product_id),
        		'separator' => $this->language->get('text_separator')
      		);			
		
      		$this->document->setTitle($this->language->get('text_error'));

      		$this->data['heading_title'] = $this->language->get('text_error');

      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
						
			$this->response->setOutput($this->render());
    	}
  	}
	
	public function stock() {
		$this->language->load('product/product');
		$this->load->model('catalog/product');
		$this->data['stock'] = '';
		
		$product_id = empty($this->request->get['product_id']) ? 0 : (int) $this->request->get['product_id'];
		if($product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			$stock = !isset($product_info['stock_status']) ? $product_info['stock'] : $product_info['stock_status'];
			
			if($product_info) {
				if ($product_info['quantity'] <= 0) {
					$this->data['stock'] = $stock;
				} elseif ($this->config->get('config_stock_display')) {
					$this->data['stock'] = $product_info['quantity'];
				} else {
					$this->data['stock'] = $this->language->get('text_instock');
				}
			}
		}
		
		die($this->data['stock']);
	}
	public function review() {
    	$this->language->load('product/product');
		
		$this->load->model('catalog/review');

		$this->data['text_on'] = $this->language->get('text_on');
		$this->data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}  
		
		$this->data['reviews'] = array();
		
		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);
			
		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);
      		
		foreach ($results as $result) {
        	$this->data['reviews'][] = array(
        		'author'     => $result['author'],
				'text'       => $result['text'],
				'rating'     => (int)$result['rating'],
        		'reviews'    => sprintf($this->language->get('text_reviews'), (int)$review_total),
        		'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
        	);
      	}			
			
		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');
			
		$this->data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/product/review.tpl';
		} else {
			$this->template = 'default/template/product/review.tpl';
		}
		
		$this->response->setOutput($this->render());
	}
	
	public function write() {
		$this->language->load('product/product');
		
		$this->load->model('catalog/review');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}
			
			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}
	
			if (empty($this->request->post['rating'])) {
				$json['error'] = $this->language->get('error_rating');
			}
	
			if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
				$json['error'] = $this->language->get('error_captcha');
			}
				
			if (!isset($json['error'])) {
				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);
				
				$json['success'] = $this->language->get('text_success');
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}
	
	public function upload() {
		$this->language->load('product/product');
		
		$json = array();
		
		if (!empty($this->request->files['file']['name'])) {
			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));
			
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 64)) {
        		$json['error'] = $this->language->get('error_filename');
	  		}	  	

			// Allowed file extension types
			$allowed = array();
			
			$filetypes = explode("\n", $this->config->get('config_file_extension_allowed'));
			
			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}
			
			if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
       		}	
			
			// Allowed file mime types		
		    $allowed = array();
			
			$filetypes = explode("\n", $this->config->get('config_file_mime_allowed'));
			
			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}
							
			if (!in_array($this->request->files['file']['type'], $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
			}
						
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}
		
		if (!$json && is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
			$file = basename($filename) . '.' . md5(mt_rand());
			
			// Hide the uploaded file name so people can not link to it directly.
			$json['file'] = $this->encryption->encrypt($file);
			
			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);
						
			$json['success'] = $this->language->get('text_upload');
		}	
		
		$this->response->setOutput(json_encode($json));		
	}
}
?>