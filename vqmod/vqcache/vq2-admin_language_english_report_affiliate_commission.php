<?php
// Heading
$_['heading_title']     = 'Affiliate Commission Report';

// Column
$_['column_affiliate']  = 'Affiliate Name';
$_['column_email']      = 'E-Mail';
$_['column_status']     = 'Status';
$_['column_commission'] = 'Commission';
$_['column_orders']     = 'No. Orders';
$_['column_total']      = 'Total';
$_['column_action']     = 'Action';

// Entry
$_['entry_date_start']  = 'Date Start:';
$_['entry_date_end']    = 'Date End:';


//+mod by yp start
$_['column_current_balance'] = 'Current Balance';
$_['text_no_payout_account'] = 'This affiliate have not set up any payout account yet !';
$_['entry_current_balance'] = 'Current Balance &gt;= :';
//+mod by yp end			


?>