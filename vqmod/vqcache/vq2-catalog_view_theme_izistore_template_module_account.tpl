<div class="box infoModule">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <ul>
      <?php if (!$logged) { ?>
      <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
      <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
      <li><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></li>
      <?php } ?>
      <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
      <?php if ($logged) { ?>
      <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      <?php } ?>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
      <?php if ($logged) { ?>
      <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
      <?php } ?>

	  <?php //+mod by yp start
		if ($text_link_to_affiliate) { 
			if($accc_show_website) {
		?>
		<li>
		<a href="javascript:;" onclick="javascript: if(!$('#accc_website_div').is(':hidden')) {$('#accc_form').submit();} else {$('#accc_website_div').show();} return false;"><?php echo $text_link_to_affiliate; ?></a>
		<form method="post" action="<?php echo $link_to_affiliate;?>" id="accc_form" <?php if($accc_website_required) {?>onsubmit="javascript:if($('[name=\'website\']').val().length < 1) {alert('<?php echo $accc_error_website;?>');return false;} else {return true;}"<?php }?>>		
		<div id="accc_website_div" style="display:none">
		<?php if($accc_website_required) {?><span class="required">*</span> <?php }?>
		<?php echo $accc_entry_website;?><br />
		<?php if($accc_website_textarea) {?>
		<textarea name="website"></textarea>
		<?php } else {?>
		<input type="text" name="website" />		
		<?php }?>
		<br />
		<input type="hidden" name="confirm" value="1" />
		<input type="submit" value="<?php echo $text_link_to_affiliate; ?>" />
		</div>
		</form>
		</li>
		<?php } else {?>
	  <li><a href="<?php echo $link_to_affiliate; ?>"><?php echo $text_link_to_affiliate; ?></a></li>
	  <?php }
		} //+mod by yp end
	  ?>


				<br />
				<li style="list-style-type: none"><b><?php echo $ms_account_seller_account; ?></b></li>
				<?php if ($ms_seller_created && $this->MsLoader->MsSeller->getStatus($this->customer->getId()) == MsSeller::STATUS_ACTIVE) { ?>
					<li><a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>"><?php echo $ms_account_dashboard; ?></a></li>
				<?php } ?>
				
				<li><a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>"><?php echo $ms_seller_created ? $ms_account_sellerinfo : $ms_account_sellerinfo; ?></a></li>
				
				<?php if ($ms_seller_created && $this->MsLoader->MsSeller->getStatus($this->customer->getId()) == MsSeller::STATUS_ACTIVE) { ?>
					<li><a href="<?php echo $this->url->link('seller/account-product/create', '', 'SSL'); ?>"><?php echo $ms_account_newproduct; ?></a></li>
					<li><a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>"><?php echo $ms_account_products; ?></a></li>

				<?php if ($this->config->get('msconf_enable_shipping') > 0) { ?>
					<li><a href="<?php echo $ms_link_shipping_settings; ?>"><?php echo $ms_account_shipping_settings; ?></a></li>
				<?php } ?>
			
					<li><a href="<?php echo $this->url->link('seller/account-order', '', 'SSL'); ?>"><?php echo $ms_account_orders; ?></a></li>
					<li><a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>"><?php echo $ms_account_transactions; ?></a></li>
					<?php if ($this->config->get('msconf_allow_withdrawal_requests')) { ?>
					<li><a href="<?php echo $this->url->link('seller/account-withdrawal', '', 'SSL'); ?>"><?php echo $ms_account_withdraw; ?></a></li>
					<?php } ?>
				<?php } ?>
			
    </ul>
  </div>
</div>
