<?php
// Heading
$_['heading_title']          = 'Products'; 

// Text  
$_['text_success']           = 'Success: You have modified products!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse';
$_['text_clear']             = 'Clear';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product Name:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'Description:';
$_['entry_store']            = 'Stores:';
$_['entry_keyword']          = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:<br/><span class="help">Stock Keeping Unit</span>';
$_['entry_upc']              = 'UPC:<br/><span class="help">Universal Product Code</span>';
$_['entry_ean']              = 'EAN:<br/><span class="help">European Article Number</span>';
$_['entry_jan']              = 'JAN:<br/><span class="help">Japanese Article Number</span>';
$_['entry_isbn']             = 'ISBN:<br/><span class="help">International Standard Book Number</span>';
$_['entry_mpn']              = 'MPN:<br/><span class="help">Manufacturer Part Number</span>';
$_['entry_location']         = 'Location:';
$_['entry_shipping']         = 'Requires Shipping:'; 
$_['entry_manufacturer']     = 'Manufacturer:<br /><span class="help">(Autocomplete)</span>';
$_['entry_date_available']   = 'Date Available:';
$_['entry_quantity']         = 'Quantity:';
$_['entry_minimum']          = 'Minimum Quantity:<br/><span class="help">Force a minimum ordered amount</span>';
$_['entry_stock_status']     = 'Out Of Stock Status:<br/><span class="help">Status shown when a product is out of stock</span>';
$_['entry_price']            = 'Price:';
$_['entry_tax_class']        = 'Tax Class:';
$_['entry_points']           = 'Points:<br/><span class="help">Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.</span>';
$_['entry_option_points']    = 'Points:';
$_['entry_subtract']         = 'Subtract Stock:';
$_['entry_weight_class']     = 'Weight Class:';
$_['entry_weight']           = 'Weight:';
$_['entry_length']           = 'Length Class:';
$_['entry_dimension']        = 'Dimensions (L x W x H):';
$_['entry_image']            = 'Image:';
$_['entry_customer_group']   = 'Customer Group:';
$_['entry_date_start']       = 'Date Start:';
$_['entry_date_end']         = 'Date End:';
$_['entry_priority']         = 'Priority:';
$_['entry_attribute']        = 'Attribute:';
$_['entry_attribute_group']  = 'Attribute Group:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Option:';
$_['entry_option_value']     = 'Option Value:';
$_['entry_required']         = 'Required:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_category']         = 'Categories:<br /><span class="help">(Autocomplete)</span>';
$_['entry_filter']           = 'Filters:<br /><span class="help">(Autocomplete)</span>';
$_['entry_download']         = 'Downloads:<br /><span class="help">(Autocomplete)</span>';
$_['entry_related']          = 'Related Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']          	 = 'Product Tags:<br /><span class="help">comma separated</span>';
$_['entry_reward']           = 'Reward Points:';
$_['entry_layout']           = 'Layout Override:';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';

				$_['tab_auction']           = 'Auction';
				$_['entry_auction']           = 'Auction completion';$_['entry_end_time']         = 'After End Time:';$_['entry_end_desc']          = 'Auction will finish once auction end time is reached. End time must be defined.';$_['entry_max']          	 = 'When max. price is reached';$_['entry_max_desc']           = 'Auction will finish once auction max price is reached. Max price must be defined.	';$_['auction_settings']           = 'Auction Settings';$_['auction_status']           = 'Active <br/> If disabled, product will regain its original data';$_['auction_start']         = 'Start Time:';$_['auction_start_desc']          = 'Auction start date. If it is lesser than current date, counter will display time, left to the auction.';$_['auction_end']          	 = 'End time:';$_['auction_end_desc']           = 'Auction end date. Necessary if end time condition is set in auction completion section.';$_['auction_starting']           = 'Starting bid:';$_['auction_tax']           = 'Taxes:';$_['auction_starting_desc']         = 'Auction starts from this price';$_['auction_tax_desc']          = 'You may also add taxes to auction winners price (taxes will be displayed along with current price)';$_['auction_max']           = 'Max. price:';$_['auction_min']           = 'Min. offer step:';$_['auction_max_desc']         = 'Auction ends at this price if max. price condition is set in auction completion section';$_['auction_min_desc']          = 'Minimal bidding amount. If left blank, default value 1 will be used';$_['auction_auto_desc']           = 'Automatic bid increment amount. If left blank, defaul value 1 will be used. Should not be higher than min. offer step.';$_['auction_time']           = 'Time interval between bids:';$_['auction_time_desc']         = 'Time interval between bids. If left blank, defaul value 1s will be used ';$_['auction_auto']          = 'Automatic bid increment amount:';$_['error_bid_price']             = 'Auction Start Price is required!';$_['error_start']             = 'Auction start date is required!';$_['error_end_time']             = 'Auction end date is required!';

$_['error_max_price']             = 'Auction max price is required!';

			


//+mod by yp start
$_['column_mta_scheme']      = 'Commission Scheme';
$_['text_edit_mta_schemes']  = 'Edit Custom Commissions';
$_['entry_mta_schemes'] = 'Custom Commission';
$_['text_hide'] = 'Hide';
$_['text_default'] = 'Default';
$_['tab_scheme'] = 'Commission';
$_['entry_product'] = 'Product:';
$_['entry_coupons'] = 'Coupons:';
$_['text_aff_schemes'] = 'Custom Schemes for Selected Affiliates';
$_['column_affiliates'] = 'Affiliates';
$_['column_coupon'] = 'Coupon';
$_['text_edit'] = 'Edit';
$_['text_add'] = 'Add';

$_['oy_close'] = 'Close';
$_['oy_title'] = 'Affiliates - Click to Select';
$_['oy_loading'] = 'Loading data from server ...';
$_['oy_id'] = 'ID';
$_['oy_name'] = 'Name';
$_['oy_email'] = 'Email';
$_['oy_scheme'] = 'Scheme';
$_['oy_level'] = 'Level';
$_['oy_balance'] = 'Balance';
$_['oy_date_added'] = 'Date Added';

$_['button_add_selected'] = 'Add Selected';
$_['button_remove_selected'] = 'Remove Selected';
$_['error_commission_price'] = 'Error: Total commission can not be higher than the price of a product!';
//+mod by yp end


?>