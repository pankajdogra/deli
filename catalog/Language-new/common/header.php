<?php
// Text
$_['text_home']           = 'Home';
$_['text_wishlist']       = 'Wish List (%s)';
$_['text_shopping_cart']  = 'Shopping Cart';
$_['text_search']         = 'Search';
$_['text_welcome']        = '<li><a href="%s">Login</a></li><li><a href="%s">Create an account</a></li>';
$_['text_logged']         = 'Hi <strong rel="%s">%s! -</strong>  <a href="%s" class="dropArrow">Logout</a>';
$_['text_account']        = 'My Account';
$_['text_checkout']       = 'Checkout';
?>