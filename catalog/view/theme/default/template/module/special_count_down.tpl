<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Cuprum:400,700' rel='stylesheet' type='text/css'>

<style type="text/css">
#column-left .hasCountdown ,#column-left .button1{
width:92% !important;
display:block;
position:relative;
}
#column-left .hasCountdown{
	margin-bottom:15px;
	
}
#specialcount {
text-align:center;
}
#column-left .countoffer ,#column-left .countdate{
font-size:12px !important;
}
.hasCountdown {
	width:99%;
	margin-bottom:8px;
}
.box-product {
    text-align: center !important;
	}
div.cart {
margin-top:2px !important;
margin-bottom:4px !important;
}
.countoffer , .countdate {
font-family:'Ringbearer';
font-size:15px;

}
.limitedoffer {
font-family: 'Ringbearer' !important;
}
.special2{
font-weight:lighter !important;
}
.special2 {
font-family:'Ringbearer';
font-size:16px;
font-weight:lighter;
line-height :120%;
}
.specialh {
min-height:35px;
}
.specialh a {
font-family: 'Ringbearer';
font-size:15px;
color:#38B0E3;
}
#specialcount {
	font-family: 'Ringbearer';
	font-size: 11px;
	color: #F07800;
	padding: 4px 1px;
	background: 
		#15161B;
	background: -webkit-gradient(
		linear, left top, left bottom, 
		from(#15161B),
		to(#15161B));
	-moz-border-radius: 17px;
	-webkit-border-radius: 17px;
	border-radius: 17px;
	border: 1px solid #000;
	-moz-box-shadow:
		0px 1px 0px rgba(000,000,000,0.2),
		inset 0px 1px 0px rgba(255,255,255,0.5);
	-webkit-box-shadow:
		0px 1px 0px rgba(000,000,000,0.2),
		inset 0px 1px 0px rgba(255,255,255,0.5);
	box-shadow:
		0px 1px 0px rgba(000,000,000,0.2),
		inset 0px 1px 0px rgba(255,255,255,0.5);
	/* text-shadow:
		0px 1px 0px rgba(255,255,255,0.3),
		0px 1px 0px rgba(255,255,255,0.3); */
		margin-bottom:2px;
}


#specialcount1 {
	font-family: 'Ringbearer';
	font-size: 13px;
	color: white;
	padding: 12px;
	
	background: #FF0000;
	background: -webkit-gradient(
		linear, left top, left bottom, 
		from(#FF0000),
		to(#FF0000));
	-moz-border-radius:17px;
	-webkit-border-radius: 17px;
	border-radius: 17px;
	border: 1px solid #760202;
	-moz-box-shadow:
		0px 1px 0px rgba(000,000,000,0.2),
		inset 0px 1px 0px rgba(255,255,255,0.5);
	-webkit-box-shadow:
		0px 1px 0px rgba(000,000,000,0.2),
		inset 0px 1px 0px rgba(255,255,255,0.5);
	box-shadow:
		0px 1px 0px rgba(000,000,000,0.2),
		inset 0px 1px 0px rgba(255,255,255,0.5);
	text-shadow:
		0px 1px 0px rgba(255,255,255,0.3),
		0px 1px 0px rgba(255,255,255,0.3);
		margin-bottom:2px;
}

.button1 {
width:99%;
	font-family:'Ringbearer';
	font-size: 14px;
	color: #ffffff;
	padding: 2px 7px;
	background: -moz-linear-gradient(
		top,
		#33a0e8 0%,
		#2180ce);
	background: -webkit-gradient(
		linear, left top, left bottom, 
		from(#33a0e8),
		to(#2180ce));
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	border: 1px solid #18649a;
	-moz-box-shadow:
		0px 1px 1px rgba(000,000,000,0.3),
		inset 0px 1px 0px rgba(131,197,241,1);
	-webkit-box-shadow:
		0px 1px 1px rgba(000,000,000,0.3),
		inset 0px 1px 0px rgba(131,197,241,1);
	box-shadow:
		0px 1px 1px rgba(000,000,000,0.3),
		inset 0px 1px 0px rgba(131,197,241,1);
	text-shadow:
		0px 1px 2px rgba(053,086,130,1),
		0px 0px 0px rgba(000,000,000,0);
}

.orange:hover {
	background: #f47c20;
	background: -webkit-gradient(linear, left top, left bottom, from(#f88e11), to(#f06015));
	background: -moz-linear-gradient(top,  #f88e11,  #f06015);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#f88e11', endColorstr='#f06015');
}
.orange:active {
	color: #fcd3a5;
	background: -webkit-gradient(linear, left top, left bottom, from(#f47a20), to(#faa51a));
	background: -moz-linear-gradient(top,  #f47a20,  #faa51a);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#f47a20', endColorstr='#faa51a');
}
.countdown_rtl {
	direction: rtl;
}
.countdown_holding span {
	color: #888;
}
.countdown_row {
	clear: both;
	width: 100%;
	padding: 0px 2px;
	text-align: center;
}
.countdown_show1 .countdown_section {
	width: 98%;
}
.countdown_show2 .countdown_section {
	width: 48%;
}
.countdown_show3 .countdown_section {
	width: 32.5%;
}
.countdown_show4 .countdown_section {
	width: 24.5%;
}
.countdown_show5 .countdown_section {
	width: 19.5%;
}
.countdown_show6 .countdown_section {
	width: 16.25%;
}
.countdown_show7 .countdown_section {
	width: 14%;
}
.box-product > div{
/*width:160px !important;*/
}
.countdown_section {
	display: block;
	width:98%;
	float: left;
	font-size: 9px;
	text-align: center;
}
.countdown_amount {
	font-size: 14px;
	color:orange;
}
.countdown_descr {
	display: block;
	font-size: 8px;
	width: 100%;
}

</style>

<div class="box">
 <!-- <div class="box-heading limitedoffer"><?php echo $heading_title; ?></div>-->
  <div class="box-content">
    <div class="box-product">
      <?php foreach ($products as $product) { ?>
      
        <?php if ($product['thumb']) { ?>
	<!--	<?php if ($product['specialper']) { ?>
		<span class="thumbsup-badge badge-green"><span class="percent">-<?php echo round($product['specialper']);?>%<br/></span></span>
	 <?php }?>-->
       <!-- <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name specialh"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        <?php if ($product['price']) { ?>
        <div class="price special2">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
        </div>
        <?php } ?>
		
		
		<div id="countdown_<?php echo $product['product_id'] ; ?>"></div>-->
		
       
<!--	   <div class="cart"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button1" /></div>-->

     
      <?php } ?>
    </div>
  </div>
</div>
<script type="text/javascript">
<?php foreach ($products as $product) { ?>
$(document).ready(function() {
var mon=<?php echo($product['mon']);  ?>;
mon=mon-1;
text_expiry = '<?php echo $text_expiry ; ?>';
text = '<div id="specialcount"><span class="countoffer">'+text_expiry+' </span><span class="countdate">{dn} {dl} {hnn}{sep}{mnn}{sep}{snn} </span> {desc} </div>';

var close = <?php echo($product['checkstatebid']);  ?>;


if(close==0){
 $('#countdown_<?php echo $product['product_id'] ; ?>').countdown({until: new Date( <?php echo($product['year']);  ?> ,mon , <?php echo($product['day']);  ?> , <?php echo($product['hours']);  ?> , <?php echo($product['minutes']);  ?> , <?php echo($product['seconds']);  ?> , 0 ), layout: text, 
    description: '' }); 
	
}else{

$('#countdown_<?php echo $product['product_id'] ; ?>').append('<p id="specialcount1">Auction Ended</p>');

}
	
	
	
  });
    <?php } ?>
</script>