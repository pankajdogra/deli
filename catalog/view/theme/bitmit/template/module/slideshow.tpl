<section class="container-fluid banbg">
  <aside class="row">
    <div class="carousel slide container" id="myCarousel">
      <div class="carousel-inner">
       <!-- <div class="item active">-->
          <div id="slideshow<?php echo $module; ?>" class="item active" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;">
            <?php foreach ($banners as $banner) { ?>
            <?php if ($banner['link']) { ?>
            <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
            <?php } else { ?>
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
            <?php } ?>
            <?php } ?>
          </div>
        </div>
<!--      </div>
-->    </div>
  </aside>
</section>
<script type="text/javascript"><!--
$(document).ready(function() {
$('#slideshow<?php echo $module; ?>').nivoSlider();
});
--></script>
