<section class="container-fluid banbg">
<aside class="row">
<div class="carousel slide container" id="myCarousel">
      <div class="carousel-inner">
        <div class="item active">
<div class="flexslider">
 	<ul id="slideshow<?php echo $module; ?>" class="slides">
    <?php foreach ($banners as $banner) { ?>
		<?php if ($banner['link']) { ?>
        	<li><a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a></li>
        <?php } else { ?>
    		<li><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></li>
    <?php } ?>
    <?php } ?>
  </ul>  
</div>
</div>
</div>
</div>
</aside>
</section>
  

<script type="text/javascript">
	 $(window).load(function() {
			$('.flexslider').flexslider({
			  animation: slideAnim,
			  slideshow: true,
			  slideshowSpeed: slideSpeed,
			  manualControls: '.controls li',
			  controlsContainer: '.flexslider'
			});
	});
</script>
<div class="clear"></div>
<div class="clear"></div>