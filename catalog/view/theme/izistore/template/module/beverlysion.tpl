<section class="container-fluid banbg">
<aside class="row">
<div class="carousel slide" id="myCarousel">
      <div class="carousel-inner">
<div class="item active">

<div id="slideshow<?php echo $module; ?>" class="banner_main" style="width: 96%; height: <?php echo $height; ?>px;float:right;padding-right:5%">
<ul>
<?php foreach ($banners as $banner) { ?>
<?php $p1 = (!empty($banner['sm_pic_1'])) ?  '<div class="caption '. $banner['sm_pic_1_data_cls'].'" data-x="'. $banner['sm_pic_1_data_x'].'" data-y="'. $banner['sm_pic_1_data_y'].'" data-speed="'. $banner['sm_pic_1_data_sp'].'"  data-start="'. $banner['sm_pic_1_data_st'].'"  data-easing="'. $banner['sm_pic_1_data_ea'].'"  ><a href="'. $banner['sm_pic_1_url'].'"><img style="border-radius: 0px 0px 0px 0px;" src="'. $banner['sm_pic_1'].'" alt="Image 1" height="'. $banner['sm_pic_1_data_h'].'" width="'. $banner['sm_pic_1_data_w'].'"></a></div>' : ''; ?>
<?php $p2 = (!empty($banner['sm_pic_2'])) ?  '<div class="caption '. $banner['sm_pic_2_data_cls'].'" data-x="'. $banner['sm_pic_2_data_x'].'" data-y="'. $banner['sm_pic_2_data_y'].'" data-speed="'. $banner['sm_pic_2_data_sp'].'"  data-start="'. $banner['sm_pic_2_data_st'].'"  data-easing="'. $banner['sm_pic_2_data_ea'].'"  ><a href="'. $banner['sm_pic_2_url'].'"><img style="border-radius: 0px 0px 0px 0px;" src="'. $banner['sm_pic_2'].'" alt="Image 1" height="'. $banner['sm_pic_2_data_h'].'" width="'. $banner['sm_pic_2_data_w'].'"></a></div>' : ''; ?>
<?php $p3 = (!empty($banner['sm_pic_3'])) ?  '<div class="caption '. $banner['sm_pic_3_data_cls'].'" data-x="'. $banner['sm_pic_3_data_x'].'" data-y="'. $banner['sm_pic_3_data_y'].'" data-speed="'. $banner['sm_pic_3_data_sp'].'"  data-start="'. $banner['sm_pic_3_data_st'].'"  data-easing="'. $banner['sm_pic_3_data_ea'].'"  ><img style="border-radius: 0px 0px 0px 0px;" src="'. $banner['sm_pic_3'].'" alt="Image 1" height="'. $banner['sm_pic_3_data_h'].'" width="'. $banner['sm_pic_3_data_w'].'"></div>' : ''; ?>
<?php $p4 = (!empty($banner['sm_pic_4'])) ?  '<div class="caption '. $banner['sm_pic_4_data_cls'].'" data-x="'. $banner['sm_pic_4_data_x'].'" data-y="'. $banner['sm_pic_4_data_y'].'" data-speed="'. $banner['sm_pic_4_data_sp'].'"  data-start="'. $banner['sm_pic_4_data_st'].'"  data-easing="'. $banner['sm_pic_4_data_ea'].'"  ><img style="border-radius: 0px 0px 0px 0px;" src="'. $banner['sm_pic_4'].'" alt="Image 1" height="'. $banner['sm_pic_4_data_h'].'" width="'. $banner['sm_pic_4_data_w'].'"></div>' : ''; ?>
<?php $p5 = (!empty($banner['sm_pic_5'])) ?  '<div class="caption '. $banner['sm_pic_5_data_cls'].'" data-x="'. $banner['sm_pic_5_data_x'].'" data-y="'. $banner['sm_pic_5_data_y'].'" data-speed="'. $banner['sm_pic_5_data_sp'].'"  data-start="'. $banner['sm_pic_5_data_st'].'"  data-easing="'. $banner['sm_pic_5_data_ea'].'"  ><img style="border-radius: 0px 0px 0px 0px;" src="'. $banner['sm_pic_5'].'" alt="Image 1" height="'. $banner['sm_pic_5_data_h'].'" width="'. $banner['sm_pic_5_data_w'].'"></div>' : ''; ?>
<?php $c1 = (!empty($banner['caption_1'])) ?  '<div class="caption '. $banner['caption_1_data_cls'].'" data-x="'. $banner['caption_1_data_x'].'" data-y="'. $banner['caption_1_data_y'].'" data-speed="'. $banner['caption_1_data_sp'].'"  data-start="'. $banner['caption_1_data_st'].'"  data-easing="'. $banner['caption_1_data_ea'].'"  ><a href="'. $banner['caption_1_url'].'">'. $banner['caption_1'].'</a></div>' : ''; ?>
<?php $c2 = (!empty($banner['caption_2'])) ?  '<div class="caption '. $banner['caption_2_data_cls'].'" data-x="'. $banner['caption_2_data_x'].'" data-y="'. $banner['caption_2_data_y'].'" data-speed="'. $banner['caption_2_data_sp'].'"  data-start="'. $banner['caption_2_data_st'].'"  data-easing="'. $banner['caption_2_data_ea'].'"  ><a href="'. $banner['caption_2_url'].'">'. $banner['caption_2'].'</a></div>' : ''; ?>
<?php $c3 = (!empty($banner['caption_3'])) ?  '<div class="caption '. $banner['caption_3_data_cls'].'" data-x="'. $banner['caption_3_data_x'].'" data-y="'. $banner['caption_3_data_y'].'" data-speed="'. $banner['caption_3_data_sp'].'"  data-start="'. $banner['caption_3_data_st'].'"  data-easing="'. $banner['caption_3_data_ea'].'"  >'. $banner['caption_3'].'</div>' : ''; ?>
<?php $c4 = (!empty($banner['caption_4'])) ?  '<div class="caption '. $banner['caption_4_data_cls'].'" data-x="'. $banner['caption_4_data_x'].'" data-y="'. $banner['caption_4_data_y'].'" data-speed="'. $banner['caption_4_data_sp'].'"  data-start="'. $banner['caption_4_data_st'].'"  data-easing="'. $banner['caption_4_data_ea'].'"  >'. $banner['caption_4'].'</div>' : ''; ?>
<?php $c5 = (!empty($banner['caption_5'])) ?  '<div class="caption '. $banner['caption_5_data_cls'].'" data-x="'. $banner['caption_5_data_x'].'" data-y="'. $banner['caption_5_data_y'].'" data-speed="'. $banner['caption_5_data_sp'].'"  data-start="'. $banner['caption_5_data_st'].'"  data-easing="'. $banner['caption_5_data_ea'].'"  >'. $banner['caption_5'].'</div>' : ''; ?>
<?php $b1 = (!empty($banner['button_1'])) ?  '<div class="caption sfb" data-x="'. $banner['button_1_data_x'].'" data-y="'. $banner['button_1_data_y'].'" data-speed="'. $banner['button_1_data_sp'].'"  data-start="'. $banner['button_1_data_st'].'"  data-easing="'. $banner['button_1_data_ea'].'"  ><a class="button_bvls '. $banner['button_1_data_cls'].'" href="'. $banner['button_1_data_st'].'">'. $banner['button_1'].'</a></div>' : ''; ?>
<?php $yt = (!empty($banner['youtube_iframe'])) ?  '<div class="'. $banner['youtube_data_cls'].'" data-x="'. $banner['youtube_data_x'].'" data-y="'. $banner['youtube_data_y'].'" data-speed="'. $banner['youtube_data_sp'].'"  data-start="'. $banner['youtube_data_st'].'"  data-easing="'. $banner['youtube_data_ea'].'"  data-autoplay="'. $banner['youtube_autoplay'].'"><iframe src="'. $banner['youtube_iframe'].'" width="'. $banner['youtube_data_w'].'" height="'. $banner['youtube_data_h'].'"></iframe></div>' : ''; ?>
<?php $vm = (!empty($banner['vimeo_iframe'])) ?  '<div class="'. $banner['vimeo_data_cls'].'" data-x="'. $banner['vimeo_data_x'].'" data-y="'. $banner['vimeo_data_y'].'" data-speed="'. $banner['vimeo_data_sp'].'"  data-start="'. $banner['vimeo_data_st'].'"  data-easing="'. $banner['vimeo_data_ea'].'"  data-autoplay="'. $banner['vimeo_autoplay'].'"><iframe src="'. $banner['vimeo_iframe'].'" width="'. $banner['vimeo_data_w'].'" height="'. $banner['vimeo_data_h'].'"></iframe></div>' : ''; ?>
<li data-transition="<?php echo $banner['transicion']; ?>" data-slotamount="<?php echo $banner['slot_amt']; ?>" data-delay="<?php echo $banner['data_delay']; ?>" data-masterspeed="<?php echo $banner['master_speed']; ?>">
    <?php if ($banner['link']) { ?>
   <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
    <?php } else { ?>
<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
    <?php } ?>
    <?php echo $p1 . $p2 . $p3 . $p4 . $p5 . $c1 . $c2 . $c3 . $c4 . $c5 . $b1 . $yt . $vm ;?>
    </li>
    <?php } ?>
</ul>
</div>
</div>
</div>

</div>
</aside>
</section>