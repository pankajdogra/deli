<?php $passURLRoute = (!empty($data['FacebookComments']['URLRoute'])) ? ($data['FacebookComments']['URLRoute'] == $this->request->get['route']) : true; ?>
<?php if($passURLRoute) { ?>
<?php if($data['FacebookComments']['Enabled'] != 'no'): ?>
<?php 
function curPageURL() {
 $pageURL = (empty($_SERVER['HTTPS'])) ? 'http://' : 'https://';
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
?>
<div id="fb-root"></div>
<script>
if (typeof FB == 'undefined') {
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $data['FacebookComments']['APIKey']; ?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
} else {
	FB.XFBML.parse();
}
</script>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $currenttemplate?>/stylesheet/facebookcomments.css" />

<?php if($data['FacebookComments']['showInTab'] == 'no') { ?>
<!-- START FacebookComments -->
<div class="fb-comments" data-href="<?php echo curPageURL(); ?>" data-num-posts="<?php echo $data['FacebookComments']['NumberOfPosts']; ?>" data-width="<?php echo $data['FacebookComments']['Width']; ?>" data-colorscheme="<?php echo $data['FacebookComments']['ColorScheme']; ?>"></div>
<?php } ?>
<style type="text/css">
<?php if($data['FacebookComments']['HideTabs'] != 'no'): ?>
.htabs, .tab-content {
	display:none !important;	
}
<?php endif; ?>
<?php if($data['FacebookComments']['HideTags'] != 'no'): ?>
.tags {
	display:none !important;	
}
<?php endif; ?>
<?php echo htmlspecialchars_decode($data['FacebookComments']['CustomCSS']); ?>
</style>
<!-- END FacebookComments -->



<?php endif; ?>
<?php } ?>
