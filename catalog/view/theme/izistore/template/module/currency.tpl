<script>
$(document).ready(function(){

	// Remove the class of child and grandchild
	// This removes the CSS 'falback'
	$("#nav ul.child").removeClass("child");
	$("#nav ul.grandchild").removeClass("grandchild");
	
	// When a list item that contains an unordered list
	// is hovered on
	$("#nav li").has("ul").hover(function(){

		//Add a class of current and fade in the sub-menu
		$(this).addClass("current").children("ul").fadeIn();
	}, function() {

		// On mouse off remove the class of current
		// Stop any sub-menu animation and set its display to none
		$(this).removeClass("current").children("ul").stop(true, true).css("display", "none");
	});

});</script>
<div id="currency">
		<?php if (count($currencies) > 1) { ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <div class="currency"><!--<p><?php echo $text_currency; ?></p>-->
      <ul id="nav">
      <li>Currency<ul class="child">
       <?php foreach ($currencies as $currency) { ?>
        <?php if ($currency['code'] == $currency_code) { ?>
        <?php if ($currency['symbol_left']) { ?>
        <a title="<?php echo $currency['title']; ?>"><strong><?php echo $currency['symbol_left']; ?></strong></a>
        <?php } else { ?>
        <a title="<?php echo $currency['title']; ?>"><strong><?php echo $currency['symbol_right']; ?></strong></a>
         <?php } ?>
        <?php } else { ?>
        <?php if ($currency['symbol_left']) { ?>
        <li>
         <a title="<?php echo $currency['title']; ?>" onClick="$('input[name=\'currency_code\']').attr('value', '<?php echo $currency['code']; ?>').submit(); $(this).parent().parent().parent().parent().parent().parent().submit();"><?php echo $currency['symbol_left']; ?>  <?php echo $currency['title']; ?></a>
         </li>
         <?php } else { ?>
         <li>
         <a title="<?php echo $currency['title']; ?>" onClick="$('input[name=\'currency_code\']').attr('value', '<?php echo $currency['code']; ?>').submit(); $(this).parent().parent().parent().parent().parent().parent().submit();"><?php echo $currency['symbol_right']; ?>  <?php echo $currency['title']; ?></a>
        </li>
        <?php } ?>
        <?php } ?>
        <?php } ?>
         </ul></li>
         
        </ul></div>
        <input type="hidden" name="currency_code" value="" />
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
      </div>
    </form>
    <?php } ?></div>
