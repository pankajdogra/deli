<head>
<link href="catalog/view/theme/default/stylesheet/fbsignup.css" type="text/css" rel="stylesheet" />
</head>
<body>
<script type="text/javascript">
window.fbAsyncInit = function () {

	FB.init({
		appId: 		'<?php echo $fbsignup["appid"];?>',
		cookie: 	true,
		oauth:		true
	});
	
	var fb_busy = false;

$('#fb-login').click(function () {
		if (fb_busy) return;
		fb_busy = true;

		$('#login-fb-waiting-text').text('Connecting with Facebook...');

		$('#login-btr').hide('fade', 300);
		$('#login-fb-waiting').delay(300).show('fade', 250).delay(400);

		FB.login(function (res) {
			$('#login-fb-waiting').queue(function (next) {
				if (res.status == 'connected' && res.authResponse != null) {
					$('#login-fb-waiting-text').hide('fade', 250, function () {
						$(this).text('<?php echo $logging_text;?>').show('fade', 250).delay(400).queue(function (next2) {
							//$('#mode').val('facebook-auto');
							document.location.href='<?php echo $fbsignup["redirect_uri"];?>';
							//$('#login-btr').submit();

							next2();
						});
					});
				}
				else {
					$('#login-fb-waiting-text').hide('fade', 300, function () {
						$(this).text('<?php echo $canclled_text;?>').show('fade', 250).delay(400).queue(function (next2) {
							$('#login-fb-waiting').hide('fade', 300, function () {
								$('#login-btr').show('fade', 400, function () { fb_busy = false; });
							});

							next2();
						});
					});
				}

				next();
			});
		}, {
			scope: '<?php echo $fbsignup["scope"];?>'
		});
	});
};

$(function () {
	var e = document.createElement('script'); e.type = 'text/javascript'; e.async = true;
	e.src = 'https:' + '//connect.facebook.net/en_US/all.js';
	$('#fb-root').append(e);
});
</script>
<div id="fb-root"></div>
<div class="box box-fbconnect" id="login-btr">
 <?php if ($box==1) { ?>
 <?php if ($efect==1){ ?>

	 <div class="box-heading"><?php echo $fbsignup_button3; ?></div>
	  <div class="box-fbconnect-a box-content" id="fb-login">
	 <img src="<?php echo $botonfc; ?>" <span class="cursor"></spam>
     </div>

 <?php  ?>
 <?php }else{ ?> 
 <?php  ?>
 <div class="box-heading"><?php echo $fbsignup_button3; ?></div>
 <div class="box-content"><a class="sprite-button facebook-login" id="fb-login"><span class="button-image"><span class="img"></span></span> <span class="button-text tipotexto"><? echo $fbsignup_button;?> <span class="bold"><? echo $fbsignup_button2;?></span>
</a></div>
 
 <?php } ?>
 <?php }else{ ?>
 <?php if ($efect==1){ ?>

  <div class="box-fbconnect-a" id="fb-login">
	 <img src="<?php echo $botonfc; ?>" <span class="cursor"></spam>
     </div>

 <?php }else{ ?>
  <div><a class="sprite-button facebook-login" id="fb-login"><span class="button-image"><span class="img"></span></span> <span class="button-text tipotexto"><? echo $fbsignup_button;?> <span class="bold"><?echo $fbsignup_button2;?></span></span>
  </a></div>

 <?php }} ?>

</div>
<form id="login-fb-waiting" style="display: none; height: 40px; margin-top: 10px;">
    <div style="margin-top: 60px; text-align: center;">
        <img src="<?php echo $base;?>catalog/view/theme/default/image/fb_loader.gif" style="margin-right: 10px; vertical-align: middle;"> <span id="login-fb-waiting-text"></span>
    </div>
</form>
</body>
