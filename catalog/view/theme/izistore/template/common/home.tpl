<?php echo $header; ?>
<h1 style="display: none;"><?php echo $heading_title; ?></h1>
<div class="topContent">
	<?php echo $content_top; ?>
</div>
<section class="container-fluid">
<aside class="container">
<aside class="span3 content">
<?php echo $column_left; ?>
</aside>

<?php echo $column_right; ?>

<aside class="span8">

    <div class="tabbable">
<?php echo $content_bottom; ?>
</div>
</aside>
<div class="clear"></div>

</aside>
</section>
<?php echo $footer; ?>