<?php echo $header; ?>
<h1 style="display: none;"><?php echo $heading_title; ?></h1>
<div class="topContent">
	<?php echo $content_top; ?>
</div>
<section class="container-fluid">
<aside class="container">
<aside class="span3 content">
<?php echo $column_left; ?>
</aside>
<aside class="span8">
<?php echo $column_right; ?>
<div id="content">
<?php echo $content_bottom; ?>
<div class="clear"></div>
</div>
</aside>
</aside>
</section>
<?php echo $footer; ?>