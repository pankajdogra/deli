<?php

class ControllerSellerAccountOrder extends ControllerSellerAccount {
	public function index() {
		$this->document->addScript('catalog/view/javascript/dialog-buyeraddress.js');
		$this->document->addScript('catalog/view/javascript/dialog-markshipped.js');
		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller_physical'));
		
		$page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;

		$sort = array(
			'order_by'  => 'date_added',
			'order_way' => 'ASC',
			'offset' => ($page - 1) * 10,
			'limit' => 10
		);

		$seller_id = $this->customer->getId();
		
		$orders = $this->MsLoader->MsOrderData->getOrders(
			array(
				'seller_id' => $seller_id,
			),
			$sort
		);
		
		// Get current language ID
		/*$language_code = $this->session->data['language'];
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		
		foreach ($languages as $language) {
			if ($language['code'] == $language_code) {
				$language_id = $language['language_id'];
				break;
			}
		}*/

    	foreach ($orders as $order) {
			
			$total = 0.0;
			$products = $this->MsLoader->MsOrderData->getOrderProducts(array('order_id' => $order['order_id'], 'seller_id' => $seller_id));
			$sellerShipping = $this->MsLoader->MsShipping->getOrderSellerShipping($order['order_id'], $seller_id, 0);
			
			$shippings = array();
			$atLeastOneShippable = false;
			if (empty($sellerShipping)) {
				foreach ($products as $product) {
					// Shippable
					if ($this->MsLoader->MsShipping->getOrderProductShippable($order['order_id'], $product['product_id'])) {
						$atLeastOneShippable = true;
						$productShipping = $this->MsLoader->MsShipping->getOrderProductShipping($order['order_id'], $product['product_id'], 0);
						$shippings[] = array(
							'shipping_cost' => $productShipping['shipping_cost'],
							'name' => $productShipping['shipping_method_name']
							//'name' => $this->MsLoader->MsShippingMethod->getShippingMethodDescriptions($productShipping['product_shipping_method_id'])[$language_id]['name'],
						);
						$total += $productShipping['shipping_cost'];
					// Not shippable
					} else {
						$shippings[] = array(
							'shipping_cost' => "0",
							'name' => "--"
						);
					}
				}
			}
			else {
				$shippings[] = array(
					'shipping_cost' => $sellerShipping['shipping_cost'],
					'name' => $sellerShipping['shipping_method_name']
					//'name' => $this->MsLoader->MsShippingMethod->getShippingMethodDescriptions($sellerShipping['seller_shipping_method_id'])[$language_id]['name'],
				);
				$total += $sellerShipping['shipping_cost'];
			}
			
			$shipped = 0;
			$orderShipping = $this->MsLoader->MsShipping->getOrderShippingTracking($order['order_id'], $seller_id);
			if ($orderShipping) {
				$shipped = $orderShipping['shipped']; // Is shipped already
			}
		
    		$this->data['orders'][] = array(
    			'order_id' => $order['order_id'],
				'seller_id' => $seller_id,
				'customer_id' => $order['customer_id'],
    			'customer' => "{$order['firstname']} {$order['lastname']}",
				'email' => $order['email'],
    			'products' => $products,
    			'date_created' => date($this->language->get('date_format_short'), strtotime($order['date_added'])),
				'shippings' => $shippings,
				'shippable' => $atLeastOneShippable,
				'shipped' => $shipped,
   				'total' => $this->currency->format($total + $this->MsLoader->MsOrderData->getOrderTotal($order['order_id'], array('seller_id' => $seller_id)), $this->config->get('config_currency'))
   			);
   		}
		
		$pagination = new Pagination();
		$pagination->total = $this->MsLoader->MsOrderData->getTotalOrders(array('seller_id' => $seller_id));
		$pagination->page = $page;
		$pagination->limit = $sort['limit']; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('seller/account-order', 'page={page}', 'SSL');
		
		$this->data['pagination'] = $pagination->render();
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		
		$this->document->setTitle($this->language->get('ms_account_orders_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),			
			array(
				'text' => $this->language->get('ms_account_orders_breadcrumbs'),
				'href' => $this->url->link('seller/account-order', '', 'SSL'),
			)
		));
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('account-order');
		$this->response->setOutput($this->render());
	}
	
	// Buyer address dialog
	public function jxRenderBuyerAddressDialog() {
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		}
		if (isset($this->request->get['customer_id'])) {
			$customer_id = $this->request->get['customer_id'];
		}
  		
		if (!isset($order_id) || !isset($customer_id)) {
			return false;
		}
		
		$this->load->model('checkout/order');
		$this->data['order_info'] = $this->model_checkout_order->getOrder($order_id);
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('dialog-buyeraddress');
		return $this->response->setOutput($this->render());
  	}
	
	// Mark shipped dialog
	public function jxRenderMarkShippedDialog() {
		if (isset($this->request->get['order_id'])) {
			$this->data['order_id'] = $this->request->get['order_id'];
		}
		if (isset($this->request->get['seller_id'])) {
			$this->data['seller_id'] = $this->request->get['seller_id'];
		}
		if (isset($this->request->get['customer_id'])) {
			$this->data['customer_id'] = $this->request->get['customer_id'];
		}
  		
		if (!isset($this->data['order_id']) || !isset($this->data['seller_id']) || !isset($this->data['customer_id'])) {
			return false;
		}
		
		$this->data['initial'] = $this->request->get['initial'];
		
		$orderShipping = $this->MsLoader->MsShipping->getOrderShippingTracking($this->request->get['order_id'], $this->request->get['seller_id']);
		if ($orderShipping) {
			$this->data['shipped'] = $orderShipping['shipped'];
			$this->data['tracking_number'] = $orderShipping['tracking_number'];
			$this->data['comment'] = $orderShipping['comment'];
		} else {
			$this->data['shipped'] = 0;
			$this->data['tracking_number'] = "";
			$this->data['comment'] = "";
		}
	
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('dialog-markshipped');
		return $this->response->setOutput($this->render());
  	}
	
	// Submit mark shipped dialog
	public function jxSubmitMarkShippedDialog() {
  		if (!isset($this->request->post['order_id']) || !isset($this->request->post['seller_id']) || !isset($this->request->post['customer_id'])) {
  			return false;
		}

		$initial = $this->request->post['initial'];
		
		$data = array(
			'order_id' => $this->request->post['order_id'],
			'seller_id' => $this->request->post['seller_id'],
			'shipped' => (int)1,
    		'tracking_number' => trim($this->request->post['tracking_number']),
			'comment' => trim($this->request->post['comment'])
		);
		
		// Get customer e-mail address
		$this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']);

		// Initial mark as shipped
		if ($initial) {
			$this->MsLoader->MsShipping->createOrderShippingTracking($data);
			$mail = array(
				'recipients' => $customer_info['email'],
				'addressee' => $customer_info['firstname'],
				'seller_id' => $data['seller_id'],
				'order_id' => $data['order_id']
			);
			$this->MsLoader->MsMail->sendMail(MsMail::SMT_MARK_SHIPPED, $mail);
		// Edit tracking information
		} else {
			$this->MsLoader->MsShipping->editOrderShippingTracking($data, $data['order_id'], $data['seller_id']);
			$mail = array(
				'recipients' => $customer_info['email'],
				'addressee' => $customer_info['firstname'],
				'seller_id' => $data['seller_id'],
				'order_id' => $data['order_id']
			);
			$this->MsLoader->MsMail->sendMail(MsMail::SMT_TRACKING_INFORMATION, $mail);
		}
  	}
}

?>
