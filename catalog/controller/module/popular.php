<?php
class ControllerModulePopular extends Controller {
	protected function index($setting) {
		$this->language->load('module/popular'); 

      $this->data['heading_title'] = $this->language->get('heading_title');
      $this->data['text_auction'] = $this->language->get('text_auction');
		
		$this->data['button_cart'] = $this->language->get('button_cart');
		
		$this->load->model('catalog/product'); 
		
		$this->load->model('tool/image');

		$this->data['products'] = array();

		$products = explode(',', $this->config->get('popular_product'));		

		if (empty($setting['limit'])) {
			$setting['limit'] = 5;
		}
		
		$products = array_slice($products, 0, (int)$setting['limit']);
		
		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
				} else {
					$image = false;
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ((float)$product_info['startbidid']) {
					$startbidid = $product_info['startbidid'];
				} else {
					$startbidid = false;
				}
				
				if ((float)$product_info['startbidprice']) {
					$startbidprice = $this->currency->format($this->tax->calculate($product_info['startbidprice'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$startbidprice = false;
				}
			
				if ((float)$product_info['endbidtime']) {
					$endbidtime = $product_info['endbidtime'];
				} else {
					$endbidtime = "0000-00-00";
				}				
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
				
				
				if ((float)$product_info['startbidprice']) {
				$this->data['startbidprice'] = $this->currency->format($this->tax->calculate($product_info['startbidprice'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				$this->data['clearstartbidprice'] = $product_info['startbidprice'];
			} else {
				$this->data['startbidprice'] = false;
				$this->data['clearstartbidprice'] = 0;
			}
			
			
				if ((float)$product_info['endbidtime']) {
			$this->data['endbidtime'] = $product_info['endbidtime'];
			} else {
				$this->data['endbidtime'] = "";
			}
			
			$this->data['countCustomerBids'] = $this->model_catalog_product->countCustomerBids($product_id);
			
			$this->data['minofferstep'] = $this->model_catalog_product->minofferstep($product_id);
			
			$this->data['minoffersteps'] = $this->currency->format($this->data['minofferstep']);
			
			 $this->data['maxcustomerbid'] = $this->model_catalog_product->forMaxProductBids($product_id);
			
			
			if($this->data['countCustomerBids'] > 0){
						
				$this->data['mincustomerbid'] = $this->data['maxcustomerbid']+ $this->data['minofferstep'];
				
				$this->data['maxcustomerbidamt'] = $this->currency->format($this->data['maxcustomerbid']);
				
				}else {
				
				$this->data['mincustomerbid'] = $this->data['clearstartbidprice']+$this->data['minofferstep'];
				
				$this->data['maxcustomerbidamt'] = $this->currency->format($this->data['clearstartbidprice']);
			
			
			}
					
				$this->data['products'][] = array(
					'product_id'    => $product_info['product_id'],
					'thumb'   	    => $image,
					'name'    	    => $product_info['name'],
					'price'   	    => $price,
					'special' 	    => $special,
					'startbidid'    => $startbidid,
					'startbidprice' => $startbidprice,
					'endbidtime'    => $endbidtime,
					'maxcustomerbidamt' => $this->data['maxcustomerbidamt'],
					'rating'        => $rating,
					'reviews'       => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'    	    => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
				);
			}
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/popular.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/popular.tpl';
		} else {
			$this->template = 'default/template/module/popular.tpl';
		}

		$this->render();
	}
}
?>