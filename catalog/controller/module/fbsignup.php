<?php
class ControllerModulefbsignup extends Controller {
	protected function index($setting) {

		$this->language->load('module/fbsignup'); 
		$this->data['heading_title'] = $this->language->get('heading_title');
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
		$this->data['logging_text'] = $this->language->get('logging_text');
		$this->data['canclled_text'] = $this->language->get('canclled_text');
		if(!$this->customer->isLogged()){
			
			$this->data['fbsignup']['appid'] = $this->config->get('fbsignup_apikey');
			$this->data['fbsignup']['secret'] = $this->config->get('fbsignup_apisecret');
			$this->data['fbsignup']['scope'] = 'email,user_birthday,user_location,user_hometown';
			$this->data['fbsignup']['redirect_uri'] = $this->url->link('account/fbsignup', '', 'SSL');

			if(!isset($this->fbsignup)){			
				require_once(DIR_SYSTEM . 'vendor/facebook-sdk/facebook.php');
				$this->fbsignup = new Facebook(array(
					'appId'  => $this->data['fbsignup']['appid'],
					'secret' => $this->data['fbsignup']['secret'],
				));
			}
			
			$this->data['fbsignup_url'] = $this->fbsignup->getLoginUrl(
				array(
					'scope' => $this->data['fbsignup']['scope'],
					'redirect_uri'  => $this->data['fbsignup']['redirect_uri']
				)
			);

			if($this->config->get('fbsignup_button_' . $this->config->get('config_language_id'))){
				$this->data['fbsignup_button'] = html_entity_decode($this->config->get('fbsignup_button_' . $this->config->get('config_language_id')));
			}
			else $this->data['fbsignup_button'] = $this->language->get('heading_title');
			
			
			if($this->config->get('fbsignup_button2_' . $this->config->get('config_language_id'))){
				$this->data['fbsignup_button2'] = html_entity_decode($this->config->get('fbsignup_button2_' . $this->config->get('config_language_id')));
			}
			else $this->data['fbsignup_button2'] = $this->language->get('heading_title');
			
			if($this->config->get('fbsignup_button3_' . $this->config->get('config_language_id'))){
				$this->data['fbsignup_button3'] = html_entity_decode($this->config->get('fbsignup_button3_' . $this->config->get('config_language_id')));
			}
			else $this->data['fbsignup_button3'] = $this->language->get('heading_title');
			
			if (isset($this->request->post['box'])) {
			$this->data['box'] = $this->request->post['box'];
		} else {
			$this->data['box'] = $this->config->get('box');
		}
		if (isset($this->request->post['efect'])) {
			$this->data['efect'] = $this->request->post['efect'];
		} else {
			$this->data['efect'] = $this->config->get('efect');
		}
		if (isset($this->request->post['redirect'])) {
			$this->data['redirect'] = $this->request->post['redirect'];
		} else {
			$this->data['redirect'] = $this->config->get('redirect');
		}
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = 'image/';
		} else {
			$server = 'image/';
		}	
			if ($this->config->get('config_botonfc') && file_exists(DIR_IMAGE . $this->config->get('config_botonfc'))) {
			$this->data['botonfc'] = $server . $this->config->get('config_botonfc');
		} else {
			$this->data['botonfc'] = '';
		}
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/fbsignup.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/module/fbsignup.tpl';
			} else {
				$this->template = 'default/template/module/fbsignup.tpl';
			}

			$this->render();
		}				

	}
}
?>