function WR360Initialize(graphicsPath, scriptsPath, configFileURL, divID, viewerWidth, viewerHeight, rootPath)
{
	var imageDiv = jQuery(divID);
	if (imageDiv.length <= 0)
		return;
	
	if (viewerWidth != "")
		imageDiv.css("width", viewerWidth);

	if (viewerHeight != "")
		imageDiv.css("height", viewerHeight)
	
	imageDiv.css("padding", 0);
	
	var newHtml = "<div id='wr360PlayerId'> \
  <script language='javascript' type='text/javascript'> \
     _imageRotator.settings.graphicsPath   = '" + scriptsPath + "/" + graphicsPath + "'; \
     _imageRotator.settings.configFileURL  = '" + configFileURL + "'; \
	 _imageRotator.settings.rootPath  = '" + rootPath + "'; \
	 _imageRotator.licenseFileURL = '" + scriptsPath + "/html/license.lic" + "'; \
  </script> \
</div>";

	imageDiv.html(newHtml);
	imageDiv.css("visibility", "visible");
	_imageRotator.runImageRotator("wr360PlayerId");
}
