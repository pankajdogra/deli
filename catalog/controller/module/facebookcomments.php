<?php  
class ControllerModuleFacebookcomments extends Controller {
	protected function index() {
		$this->language->load('module/facebookcomments');

      	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['currenttemplate'] =  $this->config->get('config_template');

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['data']['FacebookComments'] = str_replace('http', 'https', $this->config->get('FacebookComments'));
		} else {
			$this->data['data']['FacebookComments'] = $this->config->get('FacebookComments');
		}
	

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/facebookcomments.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/facebookcomments.tpl';
		} else {
			$this->template = 'default/template/module/facebookcomments.tpl';
		}

		$this->render();

	}
	
	

}
?>