<?php  
class ControllerModulebeverlysion extends Controller {
	protected function index($setting) {
		static $module = 0;
		
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		$this->load->model('tool/true_image');
		
		$this->document->addScript('catalog/view/javascript/jquery/beverlysion/js/jquery.beverlysion.plugins.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/beverlysion/js/jquery.beverlysion.revolution.min.js');
		if (file_exists('catalog/view/javascript/jquery/beverlysion/css/beverlysion.css')) {
			$this->document->addStyle('catalog/view/javascript/jquery/beverlysion/css/beverlysion.css');
		} else {
			$this->document->addStyle('catalog/view/javascript/jquery/beverlysion/css/beverlysion.css');
		}
		
		$this->data['width'] = $setting['width'];
		$this->data['height'] = $setting['height'];
		
		$this->data['banners'] = array();
		
		if (isset($setting['banner_id'])) {
			$results = $this->model_design_banner->getBanner($setting['banner_id']);
			  
			foreach ($results as $result) {
				if (file_exists(DIR_IMAGE . $result['image'])) {
					$this->data['banners'][] = array(
						'title' => $result['title'],
						'link'  => $result['link'],
						'transicion'  => $result['transicion'],
						'slot_amt'  => $result['slot_amt'],
						'data_delay'  => $result['data_delay'],
						'master_speed'  => $result['master_speed'],
						'sm_pic_1'  => $this->model_tool_true_image->true_resize($result['sm_pic_1'], $setting['width'], $setting['height']),
						'sm_pic_1_data_x'  => $result['sm_pic_1_data_x'],
						'sm_pic_1_data_y'  => $result['sm_pic_1_data_y'],
						'sm_pic_1_data_sp'  => $result['sm_pic_1_data_sp'],
						'sm_pic_1_data_st'  => $result['sm_pic_1_data_st'],
						'sm_pic_1_data_ea'  => $result['sm_pic_1_data_ea'],
						'sm_pic_1_data_cls'  => $result['sm_pic_1_data_cls'],
						'sm_pic_1_data_h'  => $result['sm_pic_1_data_h'],
						'sm_pic_1_data_w'  => $result['sm_pic_1_data_w'],
						'sm_pic_1_url'  => $result['sm_pic_1_url'],
						'sm_pic_2'  => $this->model_tool_true_image->true_resize($result['sm_pic_2'], $setting['width'], $setting['height']),
						'sm_pic_2_data_x'  => $result['sm_pic_2_data_x'],
						'sm_pic_2_data_y'  => $result['sm_pic_2_data_y'],
						'sm_pic_2_data_sp'  => $result['sm_pic_2_data_sp'],
						'sm_pic_2_data_st'  => $result['sm_pic_2_data_st'],
						'sm_pic_2_data_ea'  => $result['sm_pic_2_data_ea'],
						'sm_pic_2_data_cls'  => $result['sm_pic_2_data_cls'],
						'sm_pic_2_data_h'  => $result['sm_pic_2_data_h'],
						'sm_pic_2_data_w'  => $result['sm_pic_2_data_w'],
						'sm_pic_2_url'  => $result['sm_pic_2_url'],
						'sm_pic_3'  => $this->model_tool_true_image->true_resize($result['sm_pic_3'], $setting['width'], $setting['height']),
						'sm_pic_3_data_x'  => $result['sm_pic_3_data_x'],
						'sm_pic_3_data_y'  => $result['sm_pic_3_data_y'],
						'sm_pic_3_data_sp'  => $result['sm_pic_3_data_sp'],
						'sm_pic_3_data_st'  => $result['sm_pic_3_data_st'],
						'sm_pic_3_data_ea'  => $result['sm_pic_3_data_ea'],
						'sm_pic_3_data_cls'  => $result['sm_pic_3_data_cls'],
						'sm_pic_3_data_h'  => $result['sm_pic_3_data_h'],
						'sm_pic_3_data_w'  => $result['sm_pic_3_data_w'],
						'sm_pic_4'  => $this->model_tool_true_image->true_resize($result['sm_pic_4'], $setting['width'], $setting['height']),
						'sm_pic_4_data_x'  => $result['sm_pic_4_data_x'],
						'sm_pic_4_data_y'  => $result['sm_pic_4_data_y'],
						'sm_pic_4_data_sp'  => $result['sm_pic_4_data_sp'],
						'sm_pic_4_data_st'  => $result['sm_pic_4_data_st'],
						'sm_pic_4_data_ea'  => $result['sm_pic_4_data_ea'],
						'sm_pic_4_data_cls'  => $result['sm_pic_4_data_cls'],
						'sm_pic_4_data_h'  => $result['sm_pic_4_data_h'],
						'sm_pic_4_data_w'  => $result['sm_pic_4_data_w'],
						'sm_pic_5'  => $this->model_tool_true_image->true_resize($result['sm_pic_5'], $setting['width'], $setting['height']),
						'sm_pic_5_data_x'  => $result['sm_pic_5_data_x'],
						'sm_pic_5_data_y'  => $result['sm_pic_5_data_y'],
						'sm_pic_5_data_sp'  => $result['sm_pic_5_data_sp'],
						'sm_pic_5_data_st'  => $result['sm_pic_5_data_st'],
						'sm_pic_5_data_ea'  => $result['sm_pic_5_data_ea'],
						'sm_pic_5_data_cls'  => $result['sm_pic_5_data_cls'],
						'sm_pic_5_data_h'  => $result['sm_pic_5_data_h'],
						'sm_pic_5_data_w'  => $result['sm_pic_5_data_w'],
						'caption_1'  => $result['caption_1'],
						'caption_1_data_x'  => $result['caption_1_data_x'],
						'caption_1_data_y'  => $result['caption_1_data_y'],
						'caption_1_data_sp'  => $result['caption_1_data_sp'],
						'caption_1_data_st'  => $result['caption_1_data_st'],
						'caption_1_data_ea'  => $result['caption_1_data_ea'],
						'caption_1_data_cls'  => $result['caption_1_data_cls'],
						'caption_1_url'  => $result['caption_1_url'],
						'caption_2'  => $result['caption_2'],
						'caption_2_data_x'  => $result['caption_2_data_x'],
						'caption_2_data_y'  => $result['caption_2_data_y'],
						'caption_2_data_sp'  => $result['caption_2_data_sp'],
						'caption_2_data_st'  => $result['caption_2_data_st'],
						'caption_2_data_ea'  => $result['caption_2_data_ea'],
						'caption_2_data_cls'  => $result['caption_2_data_cls'],
						'caption_2_url'  => $result['caption_2_url'],
						'caption_3'  => $result['caption_3'],
						'caption_3_data_x'  => $result['caption_3_data_x'],
						'caption_3_data_y'  => $result['caption_3_data_y'],
						'caption_3_data_sp'  => $result['caption_3_data_sp'],
						'caption_3_data_st'  => $result['caption_3_data_st'],
						'caption_3_data_ea'  => $result['caption_3_data_ea'],
						'caption_3_data_cls'  => $result['caption_3_data_cls'],
						'caption_4'  => $result['caption_4'],
						'caption_4_data_x'  => $result['caption_4_data_x'],
						'caption_4_data_y'  => $result['caption_4_data_y'],
						'caption_4_data_sp'  => $result['caption_4_data_sp'],
						'caption_4_data_st'  => $result['caption_4_data_st'],
						'caption_4_data_ea'  => $result['caption_4_data_ea'],
						'caption_4_data_cls'  => $result['caption_4_data_cls'],
						'caption_5'  => $result['caption_5'],
						'caption_5_data_x'  => $result['caption_5_data_x'],
						'caption_5_data_y'  => $result['caption_5_data_y'],
						'caption_5_data_sp'  => $result['caption_5_data_sp'],
						'caption_5_data_st'  => $result['caption_5_data_st'],
						'caption_5_data_ea'  => $result['caption_5_data_ea'],
						'caption_5_data_cls'  => $result['caption_5_data_cls'],
						'button_1'  => $result['button_1'],
						'button_1_data_x'  => $result['button_1_data_x'],
						'button_1_data_y'  => $result['button_1_data_y'],
						'button_1_data_sp'  => $result['button_1_data_sp'],
						'button_1_data_st'  => $result['button_1_data_st'],
						'button_1_data_ea'  => $result['button_1_data_ea'],
						'button_1_data_cls'  => $result['button_1_data_cls'],
						'button_1_url'  => $result['button_1_url'],
						'youtube_iframe'  => $result['youtube_iframe'],
						'youtube_autoplay'  => $result['youtube_autoplay'],
						'youtube_data_x'  => $result['youtube_data_x'],
						'youtube_data_y'  => $result['youtube_data_y'],
						'youtube_data_sp'  => $result['youtube_data_sp'],
						'youtube_data_st'  => $result['youtube_data_st'],
						'youtube_data_ea'  => $result['youtube_data_ea'],
						'youtube_data_cls'  => $result['youtube_data_cls'],
						'youtube_data_h'  => $result['youtube_data_h'],
						'youtube_data_w'  => $result['youtube_data_w'],
						'vimeo_iframe'  => $result['vimeo_iframe'],
						'vimeo_autoplay'  => $result['vimeo_autoplay'],
						'vimeo_data_x'  => $result['vimeo_data_x'],
						'vimeo_data_y'  => $result['vimeo_data_y'],
						'vimeo_data_sp'  => $result['vimeo_data_sp'],
						'vimeo_data_st'  => $result['vimeo_data_st'],
						'vimeo_data_ea'  => $result['vimeo_data_ea'],
						'vimeo_data_cls'  => $result['vimeo_data_cls'],
						'vimeo_data_h'  => $result['vimeo_data_h'],
						'vimeo_data_w'  => $result['vimeo_data_w'],
						'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
					);
				}
			}
		}
		
		$this->data['module'] = $module++;
						
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/beverlysion.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/beverlysion.tpl';
		} else {
			$this->template = 'default/template/module/beverlysion.tpl';
		}
		
		$this->render();
	}
}
?>