<?php

// affiliate/account
$_['text_signup_link'] = 'Link for Affiliates to Sign Up Under You';
$_['text_aff_link_any_page'] = 'When you are logged into your affiliate account, you can easily obtain links with your tracking code, pointing to any page on the site, while browsing the site and clicking <img src="catalog/view/theme/default/image/link.png" /> icon. Those links track both customers and your sub-affiliates.';

// affiliate/transaction
$_['text_total_earnings']       = 'Total earnings:';
$_['text_view_subs'] = 'View affiliates signed up under you';
$_['column_level'] = 'Level';
$_['column_num_affs'] = 'Number of Affiliates';

