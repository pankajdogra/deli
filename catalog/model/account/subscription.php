<?php
class ModelAccountSubscription extends Model {	
	public function getSubscriptions($data = array()) {
		
		
		$sql = "SELECT sb.*,(pd.name) AS pname,(p.image) AS image ,(pd.description) AS description FROM " . DB_PREFIX . "product p
        LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)		
		LEFT JOIN " . DB_PREFIX . "subscriptionsbids 
		sb ON (p.product_id = sb.product_id)";
		
		$sql .= " WHERE sb.customer_id = '" . (int)$this->customer->getId() . "'";
		   
		$sort_data = array(
			'sb.email',
			'sb.date_added',
			'sb.name'
		);
	
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY sb.date_added";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
	
		return $query->rows;
	}	
		
	public function getTotalsubscriptions() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "subscriptionsbids` WHERE customer_id = '" . (int)$this->customer->getId() . "'");
			
		return $query->row['total'];
	}
	
	
	public function getTsubscriptions() {
      	$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subscriptionsbids` WHERE customer_id = '" . (int)$this->customer->getId() . "'");
			
		if ($query->num_rows) {
			return $query->num_rows;
		} else {
			return 0;	
		}
	}

    public function deletesubscription($product_id,$customer_id){
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "subscriptionsbids WHERE product_id=".(int)$product_id." AND customer_id=".(int)$customer_id."");
		return 1;
	}
	
			
	
}
?>