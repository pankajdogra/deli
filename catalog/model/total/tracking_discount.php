<?php

class ModelTotalTrackingDiscount extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->load->model('account/order');
		$this->load->model('affiliate/affiliate');
		if (!$this->config->get('tracking_discount_status') || !isset($this->request->cookie['tracking']) || !$this->cart->getSubTotal() || !$this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking'])) return;
		
		if(in_array($this->config->get('tracking_discount_when'), array('first', 'period'))) {
			if(!$this->customer->isLogged()) return;
			$_q = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE customer_id = '" . (int)$this->customer->getId() . "' AND order_status_id > '0' AND affiliate_id > '0'";
			if($this->config->get('tracking_discount_when') == 'period') $_q .= " AND date_added > '" . $this->db->escape(date('Y-m-d H:i:s', time() - 86400 * $this->config->get('tracking_discount_period'))) . "'";
			$_res = $this->db->query($_q);
			if($_res->row['total'] > 0) return;
		}				
		
		$this->load->language('total/tracking_discount');
		
		if($this->config->get('tracking_discount_what') != 'total') {
			$what = explode('_', $this->config->get('tracking_discount_what'));
			$field = $what[1] == 'one' ? 'price' : 'total';
			$_products = $this->cart->getProducts();
			$products = array();
			foreach($_products as $_p) {
				$products[] = $_p[$field];
			}
			if($what[0] == 'all') {
				$st = array_sum($products);
			} else {
				sort($products);
				$_index = $what[0] == 'min' ? 0 : (sizeof($products) - 1);
				$st = $products[$_index];
			}		
		} else {		
			$st = $total;
		}
		
		if($this->config->get('tracking_discount_type') == 'fixed') {
			$_fixed_max = $st * ((float) $this->config->get('tracking_discount_fixed_max')) * 0.01;
			$dsc =  $this->config->get('tracking_discount_amount') < $_fixed_max ?  $this->config->get('tracking_discount_amount') : $_fixed_max;
		} else {
			$_perc = ((float) $this->config->get('tracking_discount_amount')) * 0.01;
			$dsc = $st - ($st * (1 - $_perc));
		}
		$total_data[] = array(
                            'code'       => 'tracking_discount',
                            'title'      => $this->language->get('text_tracking_discount'),
                            'text'       => $this->currency->format(-$dsc),
                            'value'      => -$dsc,
                            'sort_order' => $this->config->get('tracking_discount_sort_order')
		);
        $total -= $dsc;		
	}
}

