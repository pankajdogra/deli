<?php

	class ModelCheckoutCftypCouponAffiliate extends Model {
	
		function getAffiliate($coupon_id) {
			if(!preg_match("/^\d+$/", $coupon_id)) return false;
			$res = $this->db->query("select affiliate_id from " . DB_PREFIX . "cftyp_coupon_affiliate where coupon_id = '" . (int)$coupon_id . "'");
			if($res->num_rows < 1) return false;
			$this->load->model('affiliate/affiliate');
			$affiliate = $this->model_affiliate_affiliate->getAffiliate($res->row['affiliate_id']);
			if(!$affiliate || !$affiliate['status'] || !$affiliate['approved']) return false;
			return $affiliate;			
		}	
	}
	
