<?php
//----------------------------------------------------------------------
//  CrawlProtect 3.0.1
//----------------------------------------------------------------------
// Protect your website from hackers
//----------------------------------------------------------------------
// Author: Jean-Denis Brun
//----------------------------------------------------------------------
// Website: www.crawlprotect.com
//----------------------------------------------------------------------
// That script is distributed under GNU GPL license
//----------------------------------------------------------------------
// file: sponsors.php
//----------------------------------------------------------------------
//  Last update: 09/06/2012
//----------------------------------------------------------------------

echo"<div class=\"sponsortop\">&nbsp;</div>\n";
echo"<div class=\"sponsor\">\n";
echo"<div align=\"center\">";
echo "<h1>".$language['expertise']."</h1>";
echo"<table width='95%' ><tr><td style='padding-right:25px; text-align:justify;'>";
echo $language['phpscript'];
echo"</td><td style='padding-left:25px; text-align:justify;'>";
echo $language['hackcleaning'];
echo"</td></tr></table>";
echo "<br><p>".$language['askaquote']."</p>";
echo"</div><br>";
echo"</div>\n";
?>
