<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>

<script type="text/javascript" src="catalog/view/javascript/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/flexy-menu.js"></script>
<script type="text/javascript">$(document).ready(function(){$(".flexy-menu").flexymenu({speed: 400,type: "vertical"});});</script>
             <link href="catalog/view/theme/bitmit/stylesheet/flexy-menu.css" rel="stylesheet">

         <link href="catalog/view/theme/bitmit/stylesheet/bootstrap.css" rel="stylesheet">
         <link href="catalog/view/theme/bitmit/stylesheet/bootstrap-responsive.css" rel="stylesheet">
         <link href="catalog/view/theme/bitmit/stylesheet/style_sheet.css" rel="stylesheet">


<!--<link rel="stylesheet" type="text/css" href="catalog/view/theme/bitmit/stylesheet/stylesheet.css" />
--><?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
   <script src="catalog/view/javascript/bootstrap-transition.js"></script>
    <script src="catalog/view/javascript/bootstrap-alert.js"></script>
    <script src="catalog/view/javascript/bootstrap-modal.js"></script>
<script src="catalog/view/javascript/bootstrap-dropdown.js"></script>
<!--    <script src="js/bootstrap-scrollspy.js"></script>
-->    <script src="catalog/view/javascript/bootstrap-tab.js"></script>
   <script src="catalog/view/javascript/bootstrap-tooltip.js"></script>
    <script src="catalog/view/javascript/bootstrap-popover.js"></script>
  <script src="catalog/view/javascript/bootstrap-button.js"></script>
    <script src="catalog/view/javascript/bootstrap-collapse.js"></script>
    <script src="catalog/view/javascript/bootstrap-carousel.js"></script>

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>

				<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.dcjqaccordion.2.7.js"></script>
				<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.cookie.js"></script>
				<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.hoverIntent.minified.js"></script>
			
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/dcaccordion.css"/>
			
</head>
<body>
<article class="top_bg">
<section class="container">
     <aside class="row-fluid">
       <aside class="span6 pull-right">
	        <aside class="span2 floatLeft datemenu">
               <?php echo $currency; ?>
            </aside>
			<aside class="span2 floatLeft">
              <aside class="flag">
                  <?php echo $language; ?>
               </aside>
          </aside>   
  
              <aside class="span3 floatRight login">
                 <?php if (!$logged) { ?>
                 <?php echo $text_welcome; ?>
                 <?php } else { ?>
                 <?php echo $text_logged; ?>
                 <?php } ?>
              </aside>
        </aside>
     </aside>
<!--<div id="header">
-->  
  <aside class="row">
  <aside class="span3">
<figure class="logo">
<?php if ($logo) { ?>
<a href="<?php echo $home; ?>"><img src="catalog/view/theme/bitmit/image/logo.png" alt="" title="Bitmit Logo" border="0" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a><?php } ?></figure></aside>
  
    <aside class="span3 margintop42">    
    <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
  <button class="srbtn"></button>
</aside>    
<!--  </div>-->
<aside class="span6">
<aside class="span3 shop">
    <?php echo $cart; ?> 
  </aside>       
  <aside class="span6 topmenu">
  <ul>
   <li> <a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
   <li><a href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a></li>
   <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
   <li><a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a></li>
   <li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
  </ul>
  </aside>
 <aside class="span4 floatRight">

        <aside class="email">
        <figure class="email_icon">
        <img title="" alt="" src="catalog/view/theme/bitmit/image/email.png">
        </figure>
        
        <aside class="email_txt">
        <a href="#">itopproduct@gmail.com</a>
        </aside>
        </aside>
        
        <aside class="ph">
        <figure class="ph_icon">
        <img title="" alt="" src="catalog/view/theme/bitmit/image/ph.png">
        </figure>
        <aside class="ph_txt">
        0172-4001500
        </aside>
        </aside>
</aside>
</aside>
</aside>
</section>
</article>
<aside class="clearfix"></aside>
<section class="container-fluid">
<aside class="container">
<aside class="span3 content">
<div class="nav-collapse collapse">
  <ul class="flexy-menu orange vertical">
  <li class="showhide" style="display: none;">
  <span class="title">MENU</span><span class="icon">
</span></li>
<?php if ($categories) { ?>
    <?php foreach ($categories as $category) { ?>
    <li class="active" style=""><a href="<?php echo $category['href']; ?>"><span class="icon-heart"></span><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
      
        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul style="display: none;>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
        <?php } ?>
      
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
<?php } ?> 
</div>
</aside>
</aside>
</section>
<div id="notification"></div>


