<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/category.png" alt="" /> <?php echo $heading_title; ?></h1>
	   <div class="buttons"><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
     
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php echo $column_name; ?></td>
              <td class="right">Email</td>
              <td class="right">Date Added</td>
			  <td class="right">Status</td>
            </tr>
          </thead>
          <tbody>
            <?php if ($subscriptions) { ?>
            <?php foreach ($subscriptions as $subscription) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($subscription['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $subscription['Id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $subscription['Id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $subscription['name']; ?></td>
              <td class="right"><?php echo $subscription['Email']; ?></td>
	       <td class="right"><?php echo $subscription['date_added']; ?></td>
		   <td class="right"><a href="<?php echo $subscription['href'];?>"><?php echo $subscription['status']; ?></a></td>
	
             
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>