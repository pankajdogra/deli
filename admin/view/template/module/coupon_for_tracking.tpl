<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>  
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_always; ?></td>
            <td>
				<select name="cftyp_always">
				<?php if($cftyp_always) {?>
					<option value="1" selected="selected"><?php echo $text_yes;?></option>
					<option value="0"><?php echo $text_no;?></option>
				<?php					
					} else {?>
					<option value="1"><?php echo $text_yes;?></option>
					<option value="0" selected="selected"><?php echo $text_no;?></option>					
				<?php
					}?>
				</select>
			</td>			
          </tr>
		</table>		
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>