<table class="form wideBoxes">
  <tr>
    <td>Color Scheme</td>
    <td>
    	<select name="FacebookComments[ColorScheme]">
			<option value="light" <?php echo($data['FacebookComments']['ColorScheme'] == 'light') ? 'selected' : '' ; ?>>Light</option>   
			<option value="dark" <?php echo($data['FacebookComments']['ColorScheme'] == 'dark') ? 'selected' : '' ; ?>>Dark</option>        
        </select>
   </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Facebook App ID</td>
    <td>
    	<input type="text" name="FacebookComments[APIKey]" value="<?php echo(!empty($data['FacebookComments']['APIKey'])) ? $data['FacebookComments']['APIKey'] : '' ; ?>" />
   </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Number of Posts<span class="help">How many comment posts to be visible by default.</span></td>
    <td>
    	<input type="text" name="FacebookComments[NumberOfPosts]" value="<?php echo(!empty($data['FacebookComments']['NumberOfPosts'])) ? $data['FacebookComments']['NumberOfPosts'] : '2' ; ?>" />
   </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Width<span class="help">In Pixels</span></td>
    <td>
    	<input type="text" name="FacebookComments[Width]" value="<?php echo(!empty($data['FacebookComments']['Width'])) ? $data['FacebookComments']['Width'] : '50%' ; ?>" />
   </td>
  </tr>
  <tr>
    <td>URL Route<span class="help">Specify URL route for which exact page you want to have the Facebook Comments enabled. For example for the product detail page specify <em>product/product</em></span></td>
    <td valign="top">
    	<input type="text" name="FacebookComments[URLRoute]" value="<?php echo(!empty($data['FacebookComments']['URLRoute'])) ? $data['FacebookComments']['URLRoute'] : '' ; ?>" />
   </td>
  </tr>
  <tr>
    <td>Hide Tabs</td>
    <td>
    	<select name="FacebookComments[HideTabs]">
			<option value="yes" <?php echo($data['FacebookComments']['HideTabs'] == 'yes') ? 'selected' : '' ; ?>>Yes</option>   
			<option value="no" <?php echo($data['FacebookComments']['HideTabs'] == 'no') ? 'selected' : '' ; ?>>No</option>
        </select>
   </td>
  </tr>
  <tr>
    <td>Hide Tags</td>
    <td>
    	<select name="FacebookComments[HideTags]">
			<option value="yes" <?php echo($data['FacebookComments']['HideTags'] == 'yes') ? 'selected' : '' ; ?>>Yes</option>   
			<option value="no" <?php echo($data['FacebookComments']['HideTags'] == 'no') ? 'selected' : '' ; ?>>No</option>
        </select>
   </td>
  </tr>
  <tr>
    <td>Show Facebook Comments in product tab</td>
    <td>
    	<select name="FacebookComments[showInTab]">
			<option value="yes" <?php echo($data['FacebookComments']['showInTab'] == 'yes') ? 'selected' : '' ; ?>>Yes</option>   
			<option value="no" <?php echo($data['FacebookComments']['showInTab'] == 'no') ? 'selected' : '' ; ?>>No</option>
        </select>
   </td>
  </tr>
  <tr>
    <td>Custom CSS</td>
    <td>
    	<textarea name="FacebookComments[CustomCSS]" style="width:50%; height:70px;"><?php echo (!empty($data['FacebookComments']['CustomCSS'])) ? $data['FacebookComments']['CustomCSS'] : '' ; ?></textarea>
   </td>
  </tr>
</table>
