<table class="form">
  <tr>
    <td><span class="required">*</span> <?php echo $entry_code; ?></td>
    <td>
        <select name="FacebookComments[Enabled]" class="FacebookCommentsEnabled">
            <option value="yes" <?php echo ($data['FacebookComments']['Enabled'] == 'yes') ? 'selected=selected' : ''?>>Enabled</option>
            <option value="no" <?php echo ($data['FacebookComments']['Enabled'] == 'no') ? 'selected=selected' : ''?>>Disabled</option>
        </select>
   </td>
  </tr>
  <tr class="FacebookCommentsActiveTR">
    <td><span class="required">*</span> <?php echo $entry_layouts_active; ?></td>
    <td>
    <?php $i=0;?>
    <?php foreach ($layouts as $layout) { ?>
    <?php 
	    $status = null;
        foreach ($modules as $module) {
            if(!empty($module)) {
                if ($module['layout_id'] == $layout['layout_id']) {
                    $status = $module['status'];
                    if ((int)$status == 1) {
                        $checked = ' checked=checked';
                    } else { 
                        $checked = '';
                    }
                }
            } 
          
          }
          if (!isset($status) && $layout['name'] == 'Product') {
                $status = 1;
                $checked = ' checked=checked';	
          }
		  
		  
    ?>
    <div class="FacebookCommentsLayout">
        <input type="checkbox" value="<?php echo $layout['layout_id']; ?>" id="FacebookCommentsActive<?php echo $i?>" <?php echo $checked?> /><label for="FacebookCommentsActive<?php echo $i?>"><?php echo $layout['name']; ?></label>
        <input type="hidden" name="facebookcomments_module[<?php echo $i?>][position]" value="content_bottom" />
        <input type="hidden" class="FacebookCommentsItemLayoutIDField" name="facebookcomments_module[<?php echo $i?>][layout_id]" value="<?php echo $layout['layout_id']; ?>" />
        <input type="hidden" class="FacebookCommentsItemStatusField" name="facebookcomments_module[<?php echo $i?>][status]" value="<?php echo $status ?>" />
        <input type="hidden" name="facebookcomments_module[<?php echo $i?>][sort_order]" value="<?php echo $i+10?>" />
    </div>
    <?php $i++;} ?>
     </td>
  </tr>
</table>
<script>
$('.FacebookCommentsLayout input[type=checkbox]').change(function() {
    if ($(this).is(':checked')) { 
        $('.FacebookCommentsItemStatusField', $(this).parent()).val(1);
    } else {
        $('.FacebookCommentsItemStatusField', $(this).parent()).val(0);
    }
});

$('.FacebookCommentsEnabled').change(function() {
    toggleFacebookCommentsActive(true);
});

var toggleFacebookCommentsActive = function(animated) {
    if ($('.FacebookCommentsEnabled').val() == 'yes') {
        if (animated) 
            $('.FacebookCommentsActiveTR').fadeIn();
        else 
            $('.FacebookCommentsActiveTR').show();
    } else {
        if (animated) 
            $('.FacebookCommentsActiveTR').fadeOut();
        else 
            $('.FacebookCommentsActiveTR').hide();
    }
}

toggleFacebookCommentsActive(false);
</script>
