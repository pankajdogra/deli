<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/total.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" id="form">
                <table class="form">
                    <tr>
                        <td><?php echo $entry_amount; ?></td>
                        <td><input type="text" name="tracking_discount_amount" value="<?php echo $tracking_discount_amount; ?>" size="5" /></td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_type; ?></td>
                        <td>
							<select name="tracking_discount_type">
                                <?php if ($tracking_discount_type == 'percent') { ?>
                                    <option value="percent" selected="selected"><?php echo $text_percent; ?></option>
                                    <option value="fixed"><?php echo $text_fixed; ?></option>
                                <?php } else { ?>
                                    <option value="percent"><?php echo $text_percent; ?></option>
                                    <option value="fixed" selected="selected"><?php echo $text_fixed; ?></option>
                                <?php } ?>
                            </select>						
						</td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_fixed_max; ?></td>
                        <td><input type="text" name="tracking_discount_fixed_max" value="<?php echo ($tracking_discount_fixed_max ? $tracking_discount_fixed_max : 50); ?>" size="5" /></td>
                    </tr>		

                    <tr>
                        <td><?php echo $entry_when; ?></td>
                        <td>
							<select name="tracking_discount_when">
							<?php foreach(array('all', 'first', 'period') as $_when) {
								if ($tracking_discount_when == $_when) { ?>
                                    <option value="<?php echo $_when; ?>" selected="selected"><?php echo ${'text_when_' . $_when}; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $_when; ?>"><?php echo ${'text_when_' . $_when}; ?></option>
                                <?php } 
								}
								?>
                            </select>						
						</td>
                    </tr>
					
                    <tr>
                        <td><?php echo $entry_period; ?></td>
                        <td><input type="text" name="tracking_discount_period" value="<?php echo ($tracking_discount_period ? $tracking_discount_period : 7); ?>" size="5" /></td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_what; ?></td>
                        <td>
							<select name="tracking_discount_what">
							<?php foreach(array('total', 'all_all', 'all_one', 'min_all', 'min_one', 'max_all', 'max_one') as $_what) {
								if ($tracking_discount_what == $_what) { ?>
                                    <option value="<?php echo $_what; ?>" selected="selected"><?php echo ${'text_what_' . $_what}; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $_what; ?>"><?php echo ${'text_what_' . $_what}; ?></option>
                                <?php } 
								}
								?>
                            </select>						
						</td>
                    </tr>					
					
                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td><select name="tracking_discount_status">
                                <?php if ($tracking_discount_status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_sort_order; ?></td>
                        <td><input type="text" name="tracking_discount_sort_order" value="<?php echo $tracking_discount_sort_order; ?>" size="1" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

<?php echo $footer; ?>