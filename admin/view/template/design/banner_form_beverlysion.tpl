<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/banner.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_name; ?></td>
            <td><input type="text" name="name" value="<?php echo $name; ?>" size="100" />
              <?php if ($error_name) { ?>
              <span class="error"><?php echo $error_name; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="status">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
          <td><?php echo $entry_beverlysion_global; ?></td>
                <td class="left"><select name="beverlysion_sts">
                <?php if ($beverlysion) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
        </table>
        <table id="images" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_title; ?></td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $image_row = 0; ?>
          <?php foreach ($banner_images as $banner_image) { ?>
          <tbody id="image-row<?php echo $image_row; ?>">
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['title'] : ''; ?>" />
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>
                <span class="error"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="banner_image[<?php echo $image_row; ?>][link]" value="<?php echo $banner_image['link']; ?>" /></td>
              <td class="left"><div class="image"><img src="<?php echo $banner_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][image]" value="<?php echo $banner_image['image']; ?>" id="image<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td class="left">&nbsp;Beverlysion&#153&nbsp;Advanced&nbsp;
              <select name="banner_image[<?php echo $image_row; ?>][beverlysion_act]" id="banner_image[<?php echo $image_row; ?>][beverlysion_act]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="1" '. <?php echo ($banner_image["beverlysion_act"]==1)   ? 'selected="selected"' : '' ?>. '>Yes</option>
              <option value="">------------------------</option>
               <option value="2" '. <?php echo ($banner_image["beverlysion_act"]==2)   ? 'selected="selected"' : '' ?>. '>No</option>
               <option value="">------------------------</option>
               </select>&nbsp;&nbsp;<a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a>
              </td>
            </tr>           
            <?php if ($banner_image['beverlysion_act']==1) { ?>
            <tr>
            <td colspan="4">
            <div class="arrowlistmenu_item cats_shadow">
<h3 class="menuheader expandable_opts">Advanced Options</h3>
<ul class="categoryitems_opts">
<li class="bl">Beverlysion&#153 V2.0
<table cellpadding="0" cellspacing="3" border="0">
<tr>
<td class="left">Picture 1<br /><div class="image"><img src="<?php echo $banner_image['thumb_1']; ?>" alt="" id="thumb_1<?php echo $image_row; ?>" />
                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1]" value="<?php echo $banner_image['sm_pic_1']; ?>" id="sm_pic_1<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('sm_pic_1<?php echo $image_row; ?>', 'thumb_1<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a><a onclick="$('#thumb_1<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#sm_pic_1<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a><br />
                  Height<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_h]" value="<?php echo $banner_image['sm_pic_1_data_h']; ?>" size="14px" />
                  width<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_w]" value="<?php echo $banner_image['sm_pic_1_data_w']; ?>" size="14px" /></div></td><td>
<style><!--
.list tbody td {
    padding: 5px 5px;
}
--></style>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_x]" value="<?php echo $banner_image['sm_pic_1_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_y]" value="<?php echo $banner_image['sm_pic_1_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_sp]" value="<?php echo $banner_image['sm_pic_1_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_st]" value="<?php echo $banner_image['sm_pic_1_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td>
<select name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_ea]" id="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["sm_pic_1_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select>
</td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_cls]" value="<?php echo $banner_image['sm_pic_1_data_cls']; ?>" /></td>
</tr>
<tr>
<td>URL</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_url]" value="<?php echo $banner_image['sm_pic_1_url']; ?>" /></td>
</tr>
</table>
</td>
<td class="left">Picture 2<br /><div class="image"><img src="<?php echo $banner_image['thumb_2']; ?>" alt="" id="thumb_2<?php echo $image_row; ?>" />
                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2]" value="<?php echo $banner_image['sm_pic_2']; ?>" id="sm_pic_2<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('sm_pic_2<?php echo $image_row; ?>', 'thumb_2<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a><a onclick="$('#thumb_2<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#sm_pic_2<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a><br />
                  Height<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_h]" value="<?php echo $banner_image['sm_pic_2_data_h']; ?>" size="14px" />
                  width<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_w]" value="<?php echo $banner_image['sm_pic_2_data_w']; ?>" size="14px" /></div></td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_x]" value="<?php echo $banner_image['sm_pic_2_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_y]" value="<?php echo $banner_image['sm_pic_2_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_sp]" value="<?php echo $banner_image['sm_pic_2_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_st]" value="<?php echo $banner_image['sm_pic_2_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_ea]" id="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["sm_pic_2_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_cls]" value="<?php echo $banner_image['sm_pic_2_data_cls']; ?>" /></td>
</tr>
<tr>
<td>URL</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_url]" value="<?php echo $banner_image['sm_pic_2_url']; ?>" /></td>
</tr>
</table>
</td>
<td class="left">Picture 3<br /><div class="image"><img src="<?php echo $banner_image['thumb_3']; ?>" alt="" id="thumb_3<?php echo $image_row; ?>" />
                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3]" value="<?php echo $banner_image['sm_pic_3']; ?>" id="sm_pic_3<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('sm_pic_3<?php echo $image_row; ?>', 'thumb_3<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a><a onclick="$('#thumb_3<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#sm_pic_3<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a><br />
                  Height<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_h]" value="<?php echo $banner_image['sm_pic_3_data_h']; ?>" size="14px" />
                  width<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_w]" value="<?php echo $banner_image['sm_pic_3_data_w']; ?>" size="14px" /></div></td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_x]" value="<?php echo $banner_image['sm_pic_3_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_y]" value="<?php echo $banner_image['sm_pic_3_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_sp]" value="<?php echo $banner_image['sm_pic_3_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_st]" value="<?php echo $banner_image['sm_pic_3_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_ea]" id="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["sm_pic_3_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_cls]" value="<?php echo $banner_image['sm_pic_3_data_cls']; ?>" /></td>
</tr>
</table>
</td>
<td class="left">Picture 4<br /><div class="image"><img src="<?php echo $banner_image['thumb_4']; ?>" alt="" id="thumb_4<?php echo $image_row; ?>" />
                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4]" value="<?php echo $banner_image['sm_pic_4']; ?>" id="sm_pic_4<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('sm_pic_4<?php echo $image_row; ?>', 'thumb_4<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a><a onclick="$('#thumb_4<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#sm_pic_4<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a><br />
                  Height<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_h]" value="<?php echo $banner_image['sm_pic_4_data_h']; ?>" size="14px" />
                  width<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_w]" value="<?php echo $banner_image['sm_pic_4_data_w']; ?>" size="14px" /></div></td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_x]" value="<?php echo $banner_image['sm_pic_4_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_y]" value="<?php echo $banner_image['sm_pic_4_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_sp]" value="<?php echo $banner_image['sm_pic_4_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_st]" value="<?php echo $banner_image['sm_pic_4_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_ea]" id="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["sm_pic_4_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_cls]" value="<?php echo $banner_image['sm_pic_4_data_cls']; ?>" /></td>
</tr>
</table>
</td>
<td class="left">Picture 5<br /><div class="image"><img src="<?php echo $banner_image['thumb_5']; ?>" alt="" id="thumb_5<?php echo $image_row; ?>" />
                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5]" value="<?php echo $banner_image['sm_pic_5']; ?>" id="sm_pic_5<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('sm_pic_5<?php echo $image_row; ?>', 'thumb_5<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a><a onclick="$('#thumb_5<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#sm_pic_5<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a><br />
                  Height<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_h]" value="<?php echo $banner_image['sm_pic_5_data_h']; ?>" size="14px" />
                  width<input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_w]" value="<?php echo $banner_image['sm_pic_5_data_w']; ?>" size="14px" /></div></td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_x]" value="<?php echo $banner_image['sm_pic_5_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_y]" value="<?php echo $banner_image['sm_pic_5_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_sp]" value="<?php echo $banner_image['sm_pic_5_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_st]" value="<?php echo $banner_image['sm_pic_5_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_ea]" id="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["sm_pic_5_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_cls]" value="<?php echo $banner_image['sm_pic_5_data_cls']; ?>" /></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>Caption 1</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Text</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_1]" value="<?php echo $banner_image['caption_1']; ?>" /></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_1_data_x]" value="<?php echo $banner_image['caption_1_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_1_data_y]" value="<?php echo $banner_image['caption_1_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_1_data_sp]" value="<?php echo $banner_image['caption_1_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_1_data_st]" value="<?php echo $banner_image['caption_1_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][caption_1_data_ea]" id="banner_image[<?php echo $image_row; ?>][caption_1_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["caption_1_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_1_data_cls]" value="<?php echo $banner_image['caption_1_data_cls']; ?>" /></td>
</tr>
<tr>
<td>URL</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_1_url]" value="<?php echo $banner_image['caption_1_url']; ?>" /></td>
</tr>
</table>
</td>
<td>Caption 2</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Text</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_2]" value="<?php echo $banner_image['caption_2']; ?>" /></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_2_data_x]" value="<?php echo $banner_image['caption_2_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_2_data_y]" value="<?php echo $banner_image['caption_2_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_2_data_sp]" value="<?php echo $banner_image['caption_2_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_2_data_st]" value="<?php echo $banner_image['caption_2_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][caption_2_data_ea]" id="banner_image[<?php echo $image_row; ?>][caption_2_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["caption_2_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_2_data_cls]" value="<?php echo $banner_image['caption_2_data_cls']; ?>" /></td>
</tr>
<tr>
<td>URL</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_2_url]" value="<?php echo $banner_image['caption_2_url']; ?>" /></td>
</tr>
</table>
</td>
<td>Caption 3</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Text</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_3]" value="<?php echo $banner_image['caption_3']; ?>" /></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_3_data_x]" value="<?php echo $banner_image['caption_3_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_3_data_y]" value="<?php echo $banner_image['caption_3_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_3_data_sp]" value="<?php echo $banner_image['caption_3_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_3_data_st]" value="<?php echo $banner_image['caption_3_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][caption_3_data_ea]" id="banner_image[<?php echo $image_row; ?>][caption_3_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["caption_3_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_3_data_cls]" value="<?php echo $banner_image['caption_3_data_cls']; ?>" /></td>
</tr>
<tr>
<td></td><td></td>
</tr>
</table>
</td>
<td>Caption 4</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Text</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_4]" value="<?php echo $banner_image['caption_4']; ?>" /></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_4_data_x]" value="<?php echo $banner_image['caption_4_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_4_data_y]" value="<?php echo $banner_image['caption_4_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_4_data_sp]" value="<?php echo $banner_image['caption_4_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_4_data_st]" value="<?php echo $banner_image['caption_4_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][caption_4_data_ea]" id="banner_image[<?php echo $image_row; ?>][caption_4_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["caption_4_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_4_data_cls]" value="<?php echo $banner_image['caption_4_data_cls']; ?>" /></td>
</tr>
<tr>
<td></td><td></td>
</tr>
</table>
</td>
<td>Caption 5</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Text</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_5]" value="<?php echo $banner_image['caption_5']; ?>" /></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_5_data_x]" value="<?php echo $banner_image['caption_5_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_5_data_y]" value="<?php echo $banner_image['caption_5_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_5_data_sp]" value="<?php echo $banner_image['caption_5_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_5_data_st]" value="<?php echo $banner_image['caption_5_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][caption_5_data_ea]" id="banner_image[<?php echo $image_row; ?>][caption_5_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["caption_5_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][caption_5_data_cls]" value="<?php echo $banner_image['caption_5_data_cls']; ?>" /></td>
</tr>
<tr>
<td></td><td></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>Button 1</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Text</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][button_1]" value="<?php echo $banner_image['button_1']; ?>" /></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][button_1_data_x]" value="<?php echo $banner_image['button_1_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][button_1_data_y]" value="<?php echo $banner_image['button_1_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][button_1_data_sp]" value="<?php echo $banner_image['button_1_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][button_1_data_st]" value="<?php echo $banner_image['button_1_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][button_1_data_ea]" id="banner_image[<?php echo $image_row; ?>][button_1_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["button_1_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["button_1_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Class</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][button_1_data_cls]" value="<?php echo $banner_image['button_1_data_cls']; ?>" /></td>
</tr>
<tr>
<td>URL</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][button_1_url]" value="<?php echo $banner_image['button_1_url']; ?>" /></td>
</tr>
</table>
</td>
<td>YouTube</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>YouTube Iframe Link</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][youtube_iframe]" value="<?php echo $banner_image['youtube_iframe']; ?>" /></td>
</tr>
<tr>
<td>AutoPlay</td><td><select name="banner_image[<?php echo $image_row; ?>][youtube_autoplay]" id="banner_image[<?php echo $image_row; ?>][youtube_autoplay]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="true" '. <?php echo ($banner_image["youtube_autoplay"]=="true")   ? 'selected="selected"' : '' ?>. '>Yes</option>
              <option value="">------------------------</option>
               <option value="false" '. <?php echo ($banner_image["youtube_autoplay"]=="false")   ? 'selected="selected"' : '' ?>. '>Not</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][youtube_data_x]" value="<?php echo $banner_image['youtube_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][youtube_data_y]" value="<?php echo $banner_image['youtube_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][youtube_data_sp]" value="<?php echo $banner_image['youtube_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][youtube_data_st]" value="<?php echo $banner_image['youtube_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][youtube_data_ea]" id="banner_image[<?php echo $image_row; ?>][youtube_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["youtube_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["youtube_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Fullscreen</td><td><select name="banner_image[<?php echo $image_row; ?>][youtube_data_cls]" id="banner_image[<?php echo $image_row; ?>][youtube_data_cls]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="fullscreenvideo caption fade" '. <?php echo ($banner_image["youtube_data_cls"]=="fullscreenvideo caption fade")   ? 'selected="selected"' : '' ?>. '>Yes</option>
              <option value="">------------------------</option>
               <option value="boxshadow caption lft" '. <?php echo ($banner_image["youtube_data_cls"]=="boxshadow caption lft")   ? 'selected="selected"' : '' ?>. '>Not</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Height</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][youtube_data_h]" value="<?php echo $banner_image['youtube_data_h']; ?>" /></td>
</tr>
<tr>
<td>Width</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][youtube_data_w]" value="<?php echo $banner_image['youtube_data_w']; ?>" /></td>
</tr>
</table>
</td>
<td>Vimeo</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Vimeo Iframe Link</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][vimeo_iframe]" value="<?php echo $banner_image['vimeo_iframe']; ?>" /></td>
</tr>
<tr>
<td>AutoPlay</td><td><select name="banner_image[<?php echo $image_row; ?>][vimeo_autoplay]" id="banner_image[<?php echo $image_row; ?>][vimeo_autoplay]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="true" '. <?php echo ($banner_image["vimeo_autoplay"]=="true")   ? 'selected="selected"' : '' ?>. '>Yes</option>
              <option value="">------------------------</option>
               <option value="false" '. <?php echo ($banner_image["vimeo_autoplay"]=="false")   ? 'selected="selected"' : '' ?>. '>Not</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Data X</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][vimeo_data_x]" value="<?php echo $banner_image['vimeo_data_x']; ?>" /></td>
</tr>
<tr>
<td>Data Y</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][vimeo_data_y]" value="<?php echo $banner_image['vimeo_data_y']; ?>" /></td>
</tr>
<tr>
<td>Data Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][vimeo_data_sp]" value="<?php echo $banner_image['vimeo_data_sp']; ?>" /></td>
</tr>
<tr>
<td>Data Start</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][vimeo_data_st]" value="<?php echo $banner_image['vimeo_data_st']; ?>" /></td>
</tr>
<tr>
<td>Easing</td><td><select name="banner_image[<?php echo $image_row; ?>][vimeo_data_ea]" id="banner_image[<?php echo $image_row; ?>][vimeo_data_ea]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="easeOutBack" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInQuad" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInQuad")   ? 'selected="selected"' : '' ?>. '>easeInQuad</option>
               <option value="easeOutQuad" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutQuad")   ? 'selected="selected"' : '' ?>. '>easeOutQuad</option>
               <option value="easeInOutQuad" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutQuad")   ? 'selected="selected"' : '' ?>. '>easeInOutQuad</option>
               <option value="easeInCubic" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInCubic")   ? 'selected="selected"' : '' ?>. '>easeInCubic</option>
               <option value="easeOutCubic" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutCubic")   ? 'selected="selected"' : '' ?>. '>easeOutCubic</option>
               <option value="easeInOutCubic" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutCubic")   ? 'selected="selected"' : '' ?>. '>easeInOutCubic</option>
               <option value="easeInQuart" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInQuart")   ? 'selected="selected"' : '' ?>. '>easeInQuart</option>
               <option value="easeOutQuart" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutQuart")   ? 'selected="selected"' : '' ?>. '>easeOutQuart</option>
               <option value="easeInOutQuart " '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutQuart ")   ? 'selected="selected"' : '' ?>. '>easeInOutQuart </option>
               <option value="easeInQuint" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInQuint")   ? 'selected="selected"' : '' ?>. '>easeInQuint</option>
               <option value="easeOutQuint" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutQuint")   ? 'selected="selected"' : '' ?>. '>easeOutQuint</option>
               <option value="easeInOutQuint" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutQuint")   ? 'selected="selected"' : '' ?>. '>easeInOutQuint</option>
               <option value="easeInSine" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInSine")   ? 'selected="selected"' : '' ?>. '>easeInSine</option>
               <option value="easeOutSine" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutSine")   ? 'selected="selected"' : '' ?>. '>easeOutSine</option>
               <option value="easeInOutSine" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutSine")   ? 'selected="selected"' : '' ?>. '>easeInOutSine</option>
               <option value="easeInExpo" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInExpo")   ? 'selected="selected"' : '' ?>. '>easeInExpo</option>
               <option value="easeOutExpo" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutExpo")   ? 'selected="selected"' : '' ?>. '>easeOutExpo</option>
               <option value="easeInOutExpo" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutExpo")   ? 'selected="selected"' : '' ?>. '>easeInOutExpo</option>
               <option value="easeInCirc" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInCirc")   ? 'selected="selected"' : '' ?>. '>easeInCirc</option>
               <option value="easeOutCirc" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutCirc")   ? 'selected="selected"' : '' ?>. '>easeOutCirc</option>
               <option value="easeInOutCirc" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutCirc")   ? 'selected="selected"' : '' ?>. '>easeInOutCirc</option>
               <option value="easeInElastic" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInElastic")   ? 'selected="selected"' : '' ?>. '>easeInElastic</option>
               <option value="easeOutElastic" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutElastic")   ? 'selected="selected"' : '' ?>. '>easeOutElastic</option>
               <option value="easeInOutElastic" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutElastic")   ? 'selected="selected"' : '' ?>. '>easeInOutElastic</option>
               <option value="easeInBack" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInBack")   ? 'selected="selected"' : '' ?>. '>easeInBack</option>
               <option value="easeOutBack" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutBack")   ? 'selected="selected"' : '' ?>. '>easeOutBack</option>
               <option value="easeInOutBack" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutBack")   ? 'selected="selected"' : '' ?>. '>easeInOutBack</option>
               <option value="easeInBounce" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInBounce")   ? 'selected="selected"' : '' ?>. '>easeInBounce</option>
               <option value="easeOutBounce" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeOutBounce")   ? 'selected="selected"' : '' ?>. '>easeOutBounce</option>
               <option value="easeInOutBounce" '. <?php echo ($banner_image["vimeo_data_ea"]=="easeInOutBounce")   ? 'selected="selected"' : '' ?>. '>easeInOutBounce</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Fullscreen</td><td><select name="banner_image[<?php echo $image_row; ?>][vimeo_data_cls]" id="banner_image[<?php echo $image_row; ?>][vimeo_data_cls]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="fullscreenvideo caption fade" '. <?php echo ($banner_image["vimeo_data_cls"]=="fullscreenvideo caption fade")   ? 'selected="selected"' : '' ?>. '>Yes</option>
              <option value="">------------------------</option>
               <option value="boxshadow caption lft" '. <?php echo ($banner_image["vimeo_data_cls"]=="boxshadow caption lft")   ? 'selected="selected"' : '' ?>. '>Not</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Height</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][vimeo_data_h]" value="<?php echo $banner_image['vimeo_data_h']; ?>" /></td>
</tr>
<tr>
<td>Width</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][vimeo_data_w]" value="<?php echo $banner_image['vimeo_data_w']; ?>" /></td>
</tr>
</table>
</td>
<td>Master Settings</td><td>
<table cellpadding="0" cellspacing="4" border="0" width="100%">
<tr>
<td>Transition</td><td><select name="banner_image[<?php echo $image_row; ?>][transicion]" id="banner_image[<?php echo $image_row; ?>][transicion]"> 
              <option value="" selected>Select</option>
              <option value="">---------RANDOM---------</option>
              <option value="random-static" '. <?php echo ($banner_image["transicion"]=="random-static")   ? 'selected="selected"' : '' ?>. '>random-static</option>
              <option value="">-------INDIVIDUAL-------</option>
              <option value="boxslide" '. <?php echo ($banner_image["transicion"]=="boxslide")   ? 'selected="selected"' : '' ?>. '>boxslide</option>
              <option value="boxfade" '. <?php echo ($banner_image["transicion"]=="boxfade")   ? 'selected="selected"' : '' ?>. '>boxfade</option>
               <option value="slotslide-horizontal" '. <?php echo ($banner_image["transicion"]=="slotslide-horizontal")   ? 'selected="selected"' : '' ?>. '>slotslide-horizontal</option>
               <option value="slotslide-vertical" '. <?php echo ($banner_image["transicion"]=="slotslide-vertical")   ? 'selected="selected"' : '' ?>. '>slotslide-vertical</option>
               <option value="curtain-1" '. <?php echo ($banner_image["transicion"]=="curtain-1")   ? 'selected="selected"' : '' ?>. '>curtain-1</option>
               <option value="curtain-2" '. <?php echo ($banner_image["transicion"]=="curtain-2")   ? 'selected="selected"' : '' ?>. '>curtain-2</option>
               <option value="curtain-3" '. <?php echo ($banner_image["transicion"]=="curtain-3")   ? 'selected="selected"' : '' ?>. '>curtain-3</option>
               <option value="slotzoom-horizontal" '. <?php echo ($banner_image["transicion"]=="slotzoom-horizontal")   ? 'selected="selected"' : '' ?>. '>slotzoom-horizontal</option>
               <option value="slotzoom-vertical" '. <?php echo ($banner_image["transicion"]=="slotzoom-vertical")   ? 'selected="selected"' : '' ?>. '>slotzoom-vertical</option>
               <option value="slotfade-horizontal" '. <?php echo ($banner_image["transicion"]=="slotfade-horizontal")   ? 'selected="selected"' : '' ?>. '>slotfade-horizontal</option>
               <option value="slotfade-vertical" '. <?php echo ($banner_image["transicion"]=="slotfade-vertical")   ? 'selected="selected"' : '' ?>. '>slotfade-vertical</option>
               <option value="fade" '. <?php echo ($banner_image["transicion"]=="fade")   ? 'selected="selected"' : '' ?>. '>fade</option>
               <option value="slideleft" '. <?php echo ($banner_image["transicion"]=="slideleft")   ? 'selected="selected"' : '' ?>. '>slideleft</option>
               <option value="slideup" '. <?php echo ($banner_image["transicion"]=="slideup")   ? 'selected="selected"' : '' ?>. '>slideup</option>
               <option value="slidedown" '. <?php echo ($banner_image["transicion"]=="slidedown")   ? 'selected="selected"' : '' ?>. '>slidedown</option>
               <option value="slideright" '. <?php echo ($banner_image["transicion"]=="slideright")   ? 'selected="selected"' : '' ?>. '>slideright</option>
               <option value="">--------PREMIUM---------</option>
               <option value="papercut" '. <?php echo ($banner_image["transicion"]=="papercut")   ? 'selected="selected"' : '' ?>. '>papercut</option>
               <option value="3dcurtain-horizontal" '. <?php echo ($banner_image["transicion"]=="3dcurtain-horizontal")   ? 'selected="selected"' : '' ?>. '>3dcurtain-horizontal</option>
               <option value="3dcurtain-vertical" '. <?php echo ($banner_image["transicion"]=="3dcurtain-vertical")   ? 'selected="selected"' : '' ?>. '>3dcurtain-vertical</option>
               <option value="cubic" '. <?php echo ($banner_image["transicion"]=="cubic")   ? 'selected="selected"' : '' ?>. '>cubic</option>
               <option value="flyin" '. <?php echo ($banner_image["transicion"]=="flyin")   ? 'selected="selected"' : '' ?>. '>flyin</option>
               <option value="turnoff" '. <?php echo ($banner_image["transicion"]=="turnoff")   ? 'selected="selected"' : '' ?>. '>turnoff</option>
               <option value="">------------------------</option>
               </select>
</td>
</tr>
<tr>
<td>Slot Amount</td><td><select name="banner_image[<?php echo $image_row; ?>][slot_amt]" id="banner_image[<?php echo $image_row; ?>][slot_amt]"> 
              <option value="" selected>Select</option>
              <option value="">------------------------</option>
              <option value="1" '. <?php echo ($banner_image["slot_amt"]=="1")   ? 'selected="selected"' : '' ?>. '>1</option>
               <option value="2" '. <?php echo ($banner_image["slot_amt"]=="2")   ? 'selected="selected"' : '' ?>. '>2</option>
               <option value="3" '. <?php echo ($banner_image["slot_amt"]=="3")   ? 'selected="selected"' : '' ?>. '>3</option>
               <option value="4" '. <?php echo ($banner_image["slot_amt"]=="4")   ? 'selected="selected"' : '' ?>. '>4</option>
               <option value="5" '. <?php echo ($banner_image["slot_amt"]=="5")   ? 'selected="selected"' : '' ?>. '>5 (Recomended)</option>
               <option value="6" '. <?php echo ($banner_image["slot_amt"]=="6")   ? 'selected="selected"' : '' ?>. '>6</option>
               <option value="7" '. <?php echo ($banner_image["slot_amt"]=="7")   ? 'selected="selected"' : '' ?>. '>7</option>
               <option value="8" '. <?php echo ($banner_image["slot_amt"]=="8")   ? 'selected="selected"' : '' ?>. '>8</option>
               <option value="">------------------------</option>
               </select></td>
</tr>
<tr>
<td>Data Delay</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][data_delay]" value="<?php echo $banner_image['data_delay']; ?>" /></td>
</tr>
<tr>
<td>Master Speed</td><td><input type="text" name="banner_image[<?php echo $image_row; ?>][master_speed]" value="<?php echo $banner_image['master_speed']; ?>" /></td>
</tr>
</table>
</td>
</tr>
</table>
</li>
</ul>
</div>
</td>
</tr>
<?php } else { ?>
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1]" value=""  />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_h]" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_w]" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_ea]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_data_cls]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_1_url]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2]" value=""  />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_h]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_w]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_ea]"> 
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_data_cls]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_2_url]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3]" value=""  />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_h]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_w]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_ea]"  /> 
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_3_data_cls]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4]" value="<?php echo $banner_image['sm_pic_4']; ?>" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_h]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_w]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_ea]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_4_data_cls]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_h]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_w]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_ea]" value=""> 
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][sm_pic_5_data_cls]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1_data_ea]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1_data_cls]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_1_url]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2_data_ea]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2_data_cls]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_2_url]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_3]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_3_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_3_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_3_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_3_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_3_data_ea]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_3_data_cls]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_4]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_4_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_4_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_4_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_4_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_4_data_ea]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_4_data_cls]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_5]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_5_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_5_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_5_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_5_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_5_data_ea]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][caption_5_data_cls]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1]" value="<?php echo $banner_image['button_1']; ?>" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1_data_x]" value="<?php echo $banner_image['button_1_data_x']; ?>" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1_data_y]" value="<?php echo $banner_image['button_1_data_y']; ?>" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1_data_sp]" value="<?php echo $banner_image['button_1_data_sp']; ?>" /><input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1_data_st]" value="<?php echo $banner_image['button_1_data_st']; ?>" /><input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1_data_ea]" id="banner_image[<?php echo $image_row; ?>][button_1_data_ea]"> 
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1_data_cls]" value="<?php echo $banner_image['button_1_data_cls']; ?>" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][button_1_url]" value="<?php echo $banner_image['button_1_url']; ?>" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_iframe]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_autoplay]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_ea]" value="" /> 
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_cls]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_h]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][youtube_data_w]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_iframe]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_autoplay]" value="">
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_x]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_y]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_sp]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_st]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_ea]" value="" /> 
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_cls]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_h]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][vimeo_data_w]" value="" />

<input type="hidden" name="banner_image[<?php echo $image_row; ?>][transicion]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][slot_amt]" value="">    
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][data_delay]" value="" />
<input type="hidden" name="banner_image[<?php echo $image_row; ?>][master_speed]" value="" />
      <?php } ?>    
      <?php $image_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="3"></td>
              <td class="left"><a onclick="addImage();" class="button"><?php echo $button_add_banner; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '<tr>';
    html += '<td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<input type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';	
	html += '<td class="left"><input type="text" name="banner_image[' + image_row + '][link]" value="" /></td>';	
	html += '<td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="banner_image[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
	html += '<td class="left">&nbsp;&nbsp;Beverlysion&#153&nbsp;Advanced&nbsp;&nbsp;<select name="banner_image[<?php echo $image_row; ?>][beverlysion_act]" id="banner_image[<?php echo $image_row; ?>][beverlysion_act]"><option value="" selected>Select</option><option value="">------------------------</option><option value="1" >Yes</option><option value="">------------------------</option><option value="0" >No</option><option value="">------------------------</option></select>&nbsp;&nbsp;<a onclick="$(\'#image-row' + image_row  + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '<input type="hidden" name="banner_image[' + image_row + '][transicion]" value="" /><input type="hidden" name="banner_image[' + image_row + '][slot_amt]" value="" /><input type="hidden" name="banner_image[' + image_row + '][data_delay]" value="" /><input type="hidden" name="banner_image[' + image_row + '][master_speed]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_url]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_url]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_1_url]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_2_url]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_3]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_3_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_3_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_3_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_3_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_3_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_3_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_4]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_4_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_4_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_4_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_4_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_4_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_4_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_5]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_5_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_5_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_5_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_5_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_5_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][caption_5_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][button_1_url]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_iframe]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_autoplay]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_iframe]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_autoplay]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_x]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_y]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_sp]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_st]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_ea]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_cls]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_h]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_1_data_w]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_h]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_2_data_w]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_h]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_3_data_w]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_h]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_4_data_w]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_h]" value="" /><input type="hidden" name="banner_image[' + image_row + '][sm_pic_5_data_w]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_h]" value="" /><input type="hidden" name="banner_image[' + image_row + '][youtube_data_w]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_h]" value="" /><input type="hidden" name="banner_image[' + image_row + '][vimeo_data_w]" value="" />';
	html += '</tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
}
//--></script>
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 700,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<?php echo $footer; ?>