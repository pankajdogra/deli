<?php
//Licensing
$_['regerror_email']	= 'Email Address Error';
$_['regerror_orderid']	= 'Order ID Error';
$_['regerror_noreferer']	= 'Please Contact Support';
$_['regerror_localhost']	= 'This extension can not be used on a localhost server!';
$_['regerror_licensedupe']	= 'This extension is already licensed to a different domain!';
$_['regerror_quote_msg'] = '<p>Please quote the error msg below when contacting support.</p>';

$_['license_purchase_thanks'] = '<p>Thanks for purchasing our OpenCart Extension.</p>
    <p>One final step is required to complete installation.</p>
    <p>Please complete the form below to register this extension on our licensing server.</p>
    <p>If you experience any problems installing this extension or registering your license, please raise a support ticket at <a href="http://www.support.uksitebuilder.net">http://www.support.uksitebuilder.net</a></p>';

$_['license_registration'] = 'Register License';
$_['license_opencart_email'] = 'OpenCart Extension Store Email Address';
$_['license_opencart_orderid'] = 'OpenCart Extension Store Order ID';

$_['check_email'] = 'Please check and correct your email address';
$_['check_orderid'] = 'Please check and correct your order id';

$_['server_error_curl'] = '<h2>Server Error - Curl Required</h2>
    <p>Your server does not appear to have th \'curl\' PHP module installed. The \'curl\' PHP module is required for OpenCart to function correcty. Pleae contact your web host to ask them to install the \'curl\' PHP module for you.</p>';
?>