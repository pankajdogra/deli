<?php

// Heading
$_['heading_title']    = 'Tracking Discount';

// Text
$_['text_total']       = 'Order totals';
$_['text_success']     = 'Success: You have modified tracking discount!';
$_['text_percent']     = 'Percent (%)';
$_['text_fixed']       = 'Fixed (%s)';

$_['text_when_all']    = 'to All orders';
$_['text_when_first']  = 'to First order only';
$_['text_when_period'] = 'Periodically';

$_['text_what_total']   = 'Total';
$_['text_what_all_all'] = 'All products with quantities';
$_['text_what_all_one'] = 'All products, but only 1 item of each';
$_['text_what_min_all'] = 'Least expensive product with quantities';
$_['text_what_min_one'] = 'Least expensive product, only 1 item';
$_['text_what_max_all'] = 'Most expensive product with quantities';
$_['text_what_max_one'] = 'Most expensive product, only 1 item';

// Entry
$_['entry_amount']     = 'Discount Amount:';
$_['entry_type']       = 'Discount Type:';
$_['entry_fixed_max']  = 'Maximum discount for Fixed type (%):<br /><span class="help">if fixed discount amount is higher than this % of the product price, it will be reduced. E.g. price is 10, discount is 6 (60%), this value is set to 50% - discount will be reduced to 5 (50%).</span>';
$_['entry_when']       = 'Apply Discount:<br /><span class="help">settings this to anything except <i>All orders</i> will also deny the discount to unregistered (guest) customers !</span>';
$_['entry_period']     = 'Discount Period (days):<br /><span class="help">if &apos;Apply Discount to orders&apos; set to Periodically, indicate the period in days here, e.g. 3 means that the customer will get the discount once every 3 days</span>';
$_['entry_what']       = 'Apply Discount:<br /><span class="help">to products in the order. If set to Total, discount will be applied to Total value according to Sort order, e.g. if Sub Total has sort order 1, and tracking discount has sort order 2, discount will be applied to Sub Total value. It is possible to include shipping etc.</span>';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify tracking discount!';
$_['error_amount']     = 'Error: Discount amount should be integer or floating point number, e.g. 10, 12.5 etc.';
$_['error_fixed_max']  = 'Error: Maximum discount for Fixed type should be integer or floating point number, e.g. 10, 12.5 etc.';
$_['error_period']     = 'Error: Discount period should be integer number, e.g. 2, 10 etc.';
$_['error_sort_order'] = 'Error: Sort order should be integer number, e.g. 2, 10 etc.';



