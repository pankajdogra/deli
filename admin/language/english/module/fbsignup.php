<?php
// Heading
$_['heading_title']       = 'FB Login and Register';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module FB Login and Register!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_browse']         = 'Browse Files';
$_['text_clear']          = 'Clear Image';
$_['text_image_manager']  = 'Image Manager';
$_['text_module_settings'] = 'Note: These settings will be applied to all the modules in the list below.';
$_['text_support'] = 'For more info, issues report, suggestions or features request please Email:- <mailto>stillblacklisted@gmail.com</mailto> Leave a comment on support desk <a href="http://polapainz.com/support" target="_blank"> Click here</a>.';
$_['text_button_settings']  = 'Settings Button';

//text redirect 
$_['text_home']  = 'Home';
$_['text_account']  = 'Account';
$_['text_cart']  = 'Cart';
$_['text_contact']  = 'Contact';
$_['text_wishlist']  = 'WishList';
$_['text_order']  = 'Order';
$_['text_newletter']  = 'New Letter';

// Entry

$_['entry_version']      = 'Version: 1.0 ';

$_['entry_setting']       = 'Facebook API Setting';
$_['entry_apikey']        = 'Facebook Login Application API Key:';
$_['entry_apisecret']     = 'Facebook Login Application API Secret:';

$_['entry_pwdsecret']     = 'Password Encryption Key:';
$_['entry_pwdsecret_desc']= 'Make the customers password more safe, 4 to 8 complex characters (eg. 5Qe3# ).';

$_['entry_show_setting']      = 'Display Settings';
$_['entry_show_desc']= 'Select the type of button you will use, with or image effect.';

$_['entry_show_box']      = 'Display box?';
$_['entry_box_desc']= 'Select if you want it then appears a box around the button.';

$_['entry_button']	  = 'Efect Button';
$_['entry_button_desc']	  = 'Button text that is eg:Connect with Facebook';

$_['entry_redirect']	  = 'Redirection';
$_['entry_redirect_desc']	  = 'Where will the person after connecting with facebook';

$_['entry_boxtitle']	  = 'Title Box:';
$_['entry_boxtitle_desc']	  = 'Indicates the title of the box only be seen if "Display box" is enable. ';

$_['entry_botonfc']       = 'Imagen Button';

$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

$_['entry_creator']      = '<br /><a target="_blank" href="http://polapainz.com/support">Developed By Saad Bin Saif</a>';


// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module most viewed!';
$_['error_code']         = 'Error: Facebook API Key, Facebook API Secret and Password Encryption Key required!';
?>