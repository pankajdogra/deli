<?php
// Heading
$_['heading_title']       = 'FacebookComments 1.1.2';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module FacebookComments!';
$_['text_success_activation']        = 'ACTIVATED: You have successfully activated FacebookComments!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_code']          = 'Facebook Comments status:<br /><span class="help">Enable or disable FacebookComments</span>';
$_['entry_layouts_active']          = 'Activated on:<br /><span class="help">Choose which pages FacebookComments to be active</span>';



// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module FacebookComments!';

?>