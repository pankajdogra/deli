<?php
// Heading
$_['heading_title']      = 'Auction Module';
$_['tab_position']       = 'Module Position';
$_['tab_settings']       = 'Module Settings';
$_['tab_about']       = 'About';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Auction Module!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_show_quantity_left']  = 'Show quantity left in products';
$_['text_compact']         = 'Show Compact mode in product page';
$_['text_labels_singular']         = 'Labels Singular';
$_['text_labels_plural']         = 'Labels Plural';
$_['text_compact_labels']         = 'Compact Labels';
$_['text_time_seperator']         ='Time Separator';


// Entry
$_['entry_limit']         = 'Limit:';
$_['entry_image']         = 'Image (W x H):';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Auction Module!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>