<?php
// Heading
$_['heading_title']    = 'WebRotate 360';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module WebRotate 360!';
$_['entry_status']     = 'Module status';
$_['button_clear']     = 'Clear';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module WebRotate 360!';
?>