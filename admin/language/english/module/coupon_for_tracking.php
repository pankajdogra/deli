<?php
// Heading
$_['heading_title']       = 'Coupon for Tracking';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified Coupon for Tracking module!';

// Entry
$_['entry_always']        = 'Use coupon code for affiliate tracking <i><b>always</b></i>, even when coupon itself can not be applied (expired, disabled, etc.):';

