<?php
// Heading
$_['heading_title']          = 'Auto Bids'; 

// Text  
$_['text_success']           = 'Success: You have modified Auto Bid list!';

$_['button_view']           = 'View';

$_['button_bids']           = 'Bids';

$_['text_auction']           = 'Success: Auction was successfully modified !';

$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse Files';
$_['text_clear']             = 'Clear Image';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

$_['text_bid_only_reg']    = 'Auction has ended!';
$_['text_bid_start_from']  = 'Bidding start from:';
$_['text_bid_curr']  		= 'Auction In Progress';
$_['text_bid_bids'] 			= 'Nobody';
$_['text_bid_place'] 		= 'PLACE YOUR BID';
$_['text_bid_submit'] 		= 'SUBMIT';
$_['text_bid_delete'] 		= 'DELETE';
$_['text_bid_curr_is'] 		= 'Starting bid:';

$_['text_close_time'] 		= 'Auction Close Time:';
$_['text_state'] 		= 'Auction State:';
$_['text_progress'] 		= 'In progress';
$_['text_closed'] 		= 'Closed';
$_['text_winner'] 		= 'Winning bidder:';
$_['text_current_price'] 		= 'Current price:';
$_['text_part'] 		= 'Buy Now Price:';
$_['text_min_step'] 		= 'Minimal bidding amount:';

// Column
$_['column_names']            = 'Name';
$_['column_nickname']           = 'Nickname';
$_['column_bid_price']           = 'Price';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'Description:';
$_['entry_store']            = 'Stores:';
$_['entry_keyword']          = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:';
$_['entry_upc']              = 'UPC:';
$_['entry_location']         = 'Location:';
$_['entry_manufacturer']     = 'Manufacturer:';
$_['entry_shipping']         = 'Requires Shipping:'; 
$_['entry_date_available']   = 'Date Available:';
$_['entry_quantity']         = 'Quantity:';
$_['entry_minimum']          = 'Minimum Quantity:<br/><span class="help">Force a minimum ordered amount</span>';
$_['entry_stock_status']     = 'Out Of Stock Status:<br/><span class="help">Status shown when a product is out of stock</span>';
$_['entry_price']            = 'Price:';
$_['entry_tax_class']        = 'Tax Class:';
$_['entry_points']           = 'Points:<br/><span class="help">Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.</span>';
$_['entry_option_points']    = 'Points:';
$_['entry_subtract']         = 'Subtract Stock:';
$_['entry_weight_class']     = 'Weight Class:';
$_['entry_weight']           = 'Weight:';
$_['entry_length']           = 'Length Class:';
$_['entry_dimension']        = 'Dimensions (L x W x H):';
$_['entry_image']            = 'Image:';
$_['entry_customer_group']   = 'Customer Group:';
$_['entry_date_start']       = 'Date Start:';
$_['entry_date_end']         = 'Date End:';
$_['entry_priority']         = 'Priority:';
$_['entry_attribute']        = 'Attribute:';
$_['entry_attribute_group']  = 'Attribute Group:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Option:';
$_['entry_option_value']     = 'Option Value:';
$_['entry_required']         = 'Required:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_category']         = 'Categories:';
$_['entry_download']         = 'Downloads:';
$_['entry_related']          = 'Related Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']          	 = 'Product Tags:<br /><span class="help">comma separated</span>';
$_['entry_reward']           = 'Reward Points:';
$_['entry_layout']           = 'Layout Override:';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';

/**auction**/
$_['tab_auction']           = 'Auction';
$_['tab_auction']           = 'Auction';
$_['column_bid_start_price']           = 'Bid Start Price';
$_['column_ban']           = 'Ban';
$_['column_bid_date_end']           = 'Date Added';
																						/**auction**/
?>