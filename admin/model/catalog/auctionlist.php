<?php
class ModelCatalogAuctionlist extends Model {


	public function checkforMaxProductBids($product_id){
		$query = $this->db->query("SELECT MAX(price_bid) as max_price_bid FROM " . DB_PREFIX . "customer_bids WHERE product_id=".(int)$product_id." LIMIT 1");
		if (isset($query->row['max_price_bid'])){
		$max_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_bids WHERE price_bid=".$query->row['max_price_bid']."");
		}		
		if (isset($max_query->row['price_bid']) && isset($max_query->row['customer_id'])) {
			return array($max_query->row['customer_id'],$max_query->row['price_bid'],$max_query->row['accept'],$max_query->row['date_added']);		
		}else{
			return 0;		
		}
	}
	
	public function countCustomerBids($product_id){
		$query = $this->db->query("SELECT COUNT(price_bid) as num FROM " . DB_PREFIX . "customer_bids WHERE product_id=".(int)$product_id."");
		return $query->row['num'];
	}
	
	
	public function checkBidstate($product_id){
	
		$query = $this->db->query("SELECT bid_close_status FROM " . DB_PREFIX . "product_bid WHERE product_id=".(int)$product_id."");
		return $query->row['bid_close_status'];
		
	}
	
	public function checkwinner($product_id){
	
		$query = $this->db->query("SELECT MAX(price_bid) as max_price_bid FROM " . DB_PREFIX . "customer_bids WHERE product_id=".(int)$product_id." LIMIT 1");
		
		if (isset($query->row['max_price_bid'])){
		$max_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_bids WHERE price_bid=".$query->row['max_price_bid']."");
		}
		
		if (isset($max_query->row['price_bid']) && isset($max_query->row['customer_id'])) {
			return array($max_query->row['customer_id'],$max_query->row['price_bid'],$max_query->row['name'],$max_query->row['date_added'],$max_query->row['nickname']);		
		}else{
			return "";		
		}
	}
	
	public function forMaxProductBidstable($product_id){
	   $query = $this->db->query("SELECT COUNT(bid_max_price) as num FROM " . DB_PREFIX . "product_bid WHERE 
		product_id=".(int)$product_id." AND max_price_status='1' LIMIT 1");
		return $query->row['num'];
	}
	
	
	public function forMaxProductBids1($product_id){
	   $query = $this->db->query("SELECT bid_max_price FROM " . DB_PREFIX . "product_bid WHERE 
		product_id=".(int)$product_id." AND max_price_status='1' LIMIT 1");
		if(isset($query->row['bid_max_price'])){
		
		return $query->row['bid_max_price'];
		}else{
		return "";
		}
	}
	
	
	public function forMaxProductBids($product_id){
	 
	  	  
		$query = $this->db->query("SELECT MAX(price_bid) as max_price_bid FROM " . DB_PREFIX . "customer_bids WHERE product_id=".(int)$product_id." LIMIT 1");
		
		
		if (isset($query->row['max_price_bid'])){
		return $query->row['max_price_bid'];		
		}else{
		return 0;
		}
	}
	
	public function addwinner($product_id,$customer_id,$name,$price_bid,$pname){
		$query = $this->db->query("INSERT INTO " . DB_PREFIX . "winner SET 
		product_id=".(int)$product_id.", 
		customer_id=".(int)$customer_id.", 
		name = '".$this->db->escape($name)."',
		nickname = '".$this->db->escape($name)."',
		status='0',
		ban='0',
		productname='".$this->db->escape($pname)."',
		price_bid=".(int)$price_bid.", date_added=NOW()");
		return 1;
	}
	
	public function updatewinner($product_id,$customer_id,$name,$price_bid,$pname){
		$query = $this->db->query("UPDATE  " . DB_PREFIX . "winner SET 
		product_id=".(int)$product_id.", 
		name = '".$this->db->escape($name)."',
		nickname = '".$this->db->escape($name)."',
		status='0',
		ban='0',
		productname='".$this->db->escape($pname)."',
		price_bid=".(int)$price_bid.", date_added=NOW()
		WHERE customer_id=".(int)$customer_id."
		");
		return 1;
	}
	
	public function closeAuctionlist($auction_id,$status) {
	
		$this->db->query("UPDATE " . DB_PREFIX . "product_bid 
		SET bid_close_status ='" . (int)($status) . "'
		WHERE product_bid_id = '" . (int)$auction_id . "'");
		
		
		
		
		return 1;
		
	}
	
	
	public function closeAuctionlist1($auction_id,$status,$product_id) {
	
	
	   $query = $this->db->query("SELECT MAX(price_bid) as max_price_bid FROM " . DB_PREFIX . "customer_bids WHERE product_id=".(int)$product_id." LIMIT 1");
		
		if (isset($query->row['max_price_bid'])){
		$max_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_bids WHERE price_bid=".$query->row['max_price_bid']."");
		}
		
		
		
	if (isset($max_query->row['price_bid']) && isset($max_query->row['customer_id'])) {
		
		$product_query = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id=".$max_query->row['product_id']."");
		
		$customer_query = $this->db->query("SELECT email FROM " . DB_PREFIX . "customer WHERE customer_id=".$max_query->row['customer_id']."");
		
		
			$customer_id=$max_query->row['customer_id'];
			$product_id=$max_query->row['product_id'];
			$pname=$product_query->row['name'];
			$price_bid=$max_query->row['price_bid'];
			$name=$max_query->row['name'];
			$date=$max_query->row['date_added'];
			$nickname=$max_query->row['nickname'];
			
			$email = $customer_query->row['email'];

			$insertquery = $this->db->query("INSERT INTO " . DB_PREFIX . "winner SET 
			product_id=".(int)$product_id.", 
			customer_id=".(int)$customer_id.", 
			name = '".$this->db->escape($name)."',
			nickname = '".$this->db->escape($name)."',
			status='1',
			ban='0',
			productname='".$this->db->escape($pname)."',
			price_bid=".(int)$price_bid.", date_added=NOW()");


            $link = HTTP_SERVER.'/index.php?route=account/account';
		
		
       		
		$date_added= date("d/m/Y");
		
		$this->language->load('mail/winner');	

		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));		

		$message = $this->language->get('text_welcome'). "<br/>";
		
		$message .= $name . "<br/>";
		
		$message .= $this->language->get('text_sub');
		
		$message .= $date_added . "<br/>";
					
		$message .= $this->language->get('text_auction');
		
		$message .= $pname . "<br/><br/>";
		
		$message .= $this->language->get('text_price');
		
		$message .= $this->currency->format($price_bid) . "<br/><br/>";
		
		$message .= $this->language->get('text_date');
		
		$message .= $date_added . "<br/><br/>";
		
		$message .= $this->language->get('text_message') . "<br/><br/>";
		
				
		$message .= $this->language->get('text_thanks') . "<br/>";

		$message .= $this->config->get('config_name');

		

		$mail = new Mail();

		$mail->protocol = $this->config->get('config_mail_protocol');

		$mail->parameter = $this->config->get('config_mail_parameter');

		$mail->hostname = $this->config->get('config_smtp_host');

		$mail->username = $this->config->get('config_smtp_username');

		$mail->password = $this->config->get('config_smtp_password');

		$mail->port = $this->config->get('config_smtp_port');

		$mail->timeout = $this->config->get('config_smtp_timeout');				

		$mail->setTo($email);

		$mail->setFrom($this->config->get('config_email'));

		$mail->setSender($this->config->get('config_name'));

		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		
		$mail->setHtml($message);

		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));

		$mail->send();			
	}
	
		$this->db->query("UPDATE " . DB_PREFIX . "product_bid 
		SET bid_close_status ='" . (int)($status) . "'
		WHERE product_bid_id = '" . (int)$auction_id . "'");
		
		
		
		
		
		
		
		return 1;
		
	}
	
	
	public function updateautobid($id,$status) {
	
	   if($status==1){
	   $c=0;
	   }else{
	   
	   $c=1;
	   }
	
		$this->db->query("UPDATE " . DB_PREFIX . "autobid 
		SET status ='" . (int)($c) . "'
		WHERE auto_id = '" . (int)$id . "'");
		
		return 1;
		
	}
	
	
	
	public function editAuctionlist($data) {
		
		
		/**auction code **/	
		
		if (!isset($data['use_end_time_on'])) {
		$data['use_end_time_on'] = 0;
	}

	if (!isset($data['use_max_price_on'])) {
		$data['use_max_price_on'] = 0;
	}

	if (!isset($data['status_on'])) {
		$data['status_on'] = 0;
	}
		
		$this->db->query("UPDATE  " . DB_PREFIX . "product_bid SET 			
		bid_date_start = '" . $this->db->escape($data['start_time']) . "',           
		bid_date_end = '" . $this->db->escape($data['end_time']) . "', 					
		taxes = '" . (int)($data['id_tax']) . "',           	
		bid_interval = '" . $this->db->escape($data['bids_interval']) . "', 	
		bid_max_price = '" . (float)($data['max_price']) . "', 			
		bid_min_price = '" . (float)($data['min_offer_step']) . "', 		
		bid_start_price = '" . (float)($data['start_price']) . "', 			
		bid_auto_increment = '" . $this->db->escape($data['autobid_step']) . "',
		end_time_status = '" . (int)($data['use_end_time_on']) . "',
		max_price_status = '" . (int)($data['use_max_price_on']) . "', 
		auction_status = '" . (int)($data['status_on']) . "'	
		WHERE product_bid_id = '" . (int)($data['auction_id']). "'");					
		
		
		$product_bid_id = $data['auction_id'];
		/**code end here**/
	
		return 1;
	}
	
	public function copyProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		if ($query->num_rows) {
			$data = array();
			
			$data = $query->row;
			
			$data['keyword'] = '';

			$data['status'] = '0';
						
			$data = array_merge($data, array('product_attribute' => $this->getProductAttributes($product_id)));
			$data = array_merge($data, array('product_description' => $this->getProductDescriptions($product_id)));			
			$data = array_merge($data, array('product_discount' => $this->getProductDiscounts($product_id)));
			$data = array_merge($data, array('product_image' => $this->getProductImages($product_id)));		
			$data = array_merge($data, array('product_option' => $this->getProductOptions($product_id)));
			$data = array_merge($data, array('product_related' => $this->getProductRelated($product_id)));
			$data = array_merge($data, array('product_reward' => $this->getProductRewards($product_id)));
			$data = array_merge($data, array('product_special' => $this->getProductSpecials($product_id)));
			$data = array_merge($data, array('product_tag' => $this->getProductTags($product_id)));
			$data = array_merge($data, array('product_category' => $this->getProductCategories($product_id)));
			$data = array_merge($data, array('product_download' => $this->getProductDownloads($product_id)));
			$data = array_merge($data, array('product_layout' => $this->getProductLayouts($product_id)));
			$data = array_merge($data, array('product_store' => $this->getProductStores($product_id)));
			
			$this->addProduct($data);
		}
	}
	
	public function deleteProduct($product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_tag WHERE product_id='" . (int)$product_id. "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id. "'");
		
		$this->cache->delete('product');
	}
	
	
	
	public function deleteAuctionlist($product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_bid WHERE product_id = '" . (int)$product_id . "'");
		$this->cache->delete('product');
	}
	
	
	public function deletecustomerbid($product_id) {
	    
       $this->db->query("DELETE FROM " . DB_PREFIX . "customer_bids WHERE product_id = '" . (int)$product_id . "'");
	   $this->db->query("DELETE FROM " . DB_PREFIX . "autobid WHERE product_id = '" . (int)$product_id . "'");
	   $this->db->query("DELETE FROM " . DB_PREFIX . "bids_history WHERE product_id = '" . (int)$product_id . "'");
		return 1;
	}
	
	
	public function deletebid($bid) {
	    
       $this->db->query("DELETE FROM " . DB_PREFIX . "customer_bids WHERE customer_bid_id = '" . (int)$bid . "'");
		return 1;
	}
	
	
	public function deleteautobid($bid) {
	    
       $this->db->query("DELETE FROM " . DB_PREFIX . "autobid WHERE auto_id = '" . (int)$bid . "'");
		return 1;
	}
	
	
	
	
	
	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
				
		return $query->row;
	}
	
	
public function getproductauction($product_id) {		
$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p 		
LEFT JOIN " . DB_PREFIX . "product_bid pb ON (p.product_id = pb.product_id)	
LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)	
WHERE pb.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");						
return $query->row;	
}


public function getid($product_id) {		
$query = $this->db->query("SELECT product_bid_id FROM " . DB_PREFIX . "product_bid 
WHERE product_id = '" . (int)$product_id . "'");						
return $query->row['product_bid_id'];	
}

	
	public function getProducts($data = array()) {
		if ($data) {
			$sql = "SELECT pb.*,pd.name,.p.image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_bid pb ON (p.product_id = pb.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
			
			if (!empty($data['filter_category_id'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
			}
					
			$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'"; 
			
			if (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			}

			if (!empty($data['filter_model'])) {
				$sql .= " AND LCASE(p.model) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
			}
			
			if (!empty($data['filter_price'])) {
				$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
			}
			
			if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
				$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
			}
			
			if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
				$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
			}
					
			if (!empty($data['filter_category_id'])) {
				if (!empty($data['filter_sub_category'])) {
					$implode_data = array();
					
					$implode_data[] = "category_id = '" . (int)$data['filter_category_id'] . "'";
					
					$this->load->model('catalog/category');
					
					$categories = $this->model_catalog_category->getCategories($data['filter_category_id']);
					
					foreach ($categories as $category) {
						$implode_data[] = "p2c.category_id = '" . (int)$category['category_id'] . "'";
					}
					
					$sql .= " AND (" . implode(' OR ', $implode_data) . ")";			
				} else {
					$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
				}
			}
			$sql .= " AND p.product_id = pb.product_id";
			$sql .= " GROUP BY p.product_id";
						
			$sort_data = array(
				'pd.name',
				'p.model',
				'p.price',
				'p.quantity',
				'p.status',
				'p.sort_order'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY pd.name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
		
			return $query->rows;
		} else {
			$product_data = $this->cache->get('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
		
			if (!$product_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY pd.name ASC");
	
				$product_data = $query->rows;
			
				$this->cache->set('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $product_data);
			}	
	
			return $product_data;
		}
	}
	
	
	public function getbids($data = array(),$id) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "customer_bids cb WHERE cb.product_id = '" . (int)$id . "'";
			
			if (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			}
			
			if (!empty($data['filter_nickname'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_nickname'])) . "%'";
			}

			
			
			if (!empty($data['filter_price'])) {
			$sql .= " AND cb.price_bid LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND cb.date_added LIKE '" . $this->db->escape($data['filter_date']) . "%'";
		}
		
		
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND cb.status = '" . (int)$data['filter_status'] . "'";
		}
			
		
						
			$sort_data = array(
				'cb.name',
				'cb.date_added',
				'cb.price_bid',
				'cb.status'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY cb.date_added";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
	
			$query = $this->db->query($sql);
		
			return $query->rows;
		} else {
			
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_bids cb WHERE cb.product_id = '" . (int)$id . "' ORDER BY cb.product_id ASC");

	
			return $product_data;
		}
	}
	
	
	public function getTotalbids($data = array(),$id) {
		
		
		$sql = "SELECT COUNT(DISTINCT cb.customer_bid_id) AS total FROM " . DB_PREFIX . "customer_bids cb";
								
			$sql .= " WHERE cb.product_id = '" . (int)$id . "'"; 

		if (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			}
			
			if (!empty($data['filter_nickname'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_nickname'])) . "%'";
			}

			
			
			if (!empty($data['filter_price'])) {
			$sql .= " AND cb.price_bid LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND cb.date_added LIKE '" . $this->db->escape($data['filter_date']) . "%'";
		}
		
		
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND cb.status = '" . (int)$data['filter_status'] . "'";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
	
	
	
	public function getautobids($data = array(),$id) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "autobid cb WHERE cb.product_id = '" . (int)$id . "'";
			
			if (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			}
			
			if (!empty($data['filter_nickname'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_nickname'])) . "%'";
			}

			
			
			if (!empty($data['filter_price'])) {
			$sql .= " AND cb.price_bid LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND cb.date_added LIKE '" . $this->db->escape($data['filter_date']) . "%'";
		}
		
		
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND cb.status = '" . (int)$data['filter_status'] . "'";
		}
			
		
						
			$sort_data = array(
				'cb.name',
				'cb.date_added',
				'cb.price_bid',
				'cb.status'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY cb.date_added";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
	
			$query = $this->db->query($sql);
		
			return $query->rows;
		} else {
			
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "autobid cb WHERE cb.product_id = '" . (int)$id . "' ORDER BY cb.product_id ASC");

	
			return $product_data;
		}
	}
	
	
	public function getTotalautobids($data = array(),$id) {
		
		
		$sql = "SELECT COUNT(DISTINCT cb.auto_id) AS total FROM " . DB_PREFIX . "autobid cb";
								
			$sql .= " WHERE cb.product_id = '" . (int)$id . "'"; 

		if (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			}
			
			if (!empty($data['filter_nickname'])) {
				$sql .= " AND LCASE(cb.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_nickname'])) . "%'";
			}

			
			
			if (!empty($data['filter_price'])) {
			$sql .= " AND cb.price_bid LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND cb.date_added LIKE '" . $this->db->escape($data['filter_date']) . "%'";
		}
		
		
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND cb.status = '" . (int)$data['filter_status'] . "'";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
	
	public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");
								  
		return $query->rows;
	} 
	
	public function getProductDescriptions($product_id) {
		$product_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_description' => $result['meta_description']
			);
		}
		
		return $product_description_data;
	}

	public function getProductAttributes($product_id) {
		$product_attribute_data = array();
		
		$product_attribute_query = $this->db->query("SELECT pa.attribute_id, ad.name FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.product_id = '" . (int)$product_id . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY pa.attribute_id");
		
		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();
			
			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
			
			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
			}
			
			$product_attribute_data[] = array(
				'attribute_id'                  => $product_attribute['attribute_id'],
				'name'                          => $product_attribute['name'],
				'product_attribute_description' => $product_attribute_description_data
			);
		}
		
		return $product_attribute_data;
	}
	
	public function getProductOptions($product_id) {
		$product_option_data = array();
		
		$product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");
		
		foreach ($product_option_query->rows as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				$product_option_value_data = array();	
				
				$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order");
				
				foreach ($product_option_value_query->rows as $product_option_value) {
					$product_option_value_data[] = array(
						'product_option_value_id' => $product_option_value['product_option_value_id'],
						'option_value_id'         => $product_option_value['option_value_id'],
						'name'                    => $product_option_value['name'],
						'image'                   => $product_option_value['image'],
						'quantity'                => $product_option_value['quantity'],
						'subtract'                => $product_option_value['subtract'],
						'price'                   => $product_option_value['price'],
						'price_prefix'            => $product_option_value['price_prefix'],
						'points'                  => $product_option_value['points'],
						'points_prefix'           => $product_option_value['points_prefix'],						
						'weight'                  => $product_option_value['weight'],
						'weight_prefix'           => $product_option_value['weight_prefix']					
					);
				}
				
				$product_option_data[] = array(
					'product_option_id'    => $product_option['product_option_id'],
					'option_id'            => $product_option['option_id'],
					'name'                 => $product_option['name'],
					'type'                 => $product_option['type'],
					'product_option_value' => $product_option_value_data,
					'required'             => $product_option['required']
				);				
			} else {
				$product_option_data[] = array(
					'product_option_id' => $product_option['product_option_id'],
					'option_id'         => $product_option['option_id'],
					'name'              => $product_option['name'],
					'type'              => $product_option['type'],
					'option_value'      => $product_option['option_value'],
					'required'          => $product_option['required']
				);				
			}
		}	
		
		return $product_option_data;
	}
	
	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		
		return $query->rows;
	}
	
	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");
		
		return $query->rows;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");
		
		return $query->rows;
	}
	
	public function getProductRewards($product_id) {
		$product_reward_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
		}
		
		return $product_reward_data;
	}
		
	public function getProductDownloads($product_id) {
		$product_download_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_download_data[] = $result['download_id'];
		}
		
		return $product_download_data;
	}

	public function getProductStores($product_id) {
		$product_store_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_store_data[] = $result['store_id'];
		}
		
		return $product_store_data;
	}

	public function getProductLayouts($product_id) {
		$product_layout_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_layout_data[$result['store_id']] = $result['layout_id'];
		}
		
		return $product_layout_data;
	}
		
	public function getProductCategories($product_id) {
		$product_category_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}

	public function getProductRelated($product_id) {
		$product_related_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		
		foreach ($query->rows as $result) {
			$product_related_data[] = $result['related_id'];
		}
		
		return $product_related_data;
	}
	
	public function getProductTags($product_id) {
		$product_tag_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id . "'");
		
		$tag_data = array();
		
		foreach ($query->rows as $result) {
			$tag_data[$result['language_id']][] = $result['tag'];
		}
		
		foreach ($tag_data as $language => $tags) {
			$product_tag_data[$language] = implode(',', $tags);
		}
		
		return $product_tag_data;
	}
	
	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_bid pb ON (p.product_id = pb.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		if (!empty($data['filter_category_id'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}
		 
		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		 			
		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		
		
		if (!empty($data['filter_bid_start_price'])) {
			$sql .= " AND pb.filter_bid_start_price LIKE '" . $this->db->escape($data['filter_bid_start_price']) . "%'";
		}

		if (!empty($data['filter_bid_date_start'])) {
			$sql .= " AND pb.filter_bid_date_start LIKE '" . $this->db->escape($data['filter_bid_date_start']) . "%'";
		}
		
		if (isset($data['filter_bid_date_end'])) {
			$sql .= " AND pb.filter_bid_date_end = '" . $this->db->escape($data['filter_bid_date_end']) . "'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$implode_data = array();
				
				$implode_data[] = "p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
				
				$this->load->model('catalog/category');
				
				$categories = $this->model_catalog_category->getCategories($data['filter_category_id']);
				
				foreach ($categories as $category) {
					$implode_data[] = "p2c.category_id = '" . (int)$category['category_id'] . "'";
				}
				
				$sql .= " AND (" . implode(' OR ', $implode_data) . ")";			
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
			}
		}
		$sql .= " AND p.product_id = pb.product_id";
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
	
	public function getTotalProductsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->row['total'];
	}
		
	public function getTotalProductsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByLengthClassId($length_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalProductsByManufacturerId($manufacturer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByAttributeId($attribute_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByOptionId($option_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}			
}
?>