<?php
class ModelCatalogBlackList extends Model {
	
	
	public function deleteipblock($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "block_ipaddress WHERE ban_id = '" . (int)$id . "'");
		
	}
	
	public function addipblock($data=array()) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "block_ipaddress 
		SET 
		ip_address = '" . $this->db->escape($data['ip']) . "',
		status = '" . (int)$data['status'] . "',
		date_added = NOW()");
		
	}
	
	
	public function editips($id,$data=array()) {
		$this->db->query("UPDATE  " . DB_PREFIX . "block_ipaddress 
		SET 
		ip_address = '" . $this->db->escape($data['ip']) . "',
		status = '" . (int)$data['status'] . "',
		date_added = NOW() WHERE ban_id = '" . (int)$id . "'");
		
	}
	
	public function getipblock($id) {		
	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "block_ipaddress b 
	LEFT JOIN " . DB_PREFIX . "customer c ON (c.ip = b.ip_address)
	WHERE b.ban_id = '" . (int)$id . "'");					

	return $query->row;	
	
	}
	
	
	public function getcustomerbids($id) {		
	$query = $this->db->query("SELECT COUNT(DISTINCT product_id) AS total FROM " . DB_PREFIX . "customer_bids WHERE customer_id = '" . (int)$id . "'");					

	  if($query->row['total']){
	return $query->row['total'];
}else{
return 0;

}	
	
	}
	
	
	
	
	public function getips($data = array()) {
		if ($data) {
			$sql = "SELECT b.*,c.firstname,c.lastname,c.ip,c.email,c.customer_id FROM " . DB_PREFIX . "block_ipaddress b
			LEFT JOIN " . DB_PREFIX . "customer c ON (c.ip = b.ip_address)";			 
			$sql .= " WHERE b.accept = '1' AND c.ip = b.ip_address";
						
			if (!empty($data['filter_ip'])) {
				$sql .= " AND b.ip_address LIKE '" . $this->db->escape(utf8_strtolower($data['filter_ip'])) . "%'";
			}
			
			if (!empty($data['filter_firstname'])) {
				$sql .= " AND LCASE(c.firstname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_firstname'])) . "%'";
			}
			
			if (!empty($data['filter_lastname'])) {
				$sql .= " AND LCASE(c.lastname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_lastname'])) . "%'";
			}
			
			if (!empty($data['filter_email'])) {
				$sql .= " AND LCASE(c.email) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
			}
			
			if (isset($data['filter_date'])) {
				$sql .= " AND b.date_added = '" . (int)$data['filter_date'] . "'";
			}
			
			
			
			
						
			$sort_data = array(
				'b.ip_address',
				'b.date_added',
				'c.firstname',
				'c.lastname',
				'c.email'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY b.ban_id";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

		
			
			$query = $this->db->query($sql);
		
			return $query->rows;
		} else {
			
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "block_ipaddress b 
				LEFT JOIN " . DB_PREFIX . "customer c ON (c.ip = b.ip_address) WHERE b.accept = '1' AND c.ip = b.ip_address ORDER BY b.ban_id ASC");
	
				$product_data = $query->rows;
			
	
			return $product_data;
		}
	}
	
	
	
	
	public function getTotalips($data = array()) {
		$sql = "SELECT COUNT(DISTINCT b.ban_id) AS total FROM " . DB_PREFIX . "block_ipaddress b
		LEFT JOIN " . DB_PREFIX . "customer c ON (c.ip = b.ip_address)";
		
		$sql .= " WHERE b.accept = '1' AND c.ip = b.ip_address"; 
			
		if (!empty($data['filter_ip'])) {
				$sql .= " AND b.ip_address LIKE '" . $this->db->escape(utf8_strtolower($data['filter_ip'])) . "%'";
			}
			
			if (!empty($data['filter_firstname'])) {
				$sql .= " AND LCASE(c.firstname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_firstname'])) . "%'";
			}
			
			if (!empty($data['filter_lastname'])) {
				$sql .= " AND LCASE(c.lastname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_lastname'])) . "%'";
			}
			
			if (!empty($data['filter_email'])) {
				$sql .= " AND LCASE(c.email) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
			}
			
			if (isset($data['filter_date'])) {
				$sql .= " AND b.date_added = '" . (int)$data['filter_date'] . "'";
			}
			
			
			
			
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
	
	public function getTotalProductsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->row['total'];
	}
		
	public function getTotalProductsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByLengthClassId($length_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalProductsByManufacturerId($manufacturer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByAttributeId($attribute_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByOptionId($option_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}			
}
?>