<?php
class ModelCatalogAuctionpayment extends Model {
public function deleteauctionpayment($order_id) {
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_status_id > '0' AND order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

			foreach($product_query->rows as $product) {
				$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

				$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

				foreach ($option_query->rows as $option) {
					$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
				}
			}
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_fraud WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int)$order_id . "'");
	}

	
	public function getauctionpayments($data = array(),$id) {
	
		$sql = "SELECT (w.name) AS customer,w.*, 
		os.name AS status FROM `" . DB_PREFIX . "winner` w 
		LEFT JOIN " . DB_PREFIX . "order_status os ON w.status = os.order_status_id 
	    WHERE w.product_id = '" . (int)$id. "'";
		
		
		
		if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
			$sql .= " AND w.status = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " AND w.status > '0'";
		}

		

		if (!empty($data['filter_customer'])) {
			$sql .= " AND LCASE(w.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'";
		}
		
		
		if (!empty($data['filter_pname'])) {
			$sql .= " AND LCASE(w.product_name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_pname'])) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(w.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND w.price_bid = '" . (float)$data['filter_total'] . "'";
		}

		$sort_data = array(
			'customer',
			'status',
			'w.date_added',
			'w.productname',
			'w.price_bid'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " order BY " . $data['sort'];
		} else {
			$sql .= " order BY w.winner_id";
		}

		if (isset($data['auctionpayment']) && ($data['auctionpayment'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	

	public function getTotalauctionpayments($data = array(),$id) {
      	
		
		$sql = "SELECT COUNT(w.winner_id) AS total FROM `" . DB_PREFIX . "winner` w 
	    WHERE w.product_id = '" . (int)$id. "'";
		
		
		
		if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
			$sql .= " AND w.status = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " AND w.status > '0'";
		}

		

		if (!empty($data['filter_customer'])) {
			$sql .= " AND LCASE(w.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'";
		}
		
		
		if (!empty($data['filter_pname'])) {
			$sql .= " AND LCASE(w.product_name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_pname'])) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(w.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND w.price_bid = '" . (float)$data['filter_total'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

		
}
?>