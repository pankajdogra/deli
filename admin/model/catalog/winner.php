<?php
class ModelCatalogWinner extends Model {
	
	
	public function deletewinner($winner_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "winner WHERE winner_id = '" . (int)$winner_id . "'");
		$this->cache->delete('product');
	}
	
	
	public function getipblock($id) {		
	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "block_ipaddress b 
	LEFT JOIN " . DB_PREFIX . "customer c ON (c.ip = b.ip_address)
	WHERE c.customer_id = '" . (int)$id . "'");					

	return $query->row;	
	
	}
	
	
	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
				
		return $query->row;
	}
	
	public function getProducts($data = array()) {
		if ($data) {
			$sql = "SELECT w.*,os.name AS status FROM " . DB_PREFIX . "winner w
        LEFT JOIN " . DB_PREFIX . "order_status os ON w.status = os.order_status_id";
			 
			
			if (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(w.productname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			}
			
			if (!empty($data['filter_names'])) {
				$sql .= " AND LCASE(w.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_names'])) . "%'";
			}
			
			if (!empty($data['filter_nickname'])) {
				$sql .= " AND LCASE(w.nickname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_nickname'])) . "%'";
			}

			
if (!empty($data['filter_price'])) {
				$sql .= " AND w.price_bid = '" . $this->db->escape($data['filter_price']) . "'";
			}
			
			if (!empty($data['filter_status'])) {
				$sql .= " AND w.status = '" . $this->db->escape($data['filter_status']) . "'";
			}
			
			
			
			if (isset($data['filter_bid_date_end'])) {
				$sql .= " AND w.date_added = '" . (int)$data['filter_bid_date_end'] . "'";
			}
			
			
			$sql .= " GROUP BY w.winner_id";
						
			$sort_data = array(
				'w.productname',
				'w.name',
				'w.nickname',
				'w.price_bid',
				'w.date_added',
				'w.status'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY w.name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
		
			return $query->rows;
		} else {
			
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "winner w  ORDER BY w.name ASC");
	
				$product_data = $query->rows;
			
	
			return $product_data;
		}
	}
	
	
	
	
	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT w.winner_id) AS total FROM " . DB_PREFIX . "winner w";
			
		if (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(w.productname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			}
			
			if (!empty($data['filter_names'])) {
				$sql .= " AND LCASE(w.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_names'])) . "%'";
			}
			
			if (!empty($data['filter_nickname'])) {
				$sql .= " AND LCASE(w.nickname) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_nickname'])) . "%'";
			}

			
			
			if (!empty($data['filter_price'])) {
				$sql .= " AND w.price_bid = '" . $this->db->escape($data['filter_price']) . "'";
			}
			
			if (!empty($data['filter_status'])) {
				$sql .= " AND w.status = '" . $this->db->escape($data['filter_status']) . "'";
			}
			
			
			
			if (isset($data['filter_bid_date_end'])) {
				$sql .= " AND w.date_added = '" . (int)$data['filter_bid_date_end'] . "'";
			}
			
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
	
	public function getTotalProductsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->row['total'];
	}
		
	public function getTotalProductsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByLengthClassId($length_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalProductsByManufacturerId($manufacturer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row['total'];
	}
	
	public function getTotalProductsByAttributeId($attribute_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByOptionId($option_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

		return $query->row['total'];
	}	
	
	public function getTotalProductsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}			
}
?>