<?php

class ModelSaleCftyp extends Model {

	public function getAffiliates($filter=false) {
		$q = "select affiliate_id as id, concat(firstname, ' ', lastname, '(', email , ')') as n from " . DB_PREFIX . "affiliate where `status`='1' and approved='1'";		
		if($filter) {
			$filter = $this->db->escape(strtolower($filter));
			$q .= " and (LOWER(firstname) like '$filter%' or LOWER(lastname) like '$filter%' or LOWER(email) like '$filter%' or LOWER(email) like '%@$filter%') ";
		}
		$q .= " order by concat(firstname, ' ', lastname)";
		$res = $this->db->query($q);
		$out = array();
		foreach($res->rows as $r) {
			$out[strval($r['id'])] = $r['n'];
		}
		return $out;	
	}
	
	public function getCouponAffiliateId($coupon_id) {
		$res = $this->db->query("select affiliate_id from " . DB_PREFIX . "cftyp_coupon_affiliate where coupon_id='" . (int)$coupon_id . "'");
		if($res->num_rows < 1) return '0';
		return strval($res->row['affiliate_id']);		
	}
	
	public function edit($coupon_id, $affiliate_id = false) {
		if(!preg_match("/^\d+$/", $coupon_id)) return;
		if(!$affiliate_id) {
			$this->db->query("delete from " . DB_PREFIX . "cftyp_coupon_affiliate where coupon_id='" . (int)$coupon_id . "'");
			return;
		}
		if(!preg_match("/^\d+$/", $affiliate_id)) return;
		$this->db->query("insert into " . DB_PREFIX . "cftyp_coupon_affiliate (coupon_id, affiliate_id) values ('" . (int)$coupon_id . "', '" . (int)$affiliate_id . "') on duplicate key update affiliate_id='" . (int)$affiliate_id . "'");
	}
	

}
