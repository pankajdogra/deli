<?php
	class ModelDesignbeverlysion extends Model {
	public function addBanner_beverlysion($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "banner SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "'");
	
		$banner_id = $this->db->getLastId();
	
		if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $banner_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "banner_image SET banner_id = '" . (int)$banner_id . "', link = '" .  $this->db->escape($banner_image['link']) . "', image = '" .  $this->db->escape($banner_image['image']) . "'");
				
				$banner_image_id = $this->db->getLastId();
				
				foreach ($banner_image['banner_image_description'] as $language_id => $banner_image_description) {				
					$this->db->query("INSERT INTO " . DB_PREFIX . "banner_image_description SET banner_image_id = '" . (int)$banner_image_id . "', language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', title = '" .  $this->db->escape($banner_image_description['title']) . "'");
				}
			}
		}		
	}
	
	public function editBanner_beverlysion($banner_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "banner SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "', beverlysion = '" . (int)$data['beverlysion_sts'] . "' WHERE banner_id = '" . (int)$banner_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image_description WHERE banner_id = '" . (int)$banner_id . "'");
			
		if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $banner_image) {
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "banner_image SET banner_id = '" . (int)$banner_id . "', link = '" .  $this->db->escape($banner_image['link']) . "', image = '" .  $this->db->escape($banner_image['image']) . "', sm_pic_1 = '" .  $this->db->escape($banner_image['sm_pic_1']) . "', sm_pic_1_data_x = '" .  $this->db->escape($banner_image['sm_pic_1_data_x']) . "', sm_pic_1_data_y = '" .  $this->db->escape($banner_image['sm_pic_1_data_y']) . "', sm_pic_1_data_sp = '" .  $this->db->escape($banner_image['sm_pic_1_data_sp']) . "', sm_pic_1_data_st = '" .  $this->db->escape($banner_image['sm_pic_1_data_st']) . "', sm_pic_1_data_ea = '" .  $this->db->escape($banner_image['sm_pic_1_data_ea']) . "', sm_pic_1_data_cls = '" .  $this->db->escape($banner_image['sm_pic_1_data_cls']) . "', sm_pic_1_url = '" .  $this->db->escape($banner_image['sm_pic_1_url']) . "', sm_pic_2 = '" .  $this->db->escape($banner_image['sm_pic_2']) . "', sm_pic_2_data_x = '" .  $this->db->escape($banner_image['sm_pic_2_data_x']) . "', sm_pic_2_data_y = '" .  $this->db->escape($banner_image['sm_pic_2_data_y']) . "', sm_pic_2_data_sp = '" .  $this->db->escape($banner_image['sm_pic_2_data_sp']) . "', sm_pic_2_data_st = '" .  $this->db->escape($banner_image['sm_pic_2_data_st']) . "', sm_pic_2_data_ea = '" .  $this->db->escape($banner_image['sm_pic_2_data_ea']) . "', sm_pic_2_data_cls = '" .  $this->db->escape($banner_image['sm_pic_2_data_cls']) . "', sm_pic_2_url = '" .  $this->db->escape($banner_image['sm_pic_2_url']) . "', sm_pic_3 = '" .  $this->db->escape($banner_image['sm_pic_3']) . "', sm_pic_3_data_x = '" .  $this->db->escape($banner_image['sm_pic_3_data_x']) . "', sm_pic_3_data_y = '" .  $this->db->escape($banner_image['sm_pic_3_data_y']) . "', sm_pic_3_data_sp = '" .  $this->db->escape($banner_image['sm_pic_3_data_sp']) . "', sm_pic_3_data_st = '" .  $this->db->escape($banner_image['sm_pic_3_data_st']) . "', sm_pic_3_data_ea = '" .  $this->db->escape($banner_image['sm_pic_3_data_ea']) . "', sm_pic_3_data_cls = '" .  $this->db->escape($banner_image['sm_pic_3_data_cls']) . "', sm_pic_4 = '" .  $this->db->escape($banner_image['sm_pic_4']) . "', sm_pic_4_data_x = '" .  $this->db->escape($banner_image['sm_pic_4_data_x']) . "', sm_pic_4_data_y = '" .  $this->db->escape($banner_image['sm_pic_4_data_y']) . "', sm_pic_4_data_sp = '" .  $this->db->escape($banner_image['sm_pic_4_data_sp']) . "', sm_pic_4_data_st = '" .  $this->db->escape($banner_image['sm_pic_4_data_st']) . "', sm_pic_4_data_ea = '" .  $this->db->escape($banner_image['sm_pic_4_data_ea']) . "', sm_pic_4_data_cls = '" .  $this->db->escape($banner_image['sm_pic_4_data_cls']) . "', sm_pic_5 = '" .  $this->db->escape($banner_image['sm_pic_5']) . "', sm_pic_5_data_x = '" .  $this->db->escape($banner_image['sm_pic_5_data_x']) . "', sm_pic_5_data_y = '" .  $this->db->escape($banner_image['sm_pic_5_data_y']) . "', sm_pic_5_data_sp = '" .  $this->db->escape($banner_image['sm_pic_5_data_sp']) . "', sm_pic_5_data_st = '" .  $this->db->escape($banner_image['sm_pic_5_data_st']) . "', sm_pic_5_data_ea = '" .  $this->db->escape($banner_image['sm_pic_5_data_ea']) . "', sm_pic_5_data_cls = '" .  $this->db->escape($banner_image['sm_pic_5_data_cls']) . "', caption_1 = '" .  $this->db->escape($banner_image['caption_1']) . "', caption_1_data_x = '" .  $this->db->escape($banner_image['caption_1_data_x']) . "', caption_1_data_y = '" .  $this->db->escape($banner_image['caption_1_data_y']) . "', caption_1_data_sp = '" .  $this->db->escape($banner_image['caption_1_data_sp']) . "', caption_1_data_st = '" .  $this->db->escape($banner_image['caption_1_data_st']) . "', caption_1_data_ea = '" .  $this->db->escape($banner_image['caption_1_data_ea']) . "', caption_1_data_cls = '" .  $this->db->escape($banner_image['caption_1_data_cls']) . "', caption_1_url = '" .  $this->db->escape($banner_image['caption_1_url']) . "', caption_2 = '" .  $this->db->escape($banner_image['caption_2']) . "', caption_2_data_x = '" .  $this->db->escape($banner_image['caption_2_data_x']) . "', caption_2_data_y = '" .  $this->db->escape($banner_image['caption_2_data_y']) . "', caption_2_data_sp = '" .  $this->db->escape($banner_image['caption_2_data_sp']) . "', caption_2_data_st = '" .  $this->db->escape($banner_image['caption_2_data_st']) . "', caption_2_data_ea = '" .  $this->db->escape($banner_image['caption_2_data_ea']) . "', caption_2_data_cls = '" .  $this->db->escape($banner_image['caption_2_data_cls']) . "', caption_2_url = '" .  $this->db->escape($banner_image['caption_2_url']) . "', caption_3 = '" .  $this->db->escape($banner_image['caption_3']) . "', caption_3_data_x = '" .  $this->db->escape($banner_image['caption_3_data_x']) . "', caption_3_data_y = '" .  $this->db->escape($banner_image['caption_3_data_y']) . "', caption_3_data_sp = '" .  $this->db->escape($banner_image['caption_3_data_sp']) . "', caption_3_data_st = '" .  $this->db->escape($banner_image['caption_3_data_st']) . "', caption_3_data_ea = '" .  $this->db->escape($banner_image['caption_3_data_ea']) . "', caption_3_data_cls = '" .  $this->db->escape($banner_image['caption_3_data_cls']) . "', caption_4 = '" .  $this->db->escape($banner_image['caption_4']) . "', caption_4_data_x = '" .  $this->db->escape($banner_image['caption_4_data_x']) . "', caption_4_data_y = '" .  $this->db->escape($banner_image['caption_4_data_y']) . "', caption_4_data_sp = '" .  $this->db->escape($banner_image['caption_4_data_sp']) . "', caption_4_data_st = '" .  $this->db->escape($banner_image['caption_4_data_st']) . "', caption_4_data_ea = '" .  $this->db->escape($banner_image['caption_4_data_ea']) . "', caption_4_data_cls = '" .  $this->db->escape($banner_image['caption_4_data_cls']) . "', caption_5 = '" .  $this->db->escape($banner_image['caption_5']) . "', caption_5_data_x = '" .  $this->db->escape($banner_image['caption_5_data_x']) . "', caption_5_data_y = '" .  $this->db->escape($banner_image['caption_5_data_y']) . "', caption_5_data_sp = '" .  $this->db->escape($banner_image['caption_5_data_sp']) . "', caption_5_data_st = '" .  $this->db->escape($banner_image['caption_5_data_st']) . "', caption_5_data_ea = '" .  $this->db->escape($banner_image['caption_5_data_ea']) . "', caption_5_data_cls = '" .  $this->db->escape($banner_image['caption_5_data_cls']) . "', button_1 = '" .  $this->db->escape($banner_image['button_1']) . "', button_1_data_x = '" .  $this->db->escape($banner_image['button_1_data_x']) . "', button_1_data_y = '" .  $this->db->escape($banner_image['button_1_data_y']) . "', button_1_data_sp = '" .  $this->db->escape($banner_image['button_1_data_sp']) . "', button_1_data_st = '" .  $this->db->escape($banner_image['button_1_data_st']) . "', button_1_data_ea = '" .  $this->db->escape($banner_image['button_1_data_ea']) . "', button_1_data_cls = '" .  $this->db->escape($banner_image['button_1_data_cls']) . "', button_1_url = '" .  $this->db->escape($banner_image['button_1_url']) . "', youtube_iframe = '" .  $this->db->escape($banner_image['youtube_iframe']) . "', youtube_autoplay = '" .  $this->db->escape($banner_image['youtube_autoplay']) . "', youtube_data_x = '" .  $this->db->escape($banner_image['youtube_data_x']) . "', youtube_data_y = '" .  $this->db->escape($banner_image['youtube_data_y']) . "', youtube_data_sp = '" .  $this->db->escape($banner_image['youtube_data_sp']) . "', youtube_data_st = '" .  $this->db->escape($banner_image['youtube_data_st']) . "', youtube_data_ea = '" .  $this->db->escape($banner_image['youtube_data_ea']) . "', youtube_data_cls = '" .  $this->db->escape($banner_image['youtube_data_cls']) . "', vimeo_iframe = '" .  $this->db->escape($banner_image['vimeo_iframe']) . "', vimeo_autoplay = '" .  $this->db->escape($banner_image['vimeo_autoplay']) . "', vimeo_data_x = '" .  $this->db->escape($banner_image['vimeo_data_x']) . "', vimeo_data_y = '" .  $this->db->escape($banner_image['vimeo_data_y']) . "', vimeo_data_sp = '" .  $this->db->escape($banner_image['vimeo_data_sp']) . "', vimeo_data_st = '" .  $this->db->escape($banner_image['vimeo_data_st']) . "', vimeo_data_ea = '" .  $this->db->escape($banner_image['vimeo_data_ea']) . "', vimeo_data_cls = '" .  $this->db->escape($banner_image['vimeo_data_cls']) . "', transicion = '" .  $this->db->escape($banner_image['transicion']) . "', slot_amt = '" .  $this->db->escape($banner_image['slot_amt']) . "', data_delay = '" .  $this->db->escape($banner_image['data_delay']) . "', master_speed = '" .  $this->db->escape($banner_image['master_speed']) . "', sm_pic_1_data_h = '" .  $this->db->escape($banner_image['sm_pic_1_data_h']) . "', sm_pic_1_data_w = '" .  $this->db->escape($banner_image['sm_pic_1_data_w']) . "',  sm_pic_2_data_h = '" .  $this->db->escape($banner_image['sm_pic_2_data_h']) . "', sm_pic_2_data_w = '" .  $this->db->escape($banner_image['sm_pic_2_data_w']) . "', sm_pic_3_data_h = '" .  $this->db->escape($banner_image['sm_pic_3_data_h']) . "', sm_pic_3_data_w = '" .  $this->db->escape($banner_image['sm_pic_3_data_w']) . "', sm_pic_4_data_h = '" .  $this->db->escape($banner_image['sm_pic_4_data_h']) . "', sm_pic_4_data_w = '" .  $this->db->escape($banner_image['sm_pic_4_data_w']) . "', sm_pic_5_data_h = '" .  $this->db->escape($banner_image['sm_pic_5_data_h']) . "', sm_pic_5_data_w = '" .  $this->db->escape($banner_image['sm_pic_5_data_w']) . "', youtube_data_h = '" .  $this->db->escape($banner_image['youtube_data_h']) . "', youtube_data_w = '" .  $this->db->escape($banner_image['youtube_data_w']) . "', vimeo_data_h = '" .  $this->db->escape($banner_image['vimeo_data_h']) . "', vimeo_data_w = '" .  $this->db->escape($banner_image['vimeo_data_w']) . "', beverlysion_act = '" .  $this->db->escape($banner_image['beverlysion_act']) . "'");
				
				$banner_image_id = $this->db->getLastId();
				
				foreach ($banner_image['banner_image_description'] as $language_id => $banner_image_description) {				
					$this->db->query("INSERT INTO " . DB_PREFIX . "banner_image_description SET banner_image_id = '" . (int)$banner_image_id . "', language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', title = '" .  $this->db->escape($banner_image_description['title']) . "'");
				}
			}
		}			
	}
	
	public function deleteBanner_beverlysion($banner_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image_description WHERE banner_id = '" . (int)$banner_id . "'");
	}
	
	public function getBanner_beverlysion($banner_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "banner WHERE banner_id = '" . (int)$banner_id . "'");
		
		return $query->row;
	}
		
	public function getBanners_beverlysion($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "banner";
		
		$sort_data = array(
			'name',
			'status'
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
		
	public function getBannerImages_beverlysion($banner_id) {
		$banner_image_data = array();
		
		$banner_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "' ORDER BY image ASC");
		
		foreach ($banner_image_query->rows as $banner_image) {
			$banner_image_description_data = array();
			 
			$banner_image_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_image_description WHERE banner_image_id = '" . (int)$banner_image['banner_image_id'] . "' AND banner_id = '" . (int)$banner_id . "'");
			
			foreach ($banner_image_description_query->rows as $banner_image_description) {			
				$banner_image_description_data[$banner_image_description['language_id']] = array('title' => $banner_image_description['title']);
			}
		
			$banner_image_data[] = array(
				'banner_image_description' => $banner_image_description_data,
				'link'                     => $banner_image['link'],
				'image'                    => $banner_image['image'],
				'beverlysion_act'          => $banner_image['beverlysion_act'],
				'sm_pic_1'                => $banner_image['sm_pic_1'],
				'sm_pic_1_data_x'         => $banner_image['sm_pic_1_data_x'],
				'sm_pic_1_data_y'         => $banner_image['sm_pic_1_data_y'],
				'sm_pic_1_data_sp'        => $banner_image['sm_pic_1_data_sp'],
				'sm_pic_1_data_st'        => $banner_image['sm_pic_1_data_st'],
				'sm_pic_1_data_ea'        => $banner_image['sm_pic_1_data_ea'],
				'sm_pic_1_data_cls'       => $banner_image['sm_pic_1_data_cls'],
				'sm_pic_1_url'            => $banner_image['sm_pic_1_url'],
				'sm_pic_2'                => $banner_image['sm_pic_2'],
				'sm_pic_2_data_x'         => $banner_image['sm_pic_2_data_x'],
				'sm_pic_2_data_y'         => $banner_image['sm_pic_2_data_y'],
				'sm_pic_2_data_sp'        => $banner_image['sm_pic_2_data_sp'],
				'sm_pic_2_data_st'        => $banner_image['sm_pic_2_data_st'],
				'sm_pic_2_data_ea'        => $banner_image['sm_pic_2_data_ea'],
				'sm_pic_2_data_cls'       => $banner_image['sm_pic_2_data_cls'],
				'sm_pic_2_url'            => $banner_image['sm_pic_2_url'],
				'sm_pic_3'                => $banner_image['sm_pic_3'],
				'sm_pic_3_data_x'         => $banner_image['sm_pic_3_data_x'],
				'sm_pic_3_data_y'         => $banner_image['sm_pic_3_data_y'],
				'sm_pic_3_data_sp'        => $banner_image['sm_pic_3_data_sp'],
				'sm_pic_3_data_st'        => $banner_image['sm_pic_3_data_st'],
				'sm_pic_3_data_ea'        => $banner_image['sm_pic_3_data_ea'],
				'sm_pic_3_data_cls'       => $banner_image['sm_pic_3_data_cls'],
				'sm_pic_4'                => $banner_image['sm_pic_4'],
				'sm_pic_4_data_x'         => $banner_image['sm_pic_4_data_x'],
				'sm_pic_4_data_y'         => $banner_image['sm_pic_4_data_y'],
				'sm_pic_4_data_sp'        => $banner_image['sm_pic_4_data_sp'],
				'sm_pic_4_data_st'        => $banner_image['sm_pic_4_data_st'],
				'sm_pic_4_data_ea'        => $banner_image['sm_pic_4_data_ea'],
				'sm_pic_4_data_cls'       => $banner_image['sm_pic_4_data_cls'],
				'sm_pic_5'                => $banner_image['sm_pic_5'],
				'sm_pic_5_data_x'         => $banner_image['sm_pic_5_data_x'],
				'sm_pic_5_data_y'         => $banner_image['sm_pic_5_data_y'],
				'sm_pic_5_data_sp'        => $banner_image['sm_pic_5_data_sp'],
				'sm_pic_5_data_st'        => $banner_image['sm_pic_5_data_st'],
				'sm_pic_5_data_ea'        => $banner_image['sm_pic_5_data_ea'],
				'sm_pic_5_data_cls'       => $banner_image['sm_pic_5_data_cls'],
				'caption_1'                => $banner_image['caption_1'],
				'caption_1_data_x'         => $banner_image['caption_1_data_x'],
				'caption_1_data_y'         => $banner_image['caption_1_data_y'],
				'caption_1_data_sp'        => $banner_image['caption_1_data_sp'],
				'caption_1_data_st'        => $banner_image['caption_1_data_st'],
				'caption_1_data_ea'        => $banner_image['caption_1_data_ea'],
				'caption_1_data_cls'       => $banner_image['caption_1_data_cls'],
				'caption_1_url'            => $banner_image['caption_1_url'],
				'caption_2'                => $banner_image['caption_2'],
				'caption_2_data_x'         => $banner_image['caption_2_data_x'],
				'caption_2_data_y'         => $banner_image['caption_2_data_y'],
				'caption_2_data_sp'        => $banner_image['caption_2_data_sp'],
				'caption_2_data_st'        => $banner_image['caption_2_data_st'],
				'caption_2_data_ea'        => $banner_image['caption_2_data_ea'],
				'caption_2_data_cls'       => $banner_image['caption_2_data_cls'],
				'caption_2_url'            => $banner_image['caption_2_url'],
				'caption_3'                => $banner_image['caption_3'],
				'caption_3_data_x'         => $banner_image['caption_3_data_x'],
				'caption_3_data_y'         => $banner_image['caption_3_data_y'],
				'caption_3_data_sp'        => $banner_image['caption_3_data_sp'],
				'caption_3_data_st'        => $banner_image['caption_3_data_st'],
				'caption_3_data_ea'        => $banner_image['caption_3_data_ea'],
				'caption_3_data_cls'       => $banner_image['caption_3_data_cls'],
				'caption_4'                => $banner_image['caption_4'],
				'caption_4_data_x'         => $banner_image['caption_4_data_x'],
				'caption_4_data_y'         => $banner_image['caption_4_data_y'],
				'caption_4_data_sp'        => $banner_image['caption_4_data_sp'],
				'caption_4_data_st'        => $banner_image['caption_4_data_st'],
				'caption_4_data_ea'        => $banner_image['caption_4_data_ea'],
				'caption_4_data_cls'       => $banner_image['caption_4_data_cls'],
				'caption_5'                => $banner_image['caption_5'],
				'caption_5_data_x'         => $banner_image['caption_5_data_x'],
				'caption_5_data_y'         => $banner_image['caption_5_data_y'],
				'caption_5_data_sp'        => $banner_image['caption_5_data_sp'],
				'caption_5_data_st'        => $banner_image['caption_5_data_st'],
				'caption_5_data_ea'        => $banner_image['caption_5_data_ea'],
				'caption_5_data_cls'       => $banner_image['caption_5_data_cls'],
				'button_1'                => $banner_image['button_1'],
				'button_1_data_x'         => $banner_image['button_1_data_x'],
				'button_1_data_y'         => $banner_image['button_1_data_y'],
				'button_1_data_sp'        => $banner_image['button_1_data_sp'],
				'button_1_data_st'        => $banner_image['button_1_data_st'],
				'button_1_data_ea'        => $banner_image['button_1_data_ea'],
				'button_1_data_cls'       => $banner_image['button_1_data_cls'],
				'button_1_url'       => $banner_image['button_1_url'],
				'youtube_iframe'                => $banner_image['youtube_iframe'],
				'youtube_autoplay'                => $banner_image['youtube_autoplay'],
				'youtube_data_x'         => $banner_image['youtube_data_x'],
				'youtube_data_y'         => $banner_image['youtube_data_y'],
				'youtube_data_sp'        => $banner_image['youtube_data_sp'],
				'youtube_data_st'        => $banner_image['youtube_data_st'],
				'youtube_data_ea'        => $banner_image['youtube_data_ea'],
				'youtube_data_cls'       => $banner_image['youtube_data_cls'],
				'vimeo_iframe'                => $banner_image['vimeo_iframe'],
				'vimeo_autoplay'                => $banner_image['vimeo_autoplay'],
				'vimeo_data_x'         => $banner_image['vimeo_data_x'],
				'vimeo_data_y'         => $banner_image['vimeo_data_y'],
				'vimeo_data_sp'        => $banner_image['vimeo_data_sp'],
				'vimeo_data_st'        => $banner_image['vimeo_data_st'],
				'vimeo_data_ea'        => $banner_image['vimeo_data_ea'],
				'vimeo_data_cls'       => $banner_image['vimeo_data_cls'],
				'transicion'                => $banner_image['transicion'],
				'slot_amt'                => $banner_image['slot_amt'],
				'data_delay'         => $banner_image['data_delay'],
				'master_speed'         => $banner_image['master_speed'],
				'sm_pic_1_data_h'         => $banner_image['sm_pic_1_data_h'],
				'sm_pic_1_data_w'         => $banner_image['sm_pic_1_data_w'],
				'sm_pic_2_data_h'         => $banner_image['sm_pic_2_data_h'],
				'sm_pic_2_data_w'         => $banner_image['sm_pic_2_data_w'],
				'sm_pic_3_data_h'         => $banner_image['sm_pic_3_data_h'],
				'sm_pic_3_data_w'         => $banner_image['sm_pic_3_data_w'],
				'sm_pic_4_data_h'         => $banner_image['sm_pic_4_data_h'],
				'sm_pic_4_data_w'         => $banner_image['sm_pic_4_data_w'],
				'sm_pic_5_data_h'         => $banner_image['sm_pic_5_data_h'],
				'sm_pic_5_data_w'         => $banner_image['sm_pic_5_data_w'],
				'youtube_data_h'         => $banner_image['youtube_data_h'],
				'youtube_data_w'         => $banner_image['youtube_data_w'],
				'vimeo_data_h'         => $banner_image['vimeo_data_h'],
				'vimeo_data_w'         => $banner_image['vimeo_data_w']
			);
		}
		
		return $banner_image_data;
	}
		
	public function getTotalBanners_beverlysion() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "banner");
		
		return $query->row['total'];
	}
	
	public function getbeverlysion_sts() {
		
		$beverlysion_sts = $this->db->query("SELECT banner_id, beverlysion FROM " . DB_PREFIX . "banner WHERE banner_id = '" . (int)$banner_id . "'");
		
		return $beverlysion_sts;
	}
		
}
?>