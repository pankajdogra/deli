<?php

class ControllerTotalTrackingDiscount extends Controller {
	private $error = array();
	private $name = null;

	public function index() {
		if (!$this->name) $this->name = basename(__FILE__, '.php');
		$this->data = array_merge($this->data, $this->load->language('total/' . $this->name));
		$this->data['text_fixed'] = sprintf($this->data['text_fixed'], $this->config->get('config_currency'));

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting($this->name, $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL'));
		}

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_total'),
			'href'      => $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
		'href'      => $this->url->link('total/' . $this->name, 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->document->breadcrumbs[] = array(
       		'text'      => $this->language->get('heading_title'),
       		'href'      => $this->url->link('total/' . $this->name, 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['action'] = $this->url->link('total/' . $this->name, 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL');
		
		foreach(array('amount', 'type', 'fixed_max', 'when', 'period', 'what', 'status', 'sort_order') as $_v) {
			$_v = $this->name . '_' . $_v;
			if (isset($this->request->post[$_v])) {
				$this->data[$_v] = $this->request->post[$_v];
			} else {
				$this->data[$_v] = $this->config->get($_v);
			}		
		}

		$this->load->model('localisation/tax_class');

		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		$this->template = 'total/' . $this->name . '.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->name) $this->name = basename(__FILE__, '.php');
		if (!$this->user->hasPermission('modify', 'total/' . $this->name)) {
			$this->error['warning'] = $this->language->get('error_permission');
		} else {		
			$errors = array(
				'amount' => "/^\d+(?:\.\d+)?$/",
				'fixed_max' => "/^\d+(?:\.\d+)?$/",
				'period' => "/^\d+$/",
				'sort_order' => "/^\d+$/"
			);
			$required = array('amount');
		
			$_errors = array();
			foreach($errors as $k => $preg) {
				if(!in_array($k, $required) && !strlen($this->request->post[$this->name . '_' . $k])) continue;
				if(!preg_match($preg, $this->request->post[$this->name . '_' . $k])) $_errors[] = $this->language->get('error_' . $k);
			}
		
			if(sizeof($_errors) > 0) $this->error['warning'] = implode('<br />', $_errors);
		}		
		return (!$this->error ? true : false);
	}
}

