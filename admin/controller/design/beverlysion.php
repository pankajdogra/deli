<?php 
class ControllerDesignbeverlysion extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('design/beverlysion');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('design/beverlysion');
		
		$this->getList_beverlysion();
	}

	public function insert() {
		$this->language->load('design/beverlysion');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('design/beverlysion');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_design_beverlysion->addBanner_beverlysion($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('design/banner', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm_beverlysion();
	}
	
	public function update() {
		$this->language->load('design/beverlysion');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('design/beverlysion');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_design_beverlysion->editBanner_beverlysion($this->request->get['banner_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('design/banner', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm_beverlysion();
	}
	
 public function update_beverlysion() {
		$this->language->load('design/beverlysion');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('design/beverlysion');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_design_beverlysion->editBanner_beverlysion($this->request->get['banner_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			//$this->redirect($this->url->link('design/banner', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
			$this->model_design_beverlysion->editBanner_beverlysion($this->request->get['banner_id'], $this->request->post);
		}

		$this->getForm_beverlysion();
	}
 
	public function delete_beverlysion() {
		$this->language->load('design/beverlysion');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('design/beverlysion');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $banner_id) {
				$this->model_design_beverlysion->deleteBanner_beverlysion($banner_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('design/banner', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList_beverlysion();
	}

	protected function getList_beverlysion() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('design/beverlysion', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['insert'] = $this->url->link('design/beverlysion/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('design/beverlysion/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['banners'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$banner_total = $this->model_design_beverlysion->getTotalBanners_beverlysion();
		
		$results = $this->model_design_beverlysion->getBanners_beverlysion($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('design/beverlysion/update', 'token=' . $this->session->data['token'] . '&banner_id=' . $result['banner_id'] . $url, 'SSL'),
				'href_beverlysion' => $this->url->link('design/beverlysion/update_beverlysion', 'token=' . $this->session->data['token'] . '&banner_id=' . $result['banner_id'] . $url, 'SSL')
			);

			$this->data['banners'][] = array(
				'banner_id' => $result['banner_id'],
				'name'      => $result['name'],
				'beverlysion'      => $result['beverlysion'],
				'beverlysion_sts'   => ($result['beverlysion'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'status'    => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),				
				'selected'  => isset($this->request->post['selected']) && in_array($result['banner_id'], $this->request->post['selected']),				
				'action'    => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('design/beverlysion', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('design/beverlysion', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $banner_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('design/beverlysion', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'design/banner_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

protected function getForm_beverlysion() {
		
		$this->data['beverlysion'] = $this->language->get('beverlysion');
		$this->data['entry_beverlysion_global'] = $this->language->get('entry_beverlysion_global');
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
 		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');			
				
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_link'] = $this->language->get('entry_link');
		$this->data['entry_image'] = $this->language->get('entry_image');		
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_banner'] = $this->language->get('button_add_banner');
		$this->data['button_remove'] = $this->language->get('button_remove');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

 		if (isset($this->error['banner_image'])) {
			$this->data['error_banner_image'] = $this->error['banner_image'];
		} else {
			$this->data['error_banner_image'] = array();
		}	
						
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('design/beverlysion', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		if (!isset($this->request->get['banner_id'])) { 
			$this->data['action'] = $this->url->link('design/beverlysion/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('design/beverlysion/update_beverlysion', 'token=' . $this->session->data['token'] . '&banner_id=' . $this->request->get['banner_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('design/banner', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['banner_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$banner_info = $this->model_design_beverlysion->getBanner_beverlysion($this->request->get['banner_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($banner_info)) {
			$this->data['name'] = $banner_info['name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($banner_info)) {
			$this->data['status'] = $banner_info['status'];
		} else {
			$this->data['status'] = true;
		}
		
		if (isset($this->request->post['beverlysion'])) {
			$this->data['beverlysion'] = $this->request->post['beverlysion'];
		} elseif (!empty($banner_info)) {
			$this->data['beverlysion'] = $banner_info['beverlysion'];
		} else {
			$this->data['beverlysion'] = true;
		}

		$this->load->model('localisation/language');
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->load->model('tool/image');
	
		if (isset($this->request->post['banner_image'])) {
			$banner_images = $this->request->post['banner_image'];
		} elseif (isset($this->request->get['banner_id'])) {
			$banner_images = $this->model_design_beverlysion->getBannerImages_beverlysion($this->request->get['banner_id']);	
		} else {
			$banner_images = array();
		}
		
		$this->data['banner_images'] = array();
		
		foreach ($banner_images as $banner_image) {
			if ($banner_image['image'] && file_exists(DIR_IMAGE . $banner_image['image'])) {
				$image = $banner_image['image'];
				$sm_image_1 = $banner_image['sm_pic_1'];
				$sm_image_2 = $banner_image['sm_pic_2'];
				$sm_image_3 = $banner_image['sm_pic_3'];
				$sm_image_4 = $banner_image['sm_pic_4'];
				$sm_image_5 = $banner_image['sm_pic_5'];
			} else {
				$image = 'no_image.jpg';
				$sm_image_1 = '';
				$sm_image_2 = '';
				$sm_image_3 = '';
				$sm_image_4 = '';
				$sm_image_5 = '';
			}			
			
			$this->data['banner_images'][] = array(
				'banner_image_description' => $banner_image['banner_image_description'],
				'link'                     => $banner_image['link'],
				'image'                    => $image,
				'beverlysion_act'          => $banner_image['beverlysion_act'],
				'sm_pic_1'                => $sm_image_1,
				'sm_pic_1_data_x'         => $banner_image['sm_pic_1_data_x'],
				'sm_pic_1_data_y'         => $banner_image['sm_pic_1_data_y'],
				'sm_pic_1_data_sp'        => $banner_image['sm_pic_1_data_sp'],
				'sm_pic_1_data_st'        => $banner_image['sm_pic_1_data_st'],
				'sm_pic_1_data_ea'        => $banner_image['sm_pic_1_data_ea'],
				'sm_pic_1_data_cls'       => $banner_image['sm_pic_1_data_cls'],
				'sm_pic_1_url'            => $banner_image['sm_pic_1_url'],
				'sm_pic_2'                => $sm_image_2,
				'sm_pic_2_data_x'         => $banner_image['sm_pic_2_data_x'],
				'sm_pic_2_data_y'         => $banner_image['sm_pic_2_data_y'],
				'sm_pic_2_data_sp'        => $banner_image['sm_pic_2_data_sp'],
				'sm_pic_2_data_st'        => $banner_image['sm_pic_2_data_st'],
				'sm_pic_2_data_ea'        => $banner_image['sm_pic_2_data_ea'],
				'sm_pic_2_data_cls'       => $banner_image['sm_pic_2_data_cls'],
				'sm_pic_2_url'            => $banner_image['sm_pic_2_url'],
				'sm_pic_3'                => $sm_image_3,
				'sm_pic_3_data_x'         => $banner_image['sm_pic_3_data_x'],
				'sm_pic_3_data_y'         => $banner_image['sm_pic_3_data_y'],
				'sm_pic_3_data_sp'        => $banner_image['sm_pic_3_data_sp'],
				'sm_pic_3_data_st'        => $banner_image['sm_pic_3_data_st'],
				'sm_pic_3_data_ea'        => $banner_image['sm_pic_3_data_ea'],
				'sm_pic_3_data_cls'       => $banner_image['sm_pic_3_data_cls'],
				'sm_pic_4'                => $sm_image_4,
				'sm_pic_4_data_x'         => $banner_image['sm_pic_4_data_x'],
				'sm_pic_4_data_y'         => $banner_image['sm_pic_4_data_y'],
				'sm_pic_4_data_sp'        => $banner_image['sm_pic_4_data_sp'],
				'sm_pic_4_data_st'        => $banner_image['sm_pic_4_data_st'],
				'sm_pic_4_data_ea'        => $banner_image['sm_pic_4_data_ea'],
				'sm_pic_4_data_cls'       => $banner_image['sm_pic_4_data_cls'],
				'sm_pic_5'                => $sm_image_5,
				'sm_pic_5_data_x'         => $banner_image['sm_pic_5_data_x'],
				'sm_pic_5_data_y'         => $banner_image['sm_pic_5_data_y'],
				'sm_pic_5_data_sp'        => $banner_image['sm_pic_5_data_sp'],
				'sm_pic_5_data_st'        => $banner_image['sm_pic_5_data_st'],
				'sm_pic_5_data_ea'        => $banner_image['sm_pic_5_data_ea'],
				'sm_pic_5_data_cls'       => $banner_image['sm_pic_5_data_cls'],
				'caption_1'                => $banner_image['caption_1'],
				'caption_1_data_x'         => $banner_image['caption_1_data_x'],
				'caption_1_data_y'         => $banner_image['caption_1_data_y'],
				'caption_1_data_sp'        => $banner_image['caption_1_data_sp'],
				'caption_1_data_st'        => $banner_image['caption_1_data_st'],
				'caption_1_data_ea'        => $banner_image['caption_1_data_ea'],
				'caption_1_data_cls'       => $banner_image['caption_1_data_cls'],
				'caption_1_url'            => $banner_image['caption_1_url'],
				'caption_2'                => $banner_image['caption_2'],
				'caption_2_data_x'         => $banner_image['caption_2_data_x'],
				'caption_2_data_y'         => $banner_image['caption_2_data_y'],
				'caption_2_data_sp'        => $banner_image['caption_2_data_sp'],
				'caption_2_data_st'        => $banner_image['caption_2_data_st'],
				'caption_2_data_ea'        => $banner_image['caption_2_data_ea'],
				'caption_2_data_cls'       => $banner_image['caption_2_data_cls'],
				'caption_2_url'            => $banner_image['caption_2_url'],
				'caption_3'                => $banner_image['caption_3'],
				'caption_3_data_x'         => $banner_image['caption_3_data_x'],
				'caption_3_data_y'         => $banner_image['caption_3_data_y'],
				'caption_3_data_sp'        => $banner_image['caption_3_data_sp'],
				'caption_3_data_st'        => $banner_image['caption_3_data_st'],
				'caption_3_data_ea'        => $banner_image['caption_3_data_ea'],
				'caption_3_data_cls'       => $banner_image['caption_3_data_cls'],
				'caption_4'                => $banner_image['caption_4'],
				'caption_4_data_x'         => $banner_image['caption_4_data_x'],
				'caption_4_data_y'         => $banner_image['caption_4_data_y'],
				'caption_4_data_sp'        => $banner_image['caption_4_data_sp'],
				'caption_4_data_st'        => $banner_image['caption_4_data_st'],
				'caption_4_data_ea'        => $banner_image['caption_4_data_ea'],
				'caption_4_data_cls'       => $banner_image['caption_4_data_cls'],
				'caption_5'                => $banner_image['caption_5'],
				'caption_5_data_x'         => $banner_image['caption_5_data_x'],
				'caption_5_data_y'         => $banner_image['caption_5_data_y'],
				'caption_5_data_sp'        => $banner_image['caption_5_data_sp'],
				'caption_5_data_st'        => $banner_image['caption_5_data_st'],
				'caption_5_data_ea'        => $banner_image['caption_5_data_ea'],
				'caption_5_data_cls'       => $banner_image['caption_5_data_cls'],
				'button_1'                => $banner_image['button_1'],
				'button_1_data_x'         => $banner_image['button_1_data_x'],
				'button_1_data_y'         => $banner_image['button_1_data_y'],
				'button_1_data_sp'        => $banner_image['button_1_data_sp'],
				'button_1_data_st'        => $banner_image['button_1_data_st'],
				'button_1_data_ea'        => $banner_image['button_1_data_ea'],
				'button_1_data_cls'       => $banner_image['button_1_data_cls'],
				'button_1_url'       => $banner_image['button_1_url'],
				'youtube_iframe'                => $banner_image['youtube_iframe'],
				'youtube_autoplay'                => $banner_image['youtube_autoplay'],
				'youtube_data_x'         => $banner_image['youtube_data_x'],
				'youtube_data_y'         => $banner_image['youtube_data_y'],
				'youtube_data_sp'        => $banner_image['youtube_data_sp'],
				'youtube_data_st'        => $banner_image['youtube_data_st'],
				'youtube_data_ea'        => $banner_image['youtube_data_ea'],
				'youtube_data_cls'       => $banner_image['youtube_data_cls'],
				'vimeo_iframe'                => $banner_image['vimeo_iframe'],
				'vimeo_autoplay'                => $banner_image['vimeo_autoplay'],
				'vimeo_data_x'         => $banner_image['vimeo_data_x'],
				'vimeo_data_y'         => $banner_image['vimeo_data_y'],
				'vimeo_data_sp'        => $banner_image['vimeo_data_sp'],
				'vimeo_data_st'        => $banner_image['vimeo_data_st'],
				'vimeo_data_ea'        => $banner_image['vimeo_data_ea'],
				'vimeo_data_cls'       => $banner_image['vimeo_data_cls'],
				'transicion'                => $banner_image['transicion'],
				'slot_amt'                => $banner_image['slot_amt'],
				'data_delay'         => $banner_image['data_delay'],
				'master_speed'         => $banner_image['master_speed'],
				'sm_pic_1_data_h'         => $banner_image['sm_pic_1_data_h'],
				'sm_pic_1_data_w'         => $banner_image['sm_pic_1_data_w'],
				'sm_pic_2_data_h'         => $banner_image['sm_pic_2_data_h'],
				'sm_pic_2_data_w'         => $banner_image['sm_pic_2_data_w'],
				'sm_pic_3_data_h'         => $banner_image['sm_pic_3_data_h'],
				'sm_pic_3_data_w'         => $banner_image['sm_pic_3_data_w'],
				'sm_pic_4_data_h'         => $banner_image['sm_pic_4_data_h'],
				'sm_pic_4_data_w'         => $banner_image['sm_pic_4_data_w'],
				'sm_pic_5_data_h'         => $banner_image['sm_pic_5_data_h'],
				'sm_pic_5_data_w'         => $banner_image['sm_pic_5_data_w'],
				'youtube_data_h'         => $banner_image['youtube_data_h'],
				'youtube_data_w'         => $banner_image['youtube_data_w'],
				'vimeo_data_h'         => $banner_image['vimeo_data_h'],
				'vimeo_data_w'         => $banner_image['vimeo_data_w'],
				'thumb_1'                    => $this->model_tool_image->resize($sm_image_1, 100, 100),
				'thumb_2'                    => $this->model_tool_image->resize($sm_image_2, 100, 100),
				'thumb_3'                    => $this->model_tool_image->resize($sm_image_3, 100, 100),
				'thumb_4'                    => $this->model_tool_image->resize($sm_image_4, 100, 100),
				'thumb_5'                    => $this->model_tool_image->resize($sm_image_5, 100, 100),
				'thumb'                    => $this->model_tool_image->resize($image, 100, 100)
			);	
		} 
	
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);		

		$this->template = 'design/banner_form_beverlysion.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'design/beverlysion')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		if (isset($this->request->post['banner_image'])) {
			foreach ($this->request->post['banner_image'] as $banner_image_id => $banner_image) {
				foreach ($banner_image['banner_image_description'] as $language_id => $banner_image_description) {
					if ((utf8_strlen($banner_image_description['title']) < 2) || (utf8_strlen($banner_image_description['title']) > 64)) {
						$this->error['banner_image'][$banner_image_id][$language_id] = $this->language->get('error_title'); 
					}					
				}
			}	
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'design/beverlysion')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>