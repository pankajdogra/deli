<?php
class ControllerToolcrawlprotect extends Controller {
	public function index() {
		$this->data = $this->load->language('tool/crawlprotect');
		$this->document->setTitle($this->data['heading_title']);
		$this->document->addStyle('view/stylesheet/stylesheet.css');
		$this->template = 'tool/crawlprotect.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());
	}
}
?>