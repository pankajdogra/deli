<?php

class ControllerModuleCouponForTracking extends Controller {

	private $name = 'cftyp';
	
	public function index() {
		$mname = basename(__FILE__, '.php');				
		$this->data = array_merge($this->data, $this->load->language('module/' . $mname));	
		$this->document->setTitle($this->language->get('heading_title'));
		$this->data[$this->name . '_always'] = $this->config->get($this->name . '_always');
		if(isset($this->request->post[$this->name . '_always'])) {
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting($this->name, array(
				$this->name . '_status' => 1,
				$this->name . '_always' => ($this->request->post[$this->name . '_always'] ? 1 : 0)
			));			
			$this->data[$this->name . '_always'] = ($this->request->post[$this->name . '_always'] ? 1 : 0);
		}
		$this->data['classname'] = $mname;
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/' . $mname, 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['action'] = $this->url->link('module/' . $mname, 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'module/'.$mname.'.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());	
	}
	
	public function install() {
		$this->db->query("CREATE TABLE if not exists " . DB_PREFIX . $this->name . "_coupon_affiliate (
			coupon_id int(11) unsigned not null default '0',
			affiliate_id int(11) unsigned not null default '0',
			primary key (coupon_id)		
		) ENGINE=MyISAM");
		
		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting($this->name, array(
			$this->name . '_status' => 1,
			$this->name . '_always' => 0
		));	
	}
	
	public function uninstall() {
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting($this->name);
	}

}
