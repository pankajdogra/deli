<?php 
class ControllerCatalogWinner extends Controller {
	private $error = array(); 
     
  	public function index() {
		$this->load->language('catalog/winner');
    	
		$this->document->setTitle($this->language->get('heading_title')); 
		
		$this->load->model('catalog/winner');
		
		$this->getList();
  	}
  
  	

  	

  	public function delete() {
    	$this->load->language('catalog/winner');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/winner');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $winner_id) {
				$this->model_catalog_winner->deletewinner($winner_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}
		
			
			if (isset($this->request->get['filter_names'])) {
			$url .= '&filter_names=' . urlencode(html_entity_decode($this->request->get['filter_names'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['filter_bid_date_end'])) {
			$url .= '&filter_bid_date_end=' . $this->request->get['filter_bid_date_end'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_names'])) {
			$filter_names= $this->request->get['filter_names'];
		} else {
			$filter_names = null;
		}
		
		
		if (isset($this->request->get['filter_nickname'])) {
			$filter_nickname = $this->request->get['filter_nickname'];
		} else {
			$filter_nickname = null;
		}
					
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

    	$this->getList();
  	}

  	
	
  	private function getList() {				
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		
		if (isset($this->request->get['filter_bid_date_start'])) {
			$filter_bid_date_start = $this->request->get['filter_bid_date_start'];
		} else {
			$filter_bid_date_start = null;
		}

		if (isset($this->request->get['filter_bid_date_end'])) {
			$filter_bid_date_end = $this->request->get['filter_bid_date_end'];
		} else {
			$filter_bid_date_end = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		
		if (isset($this->request->get['filter_names'])) {
			$filter_names= $this->request->get['filter_names'];
		} else {
			$filter_names = null;
		}
		
		
		if (isset($this->request->get['filter_nickname'])) {
			$filter_nickname = $this->request->get['filter_nickname'];
		} else {
			$filter_nickname = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
						
		$url = '';
						
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		
		if (isset($this->request->get['filter_names'])) {
			$url .= '&filter_names=' . urlencode(html_entity_decode($this->request->get['filter_names'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['filter_bid_date_end'])) {
			$url .= '&filter_bid_date_end=' . $this->request->get['filter_bid_date_end'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
						
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url, 'SSL'),       		
      		'separator' => ' :: '
   		);
		
		$this->data['insert'] = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['winner'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['copy'] = $this->url->link('catalog/winner/copy', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['delete'] = $this->url->link('catalog/winner/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
    	
		$this->data['products'] = array();

		$data = array(
			'filter_name'	  => $filter_name, 
			'filter_price'	  => $filter_price,
			'filter_bid_date_start'	  => $filter_bid_date_start,
			'filter_bid_date_end' => $filter_bid_date_end,
			'filter_status'   => $filter_status,
			'filter_names'   => $filter_names,
			'filter_nickname'   => $filter_nickname,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		
		$this->load->model('tool/image');
		
		$winner_total = $this->model_catalog_winner->getTotalProducts($data);
			
		$results = $this->model_catalog_winner->getProducts($data);
				    	
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/product/update', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
			
			$bids=$this->model_catalog_winner->getipblock($result['customer_id']);
			
			
			
	
      		$this->data['products'][] = array(
				'winner_id' => $result['winner_id'],
				'name'       => $result['name'],
				'nickname'      => $result['nickname'],
				'bid_price'      => $result['price_bid'],
				'dated_added'      => $result['date_added'],
				'productname'   => $result['productname'],
				'status'     => $result['status'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['winner_id'], $this->request->post['selected']),
				'ban'       => $bids
			);
    	}
		
		$this->data['heading_title'] = $this->language->get('heading_title');		
				
		$this->data['text_enabled'] = $this->language->get('text_enabled');		
		$this->data['text_disabled'] = $this->language->get('text_disabled');		
		$this->data['text_no_results'] = $this->language->get('text_no_results');		
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');		
			
		$this->data['column_image'] = $this->language->get('column_image');		
		$this->data['column_name'] = $this->language->get('column_name');		
		$this->data['column_names'] = $this->language->get('column_names');		
		$this->data['column_nickname'] = $this->language->get('column_nickname');		
		$this->data['column_bid_date_end'] = $this->language->get('column_bid_date_end');		
		$this->data['column_status'] = $this->language->get('column_status');		
		$this->data['column_action'] = $this->language->get('column_action');

       $this->data['column_bid_price'] = $this->language->get('column_bid_price');		
		$this->data['column_ban'] = $this->language->get('column_ban');		
				
		$this->data['button_copy'] = $this->language->get('button_copy');		
		$this->data['button_insert'] = $this->language->get('button_insert');		
		$this->data['button_delete'] = $this->language->get('button_delete');		
		$this->data['button_filter'] = $this->language->get('button_filter');
		
		$this->data['button_auction'] = $this->language->get('button_auction');	
		 
 		$this->data['token'] = $this->session->data['token'];
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_names'])) {
			$url .= '&filter_names=' . urlencode(html_entity_decode($this->request->get['filter_names'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['filter_bid_date_end'])) {
			$url .= '&filter_bid_date_end=' . $this->request->get['filter_bid_date_end'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

								
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
					
		$this->data['sort_name'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$this->data['sort_names'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . '&sort=w.name' . $url, 'SSL');
		$this->data['sort_nickname'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . '&sort=w.nickname' . $url, 'SSL');
		$this->data['sort_bid_date_end'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . '&sort=w.date_added' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . '&sort=w.status' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . '&sort=w.price_bid' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_names'])) {
			$url .= '&filter_names=' . urlencode(html_entity_decode($this->request->get['filter_names'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['filter_bid_date_end'])) {
			$url .= '&filter_bid_date_end=' . $this->request->get['filter_bid_date_end'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
				
		$pagination = new Pagination();
		$pagination->total = $winner_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_names'] = $filter_names;
		$this->data['filter_price'] = $filter_price;
		$this->data['filter_bid_date_start'] = $filter_bid_date_start;
		$this->data['filter_bid_date_end'] = $filter_bid_date_end;
		$this->data['filter_status'] = $filter_status;
		$this->data['filter_nickname'] = $filter_nickname;
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/winner_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
  	}

  	 
	
  	private function validateForm() { 
    	if (!$this->user->hasPermission('modify', 'catalog/winner')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

    	foreach ($this->request->post['winner_description'] as $language_id => $value) {
      		if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 255)) {
        		$this->error['name'][$language_id] = $this->language->get('error_name');
      		}
    	}
		
    	if ((utf8_strlen($this->request->post['model']) < 1) || (utf8_strlen($this->request->post['model']) > 64)) {
      		$this->error['model'] = $this->language->get('error_model');
    	}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
					
    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}
	
  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/winner')) {
      		$this->error['warning'] = $this->language->get('error_permission');  
    	}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
  	
  	private function validateCopy() {
    	if (!$this->user->hasPermission('modify', 'catalog/winner')) {
      		$this->error['warning'] = $this->language->get('error_permission');  
    	}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
		
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_names']) || isset($this->request->get['filter_nickname'])) {
			$this->load->model('catalog/winner');
			
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}
			
			if (isset($this->request->get['filter_names'])) {
				$filter_names = $this->request->get['filter_names'];
			} else {
				$filter_names = '';
			}
						
			if (isset($this->request->get['filter_nickname'])) {
				$filter_nickname = $this->request->get['filter_nickname'];
			} else {
				$filter_nickname = '';
			}
			
			
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];	
			} else {
				$limit = 20;	
			}			
						
			$data = array(
				'filter_name'         => $filter_name,
				'filter_model'        => $filter_names,
				'filter_nickname'  => $filter_nickname,
				'start'               => 0,
				'limit'               => $limit
			);
			
			$results = $this->model_catalog_winner->getProducts($data);
			
			foreach ($results as $result) {
				
				$json[] = array(
					'product_id' => $result['product_id'],
					'productname'       => html_entity_decode($result['productname'], ENT_QUOTES, 'UTF-8'),	
					'name'       => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'),
					'nickname'       => html_entity_decode($result['nickname'], ENT_QUOTES, 'UTF-8'),
					'price_bid'      => $result['price_bid']
				);	
			}
		}

		$this->response->setOutput(json_encode($json));
	}
}
?>