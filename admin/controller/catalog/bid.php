<?php 
class ControllerCatalogBid extends Controller {
	private $error = array(); 
     
  	public function index() {
		$this->load->language('catalog/bid');
    	
		$this->document->setTitle($this->language->get('heading_title')); 
		
		$this->load->model('catalog/auctionlist');
		
		$this->getList();
  	}
  
  	
  	
	

  	public function delete() {
    	$this->load->language('catalog/bid');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/auctionlist');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $auctionlist_id) {
				$this->model_catalog_auctionlist->deletebid($auctionlist_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		
		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
					
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('catalog/auctionlist', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

    	$this->getList();
  	}
	
	
	

  	
  	private function getList() {				
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_nickname'])) {
			$filter_nickname = $this->request->get['filter_nickname'];
		} else {
			$filter_nickname = null;
		}
		
		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
						
		$url = '';
						
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		
		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
						
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . $url, 'SSL'),       		
      		'separator' => ' :: '
   		);
		
		$this->data['insert'] = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['winner'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['copy'] = $this->url->link('catalog/bid/copy', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['delete'] = $this->url->link('catalog/bid/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
    	
		$this->data['products'] = array();

		$data = array(
			'filter_name'	  => $filter_name, 
			'filter_price'	  => $filter_price,
			'filter_date'	  => $filter_date,
			'filter_nickname' => $filter_nickname,
			'filter_status'   => $filter_status,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		
		$this->load->model('tool/image');
		
		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = '';
		}
		
		$this->data['product_id'] =  $product_id;
		
		
		$bid_total = $this->model_catalog_auctionlist->getTotalbids($data,$product_id);
		
			
		$results = $this->model_catalog_auctionlist->getbids($data,$product_id);
				    	
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/bid/update', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
			
			
	
      		$this->data['products'][] = array(
				'customer_bid_id' => $result['customer_bid_id'],
				'name'       => $result['name'].' '.$result['lastname'],
				'nickname'      => $result['nickname'],
				'bid_price'      => $result['price_bid'],
				'dated_added'      => $result['date_added'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'   => isset($this->request->post['selected']) && in_array($result['customer_bid_id'], $this->request->post['selected']),
				'ban'       => $result['ban']
			);
    	}
		
		$this->data['heading_title'] = $this->language->get('heading_title');		
				
		$this->data['text_enabled'] = $this->language->get('text_enabled');		
		$this->data['text_disabled'] = $this->language->get('text_disabled');		
		$this->data['text_no_results'] = $this->language->get('text_no_results');		
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');		
			
				
		$this->data['column_name'] = $this->language->get('column_name');		
		$this->data['column_names'] = $this->language->get('column_names');		
		$this->data['column_nickname'] = $this->language->get('column_nickname');		
		$this->data['column_bid_date_end'] = $this->language->get('column_bid_date_end');		
		$this->data['column_status'] = $this->language->get('column_status');		
		$this->data['column_action'] = $this->language->get('column_action');

       $this->data['column_bid_price'] = $this->language->get('column_bid_price');		
		$this->data['column_ban'] = $this->language->get('column_ban');	
		
		$this->data['button_copy'] = $this->language->get('button_copy');		
		$this->data['button_insert'] = $this->language->get('button_insert');		
		$this->data['button_delete'] = $this->language->get('button_delete');		
		$this->data['button_filter'] = $this->language->get('button_filter');
		
		$this->data['button_auction'] = $this->language->get('button_auction');	
		
		
		
		 
 		$this->data['token'] = $this->session->data['token'];
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		
		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['product_id'])) {
			$url .= '&product_id=' . $this->request->get['product_id'];
		}

		

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
								
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
					
		$this->data['sort_name'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . '&sort=cb.name' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . '&sort=cb.price_bid' . $url, 'SSL');
		$this->data['sort_date'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . '&sort=cb.date_added' . $url, 'SSL');
		$this->data['sort_nickname'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . '&sort=cb.nickname' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$this->data['sort_order'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_nickname'])) {
			$url .= '&filter_nickname=' . urlencode(html_entity_decode($this->request->get['filter_nickname'], ENT_QUOTES, 'UTF-8'));
		}
		
		
		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
				
		$pagination = new Pagination();
		$pagination->total = $bid_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_nickname'] = $filter_nickname;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_price'] = $filter_price;
		$this->data['filter_status'] = $filter_status;
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/bid_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
  	}

  	private function getForm() {
    	$this->data['heading_title'] = $this->language->get('heading_title');
 
    	$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
    	$this->data['text_none'] = $this->language->get('text_none');
    	$this->data['text_yes'] = $this->language->get('text_yes');
    	$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$this->data['text_plus'] = $this->language->get('text_plus');
		$this->data['text_minus'] = $this->language->get('text_minus');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_option_value'] = $this->language->get('text_option_value');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
    	$this->data['entry_model'] = $this->language->get('entry_model');
		$this->data['entry_sku'] = $this->language->get('entry_sku');
		$this->data['entry_upc'] = $this->language->get('entry_upc');
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_minimum'] = $this->language->get('entry_minimum');
		$this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
    	$this->data['entry_shipping'] = $this->language->get('entry_shipping');
    	$this->data['entry_date_available'] = $this->language->get('entry_date_available');
    	$this->data['entry_quantity'] = $this->language->get('entry_quantity');
		$this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
    	$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_points'] = $this->language->get('entry_points');
		$this->data['entry_option_points'] = $this->language->get('entry_option_points');
		$this->data['entry_subtract'] = $this->language->get('entry_subtract');
    	$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
    	$this->data['entry_weight'] = $this->language->get('entry_weight');
		$this->data['entry_dimension'] = $this->language->get('entry_dimension');
		$this->data['entry_length'] = $this->language->get('entry_length');
    	$this->data['entry_image'] = $this->language->get('entry_image');
    	$this->data['entry_download'] = $this->language->get('entry_download');
    	$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_related'] = $this->language->get('entry_related');
		$this->data['entry_attribute'] = $this->language->get('entry_attribute');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_option'] = $this->language->get('entry_option');
		$this->data['entry_option_value'] = $this->language->get('entry_option_value');
		$this->data['entry_required'] = $this->language->get('entry_required');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_priority'] = $this->language->get('entry_priority');
		$this->data['entry_tag'] = $this->language->get('entry_tag');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_reward'] = $this->language->get('entry_reward');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
				
    	$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_delete'] = $this->language->get('button_delete');
    	$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$this->data['button_add_option'] = $this->language->get('button_add_option');
		$this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$this->data['button_add_discount'] = $this->language->get('button_add_discount');
		$this->data['button_add_special'] = $this->language->get('button_add_special');
		$this->data['button_add_image'] = $this->language->get('button_add_image');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->data['button_auction'] = $this->language->get('button_auction');	
		
		
		$this->data['button_finish'] = $this->language->get('button_finish');
		
		$this->data['button_restart'] = $this->language->get('button_restart');
		
		$this->data['text_bid_only_reg'] = $this->language->get('text_bid_only_reg');
			$this->data['text_bid_start_from'] = $this->language->get('text_bid_start_from');
			$this->data['text_bid_curr'] = $this->language->get('text_bid_curr');
			$this->data['text_bid_bids'] = $this->language->get('text_bid_bids');
			$this->data['text_bid_place'] = $this->language->get('text_bid_place');
			$this->data['text_bid_submit'] = $this->language->get('text_bid_submit');
			$this->data['text_bid_delete'] = $this->language->get('text_bid_delete');
			$this->data['text_bid_curr_is'] = $this->language->get('text_bid_curr_is');
			$this->data['text_bid_accept'] = $this->language->get('text_bid_accept');
			$this->data['text_bid_refuse'] = $this->language->get('text_bid_refuse');
			$this->data['text_bid_big_off'] = $this->language->get('text_bid_big_off');
			$this->data['text_bid_explain'] = $this->language->get('text_bid_explain');	
			$this->data['text_bid_reserved'] = $this->language->get('text_bid_reserved');
		
    	$this->data['tab_general'] = $this->language->get('tab_general');
    	$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_attribute'] = $this->language->get('tab_attribute');
		$this->data['tab_option'] = $this->language->get('tab_option');		
		$this->data['tab_discount'] = $this->language->get('tab_discount');
		$this->data['tab_special'] = $this->language->get('tab_special');
    	$this->data['tab_image'] = $this->language->get('tab_image');		
		$this->data['tab_links'] = $this->language->get('tab_links');
		$this->data['tab_reward'] = $this->language->get('tab_reward');
		$this->data['tab_design'] = $this->language->get('tab_design');

$this->data['button_view'] = $this->language->get('button_view');

$this->data['button_bids'] = $this->language->get('button_bids');		
		
		
		/**auction***/				
		
		$this->data['text_close_time'] = $this->language->get('text_close_time');
	
			
		$this->data['text_winner'] = $this->language->get('text_winner');
		
		$this->data['text_state'] = $this->language->get('text_state');
		
		$this->data['text_progress'] = $this->language->get('text_progress');
		
		
		$this->data['text_closed'] = $this->language->get('text_closed');
		
			$this->data['text_part'] = $this->language->get('text_part');
			
			$this->data['text_current_price'] = $this->language->get('text_current_price');
			
			$this->data['text_min_step'] = $this->language->get('text_min_step');
			
			
		$this->data['tab_auction'] = $this->language->get('tab_auction');				$this->data['entry_auction'] = $this->language->get('entry_auction');				$this->data['entry_end_time'] = $this->language->get('entry_end_time');						$this->data['entry_end_desc'] = $this->language->get('entry_end_desc');				$this->data['entry_max'] = $this->language->get('entry_max');						$this->data['entry_max_desc'] = $this->language->get('entry_max_desc');								$this->data['auction_settings'] = $this->language->get('auction_settings');				$this->data['auction_status'] = $this->language->get('auction_status');						$this->data['auction_start'] = $this->language->get('auction_start');				$this->data['auction_start_desc'] = $this->language->get('auction_start_desc');						$this->data['auction_end'] = $this->language->get('auction_end');						$this->data['auction_end_desc'] = $this->language->get('auction_end_desc');				$this->data['auction_starting'] = $this->language->get('auction_starting');						$this->data['auction_tax'] = $this->language->get('auction_tax');				$this->data['auction_starting_desc'] = $this->language->get('auction_starting_desc');						$this->data['auction_tax_desc'] = $this->language->get('auction_tax_desc');						$this->data['auction_max'] = $this->language->get('auction_max');						$this->data['auction_min'] = $this->language->get('auction_min');				$this->data['auction_max_desc'] = $this->language->get('auction_max_desc');						$this->data['auction_min_desc'] = $this->language->get('auction_min_desc');						$this->data['auction_auto'] = $this->language->get('auction_auto');						$this->data['auction_time'] = $this->language->get('auction_time');				$this->data['auction_auto_desc'] = $this->language->get('auction_auto_desc');						$this->data['auction_time_desc'] = $this->language->get('auction_time_desc');				/**end**/
		 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

 		if (isset($this->error['meta_description'])) {
			$this->data['error_meta_description'] = $this->error['meta_description'];
		} else {
			$this->data['error_meta_description'] = array();
		}		
   
   		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}	
		
   		if (isset($this->error['model'])) {
			$this->data['error_model'] = $this->error['model'];
		} else {
			$this->data['error_model'] = '';
		}		
     	
		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
		} else {
			$this->data['error_date_available'] = '';
		}


if (isset($this->error['start_time'])) {			
$this->data['error_start'] = $this->error['start_time'];		} else {			
$this->data['error_start'] = '';		}						if (isset($this->error['bid_price'])) {	
		$this->data['error_bid_price'] = $this->error['bid_price'];		} else {			$this->data['error_bid_price'] = '';		}       if (isset($this->error['end_time'])) {			$this->data['error_end_time'] = $this->error['end_time'];		} else {			$this->data['error_end_time'] = '';		} 
     	
		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
		} else {
			$this->data['error_date_available'] = '';
		}		

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}	
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
								
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['productbidid'] = $this->model_catalog_auctionlist->getid($this->request->get['product_id']);
		
		$this->data['checkstatebid'] = $this->model_catalog_auctionlist->checkBidstate($this->request->get['product_id']);
		
		$this->data['path'] = HTTP_CATALOG.'index.php?route=product/product&product_id='.$this->request->get['product_id'];
		
		if (!empty($this->data['productbidid'])) {			
		$this->data['product_bid_id'] =  $this->data['productbidid'];	
		} else {			
		$this->data['product_bid_id'] ='';	
		}
									
		
		if (!isset($this->request->get['product_id'])) {
		$this->data['product_id'] = '';
		} else {
		$this->data['product_id'] = $this->request->get['product_id'];
		}
		
		
		
		if (!isset($this->request->get['product_id'])) {
		$this->data['bids'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		$this->data['bids'] = $this->url->link('catalog/bid', 'token=' . $this->session->data['token'] . '&product_id=' .$this->request->get['product_id'].$url, 'SSL');
		}
		
		if (!isset($this->request->get['product_id'])) {
		$this->data['restart'] = $this->url->link('catalog/auctionlist/closebid', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		$this->data['restart'] = $this->url->link('catalog/auctionlist/closebid', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'].'&auction_id=' . $this->data['product_bid_id'].'&status=' . 0 . $url, 'SSL');
		}
		
		$this->data['winner'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (!isset($this->request->get['product_id'])) {
		$this->data['update'] = $this->url->link('catalog/auctionlist/update1', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		$this->data['update'] = $this->url->link('catalog/auctionlist/update1', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('catalog/auctionlist', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		
		$this->data['winner'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		
		$this->data['view'] = $this->url->link('catalog/winner', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		

		
		$this->data['token'] = $this->session->data['token'];
		
		$this->load->model('localisation/language');
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
				
		$special = false;
		
		if (isset($this->request->get['product_id'])) {
			
		$product_specials = $this->model_catalog_auctionlist->getProductSpecials($this->request->get['product_id']);
			
			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] < date('Y-m-d')) && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] > date('Y-m-d'))) {
					$special = $product_special['price'];
			
					break;
				}					
			
		}
		
		}
		
		if (isset($this->request->get['product_id'])) {      		
		$auction = $this->model_catalog_auctionlist->getproductauction($this->request->get['product_id']);    	
		}else{		
		$auction ="";		
		}
		
		$this->data['maxcustomerbid'] = $this->model_catalog_auctionlist->forMaxProductBids($this->request->get['product_id']);
		
		$this->data['countCustomerBids'] = $this->model_catalog_auctionlist->countCustomerBids($this->request->get['product_id']);
		
		$this->data['maxcustomerbids'] = $this->currency->format($this->data['maxcustomerbid']);
		
		$this->data['checkwinner'] = $this->model_catalog_auctionlist->checkwinner($this->request->get['product_id']);
		
		
		
		
		
		if (isset($this->request->post['price'])) {
      		$this->data['price'] = $this->request->post['price'];
    	} elseif (!empty($auction)) {
			$this->data['price'] = $auction['price'];
		} else {
      		$this->data['price'] = '';
    	}
		
		
			$this->data['special'] = $special;
		
		
		
		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
		} elseif (!empty($auction)) {
			$this->data['image'] = $auction['image'];
		} else {
			$this->data['image'] = '';
		}
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($auction)) {
			$this->data['name'] = $auction['name'];
		} else {
			$this->data['name'] = '';
		}
		
		$this->load->model('tool/image');
		
		if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($auction) && $auction['image'] && file_exists(DIR_IMAGE . $auction['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($auction['image'], 100, 100);
		} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

	
		
		if (isset($this->request->post['use_end_time_on'])) {		
		$this->data['use_end_time_on'] = $this->request->post['use_end_time_on'];		
		} elseif (!empty($auction['end_time_status'])) {			
		$this->data['use_end_time_on'] =  $auction['end_time_status'];		
		} else {			
		$this->data['use_end_time_on'] ='';	
		}


        		
		
		if (isset($this->request->post['use_max_price_on'])) {			
		$this->data['use_max_price_on'] = $this->request->post['use_max_price_on'];		
		} elseif (!empty($auction['max_price_status'])) {			
		$this->data['use_max_price_on'] =  $auction['max_price_status'];		
		} else {			
		$this->data['use_max_price_on'] ="";		
		}						
		
		if (isset($this->request->post['status_on'])) {			
		$this->data['status_on'] = $this->request->post['status_on'];		
		} elseif (!empty($auction['auction_status'])) {			
		$this->data['status_on'] =  $auction['auction_status'];		
		} else {
		$this->data['status_on'] ="";		}						
		
		if (isset($this->request->post['start_time'])) {			
		$this->data['start_time'] = $this->request->post['start_time'];		
		} elseif (!empty($auction['bid_date_start'])) {			
		$this->data['start_time'] =  date('Y-m-d', strtotime($auction['bid_date_start']));		
		} else {	
		$this->data['start_time'] = "";		
		}						
		
		if (isset($this->request->post['end_time'])) {			
		$this->data['end_time'] = $this->request->post['end_time'];		
		} elseif (!empty($auction['bid_date_end'])) {			
		$this->data['end_time'] =  date('Y-m-d', strtotime($auction['bid_date_end']));		
		} else {			
		
		$this->data['end_time'] = '';				

		}		


		if (isset($this->request->post['id_tax'])) {      		
		$this->data['id_tax'] = $this->request->post['id_tax'];    
		} elseif (!empty($auction)) {			
		$this->data['id_tax'] = $auction['taxes'];		
		} else {      		
		$this->data['tax_class_id'] = 0;    	}	

		if (isset($this->request->post['start_price'])) {      		
		$this->data['start_price'] = $this->request->post['start_price'];    	
		} elseif (!empty($auction)) {			
		$this->data['start_price'] = $auction['bid_start_price'];		
		} else {      		
		$this->data['start_price'] = '';    	
		}
		
		

		
		if (isset($this->request->post['max_price'])) {      		
		$this->data['max_price'] = $this->request->post['max_price'];    	
		} elseif (!empty($auction['bid_max_price'])) {			
		$this->data['max_price'] = $auction['bid_max_price'];		
		} else {      		
		$this->data['max_price'] = '';    	}

		if (isset($this->request->post['min_offer_step'])) {      		
		$this->data['min_offer_step'] = $this->request->post['min_offer_step'];    	} 
		elseif (!empty($auction['bid_min_price'])) {			
		$this->data['min_offer_step'] = $auction['bid_min_price'];		
		} else {      		
		$this->data['min_offer_step'] = 1;    	
		}								
		
		if (isset($this->request->post['autobid_step'])) {      		
		$this->data['autobid_step'] = $this->request->post['autobid_step'];    	
		} elseif (!empty($auction['bid_auto_increment'])) {			
		$this->data['autobid_step'] = $auction['bid_auto_increment'];		
		} else {      		
		$this->data['autobid_step'] = 1;    	
		}						
		
		if (isset($this->request->post['bids_interval'])) {			
		$this->data['bids_interval'] = $this->request->post['bids_interval'];		
		} elseif (!empty($auction['bid_interval'])) {		
		$this->data['bids_interval'] =  $auction['bid_interval'];		
		} else {			
		$this->data['bids_interval'] = "";
		}
										
		$this->template = 'catalog/bid_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
  	}
	
	
  	private function validateForm() { 
    	if (!$this->user->hasPermission('modify', 'catalog/auctionlist')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

    	
		
		if (empty($this->request->post['start_time'])) {      		$this->error['start_time'] = $this->language->get('error_start');    	}								if (empty($this->request->post['start_price'])) {      		$this->error['bid_price'] = $this->language->get('error_bid_price');    	}						if (isset($this->request->post['use_end_time_on'])) {      		if (empty($this->request->post['end_time'])) {      		$this->error['end_time'] = $this->language->get('error_end_time');						}    	} 
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
					
    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}
	
  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/auctionlist')) {
      		$this->error['warning'] = $this->language->get('error_permission');  
    	}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
  	
  	private function validateCopy() {
    	if (!$this->user->hasPermission('modify', 'catalog/auctionlist')) {
      		$this->error['warning'] = $this->language->get('error_permission');  
    	}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
		
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model']) || isset($this->request->get['filter_category_id'])) {
			$this->load->model('catalog/auctionlist');
			
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}
			
			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}
						
			if (isset($this->request->get['filter_category_id'])) {
				$filter_category_id = $this->request->get['filter_category_id'];
			} else {
				$filter_category_id = '';
			}
			
			if (isset($this->request->get['filter_sub_category'])) {
				$filter_sub_category = $this->request->get['filter_sub_category'];
			} else {
				$filter_sub_category = '';
			}
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];	
			} else {
				$limit = 20;	
			}			
						
			$data = array(
				'filter_name'         => $filter_name,
				'filter_model'        => $filter_model,
				'filter_category_id'  => $filter_category_id,
				'filter_sub_category' => $filter_sub_category,
				'start'               => 0,
				'limit'               => $limit
			);
			
			$results = $this->model_catalog_auctionlist->getAuctionlists($data);
			
			foreach ($results as $result) {
				$option_data = array();
				
				$auctionlist_options = $this->model_catalog_auctionlist->getAuctionlistOptions($result['auctionlist_id']);	
				
				foreach ($auctionlist_options as $auctionlist_option) {
					if ($auctionlist_option['type'] == 'select' || $auctionlist_option['type'] == 'radio' || $auctionlist_option['type'] == 'checkbox' || $auctionlist_option['type'] == 'image') {
						$option_value_data = array();
					
						foreach ($auctionlist_option['auctionlist_option_value'] as $auctionlist_option_value) {
							$option_value_data[] = array(
								'auctionlist_option_value_id' => $auctionlist_option_value['auctionlist_option_value_id'],
								'option_value_id'         => $auctionlist_option_value['option_value_id'],
								'name'                    => $auctionlist_option_value['name'],
								'price'                   => (float)$auctionlist_option_value['price'] ? $this->currency->format($auctionlist_option_value['price'], $this->config->get('config_currency')) : false,
								'price_prefix'            => $auctionlist_option_value['price_prefix']
							);	
						}
					
						$option_data[] = array(
							'auctionlist_option_id' => $auctionlist_option['auctionlist_option_id'],
							'option_id'         => $auctionlist_option['option_id'],
							'name'              => $auctionlist_option['name'],
							'type'              => $auctionlist_option['type'],
							'option_value'      => $option_value_data,
							'required'          => $auctionlist_option['required']
						);	
					} else {
						$option_data[] = array(
							'auctionlist_option_id' => $auctionlist_option['auctionlist_option_id'],
							'option_id'         => $auctionlist_option['option_id'],
							'name'              => $auctionlist_option['name'],
							'type'              => $auctionlist_option['type'],
							'option_value'      => $auctionlist_option['option_value'],
							'required'          => $auctionlist_option['required']
						);				
					}
				}
					
				$json[] = array(
					'auctionlist_id' => $result['auctionlist_id'],
					'name'       => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'),	
					'model'      => $result['model'],
					'option'     => $option_data,
					'price'      => $result['price']
				);	
			}
		}

		$this->response->setOutput(json_encode($json));
	}
	
	public function resetallbid() {
	
		$this->language->load('catalog/auctionlist');
		
		$this->load->model('catalog/auctionlist');
				
		$json = array();
		
		$this->model_catalog_auctionlist->closeAuctionlist($this->request->get['auction_id'],$this->request->get['status']);
		
		$result = $this->model_catalog_auctionlist->deletecustomerbid($this->request->get['product_id']);
		
		if($result){
						
	    $json['success'] = "All bids and auto bid will be removed and auction price will be reset to starting price";
		
		}
				
		
		
		
		$this->response->setOutput(json_encode($json));
	}
}
?>