<?php
class ControllerCatalogAuctionpayment extends Controller {
	private $error = array();

  	public function index() {
		$this->load->language('catalog/auctionpayment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/auctionpayment');

    	$this->getList();
  	}
	
  	
  	
  	public function delete() {
		$this->load->language('catalog/auctionpayment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/auctionpayment');

    	if (isset($this->request->post['selected']) && ($this->validateDelete())) {
			foreach ($this->request->post['selected'] as $auctionpayment_id) {
				$this->model_catalog_auctionpayment->deleteauctionpayment($auctionpayment_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_auctionpayment_id'])) {
				$url .= '&filter_auctionpayment_id=' . $this->request->get['filter_auctionpayment_id'];
			}
			
			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_pname'])) {
				$url .= '&filter_pname=' . urlencode(html_entity_decode($this->request->get['filter_pname'], ENT_QUOTES, 'UTF-8'));
			}
												
			if (isset($this->request->get['filter_auctionpayment_status_id'])) {
				$url .= '&filter_auctionpayment_status_id=' . $this->request->get['filter_auctionpayment_status_id'];
			}
			
			if (isset($this->request->get['filter_total'])) {
				$url .= '&filter_total=' . $this->request->get['filter_total'];
			}
						
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}
			
			if (isset($this->request->get['filter_date_modified'])) {
				$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
			}
													
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['auctionpayment'])) {
				$url .= '&auctionpayment=' . $this->request->get['auctionpayment'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    	}

    	$this->getList();
  	}

  	private function getList() {
		if (isset($this->request->get['filter_auctionpayment_id'])) {
			$filter_auctionpayment_id = $this->request->get['filter_auctionpayment_id'];
		} else {
			$filter_auctionpayment_id = null;
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}
		
		
		if (isset($this->request->get['filter_pname'])) {
			$filter_pname = $this->request->get['filter_pname'];
		} else {
			$filter_pname = null;
		}

		if (isset($this->request->get['filter_auctionpayment_status_id'])) {
			$filter_auctionpayment_status_id = $this->request->get['filter_auctionpayment_status_id'];
		} else {
			$filter_auctionpayment_status_id = null;
		}
		
		if (isset($this->request->get['filter_total'])) {
			$filter_total = $this->request->get['filter_total'];
		} else {
			$filter_total = null;
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}
		
		if (isset($this->request->get['filter_date_modified'])) {
			$filter_date_modified = $this->request->get['filter_date_modified'];
		} else {
			$filter_date_modified = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'o.auctionpayment_id';
		}

		if (isset($this->request->get['auctionpayment'])) {
			$auctionpayment = $this->request->get['auctionpayment'];
		} else {
			$auctionpayment = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
				
		$url = '';

		if (isset($this->request->get['filter_auctionpayment_id'])) {
			$url .= '&filter_auctionpayment_id=' . $this->request->get['filter_auctionpayment_id'];
		}
		
		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}
											
		if (isset($this->request->get['filter_auctionpayment_status_id'])) {
			$url .= '&filter_auctionpayment_status_id=' . $this->request->get['filter_auctionpayment_status_id'];
		}
		
		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}
		
		if (isset($this->request->get['filter_pname'])) {
			$url .= '&filter_pname=' . urlencode(html_entity_decode($this->request->get['filter_pname'], ENT_QUOTES, 'UTF-8'));
		}
					
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
		
		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['auctionpayment'])) {
			$url .= '&auctionpayment=' . $this->request->get['auctionpayment'];
		}
		
		if (isset($this->request->get['product_id'])) {
			$url .= '&product_id=' . $this->request->get['product_id'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['invoice'] = $this->url->link('catalog/auctionpayment/invoice', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['insert'] = $this->url->link('catalog/auctionpayment/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/auctionpayment/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['auctionpayments'] = array();
		
		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = '';
		}

		$data = array(
			'filter_auctionpayment_id'        => $filter_auctionpayment_id,
			'filter_customer'	     => $filter_customer,
			'filter_auctionpayment_status_id' => $filter_auctionpayment_status_id,
			'filter_total'           => $filter_total,
			'filter_pname'          => $filter_pname,
			'filter_date_added'      => $filter_date_added,
			'filter_date_modified'   => $filter_date_modified,
			'sort'                   => $sort,
			'auctionpayment'                  => $auctionpayment,
			'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                  => $this->config->get('config_admin_limit')
		);

		$auctionpayment_total = $this->model_catalog_auctionpayment->getTotalauctionpayments($data,$product_id);

		$results = $this->model_catalog_auctionpayment->getauctionpayments($data,$product_id);
		
		
    	foreach ($results as $result) {
			$action = array();
						
			
			
						
			$this->data['auctionpayments'][] = array(
				'auctionpayment_id'      => $result['winner_id'],
				'customer'      => $result['customer'],
				'status'        => $result['status'],
				'pname'        => $result['productname'],
				'total'         => $this->currency->format($result['price_bid']),
				'date_added'    => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'selected'      => isset($this->request->post['selected']) && in_array($result['auctionpayment_id'], $this->request->post['selected']),
				'action'        => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_missing'] = $this->language->get('text_missing');

		$this->data['column_auctionpayment_id'] = $this->language->get('column_auctionpayment_id');
    	$this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_product'] = $this->language->get('column_product');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_total'] = $this->language->get('column_total');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_date_modified'] = $this->language->get('column_date_modified');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_invoice'] = $this->language->get('button_invoice');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_auctionpayment_id'])) {
			$url .= '&filter_auctionpayment_id=' . $this->request->get['filter_auctionpayment_id'];
		}
		
		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_pname'])) {
			$url .= '&filter_pname=' . urlencode(html_entity_decode($this->request->get['filter_pname'], ENT_QUOTES, 'UTF-8'));
		}
											
		if (isset($this->request->get['filter_auctionpayment_status_id'])) {
			$url .= '&filter_auctionpayment_status_id=' . $this->request->get['filter_auctionpayment_status_id'];
		}
		
		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}
					
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
		
		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}
		
		if (isset($this->request->get['product_id'])) {
			$url .= '&product_id=' . $this->request->get['product_id'];
		}

		if ($auctionpayment == 'ASC') {
			$url .= '&auctionpayment=DESC';
		} else {
			$url .= '&auctionpayment=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_auctionpayment'] = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . '&sort=o.auctionpayment_id' . $url, 'SSL');
		$this->data['sort_customer'] = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, 'SSL');
		
		$this->data['sort_pname'] = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . '&sort=w.productname' . $url, 'SSL');
		
		
		$this->data['sort_status'] = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_total'] = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . '&sort=w.price_bid' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . '&sort=w.date_added' . $url, 'SSL');
		$this->data['sort_date_modified'] = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_auctionpayment_id'])) {
			$url .= '&filter_auctionpayment_id=' . $this->request->get['filter_auctionpayment_id'];
		}
		
		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}
		
		
		if (isset($this->request->get['filter_pname'])) {
			$url .= '&filter_pname=' . urlencode(html_entity_decode($this->request->get['filter_pname'], ENT_QUOTES, 'UTF-8'));
		}
											
		if (isset($this->request->get['filter_auctionpayment_status_id'])) {
			$url .= '&filter_auctionpayment_status_id=' . $this->request->get['filter_auctionpayment_status_id'];
		}
		
		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}
					
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
		
		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}
		
		if (isset($this->request->get['product_id'])) {
			$url .= '&product_id=' . $this->request->get['product_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['auctionpayment'])) {
			$url .= '&auctionpayment=' . $this->request->get['auctionpayment'];
		}

		$pagination = new Pagination();
		$pagination->total = $auctionpayment_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/auctionpayment', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_auctionpayment_id'] = $filter_auctionpayment_id;
		$this->data['filter_customer'] = $filter_customer;
		$this->data['filter_auctionpayment_status_id'] = $filter_auctionpayment_status_id;
		$this->data['filter_total'] = $filter_total;
		$this->data['filter_date_added'] = $filter_date_added;
		$this->data['filter_date_modified'] = $filter_date_modified;
		
		$this->data['filter_pname'] = $filter_pname;

		

		$this->data['sort'] = $sort;
		$this->data['auctionpayment'] = $auctionpayment;

		$this->template = 'catalog/auctionpayment_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  	}

  	
      		    
	
   	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/auctionpayment')) {
			$this->error['warning'] = $this->language->get('error_permission');
    	}

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
	
	
}
?>